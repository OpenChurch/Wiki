# Impressum und Autor:innen

Mitarbeit am OpenChurch.Wiki ist offen für alle Menschen, die sich daran mit beteiligen möchten und mit den [Zielen](index.md) identifizieren können.

Auf Codeberg ist die [Liste der Autor:innen](https://codeberg.org/OpenChurch/Wiki/activity/contributors) mit ihren Beiträgen einsehbar.

Dieses Projekt ist nicht-kommerziell. Es beruht auf ehrenamtlicher Arbeit und auf Arbeit von Menschen, die kirchliche Gemeinschaften durch die Beträge ihrer Mitglieder (z.B. Kirchensteuer) finanzieren.

Verantwortlich für Inhalt und Gestaltung:

[Johannes Brakensiek](https://johannes.brakensiek.info/)