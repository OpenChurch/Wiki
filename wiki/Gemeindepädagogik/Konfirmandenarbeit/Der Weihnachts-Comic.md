# Der Weihnachts-Comic

## Die Weihnachtsgeschichte in sechs Abschnitten nach Lukas 1 und 2

* [Hörbuchfassung von Kapitel 1.](https://bs1.erf-cdn.de/resources/ahfa/1f5d1676aa99af16032646f65ef201f057cb2bea.mp3)
* [Hörbuchfassung von Kapitel 2.](https://bs1.erf-cdn.de/resources/ahfa/2420f95c6286f49f0db7ec4efad47e073bdc1361.mp3)

In der Tabelle stehen die Hörzeiten für die Abschnitte.

| Gruppe | Text                 | Lesefassung                  | Hörfassung                  |
|--------|----------------------|------------------------------|-----------------------------|
| Abschnitt 1 | Ein Engel kündigt Maria die Geburt von Jesus an | [Lukas 1,26-38](https://www.bibleserver.com/HFA/Lukas1,26-38) | 3:53 bis 5:47 |
| Abschnitt 2 | Jesus wird geboren | [Lukas 2,1-7 HfA](https://www.bibleserver.com/HFA/Lukas2,1-7) | 0:00 bis 0:55 |
| Abschnitt 3 | Die Engel bei den Hirten | [Lukas 2,8-14 HfA](https://www.bibleserver.com/HFA/Lukas2,8-14) | 0:56 bis 1:46 |
| Abschnitt 4 | Die Hirten besuchen Jesus | [Lukas 2,15-20 HfA](https://www.bibleserver.com/HFA/Lukas2,15-20) | 1:47 bis 2:32 |
| Abschnitt 5 | Simeon und Hanna | [Lukas 2,25-39 HfA](https://www.bibleserver.com/HFA/Lukas2,25-39) | 3:14 bis 5:26 |
| Abschnitt 6 | Der zwölfjährige Jesus im Tempel | [Lukas 2,41-51 HfA](https://www.bibleserver.com/HFA/Lukas2,41-51) | 5:35 bis 7:03 |

## Aufgaben

1. Lest bzw. hört euren Abschnitt.
2. Schreibt miteinander auf:
    1. Wie ist der Hintergrund bzw. die Szene und Umgebung?
    2. Welche Personen treten auf? Wer spricht mit wem?
    3. Was sind die wichtigsten Handlungen und die wichtigsten Aussagen?
3. Wie könnt ihr die wichtigsten Szenen des Abschnitts darstellen?
4. Stellt euren Abschnitt mit Fotos auf maximal einer Comic-Buch-Seite dar. Ihr könnt die Bilder mit Effekten versehen. Ihr könnt Sprechblasen ergänzen.

Verwendet dafür die App ["Comic Book!"](https://apps.apple.com/de/app/comicbook/id436114747).