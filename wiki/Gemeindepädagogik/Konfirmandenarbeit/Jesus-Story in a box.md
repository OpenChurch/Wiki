# Jesus-Story in a box

## 14 Texte aus dem Evangelium nach Markus

| Text                 | Lesefassung                  | Hörfassung                  |
|----------------------|------------------------------|-----------------------------|
| Die Heilung eines Gelähmten | [Markus 2,1–12 HfA](https://www.bibleserver.com/HFA/Markus2%2C1-12) | [Markus 2,1–12 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/9039fb1be44334ae0d30c6681e2be60debce60e8.mp3), bis 1:45 Minute |
| Die Berufung eines Zöllners | [Markus 2,13–17 HfA](https://www.bibleserver.com/HFA/Markus2%2C13-17) | [Markus 2,3–17 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/9039fb1be44334ae0d30c6681e2be60debce60e8.mp3), ab 1:47 bis 2:56 Minute |
| Die Stillung des Seesturms | [Markus 4,35–40 HfA](https://www.bibleserver.com/HFA/Markus4%2C35-40) | [Markus 4,35–40 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/9dc931041069a2ea3bf13f4e2d2ffbbc574c4c55.mp3), ab 6:04 Minute |
| Ablehnung in Nazaret | [Markus 6,1–6 HfA](https://www.bibleserver.com/HFA/Markus6%2C1-6) | [Markus 6,1–6 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/121adf988723896de0c447b9d65efec82de25523.mp3), bis 1:00 Minute |
| Die Speisung der fünftausend | [Markus 6,30–44 HfA](https://www.bibleserver.com/HFA/Markus6%2C30-44) | [Markus 6,30–44 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/121adf988723896de0c447b9d65efec82de25523.mp3), ab 5:30 Minute |
| Das Bekenntnis des Petrus <br/>und die erste Leidensankündigung | [Markus 8,27–33 HfA](https://www.bibleserver.com/HFA/Markus8%2C27-33) | [Markus 8,27–33 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/daef2f461b43a73bf7260978050191d24e690eea.mp3), ab 4:20 Minute bis 5:47 Minute |
| Der Rangstreit unter den Jüngern | [Markus 9,33–37 HfA](https://www.bibleserver.com/HFA/Markus9%2C33-37) | [Markus 9,33–37 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/a8a50960b13642dbb01e69e0ad2374f00559128b.mp3), ab 5:37 Minute bis 6:22 Minute |
| Die Segnung der Kinder | [Markus 10,13–16 HfA](https://www.bibleserver.com/HFA/Markus10%2C13-16) | [Markus 10,13–16 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/c93153a736e3310270ed743e02532c76efbfdf3e.mp3), ab 1:35 Minute bis 2:10 Minute |
| Nachfolge und Reichtum | [Markus 10,17–22 HfA](https://www.bibleserver.com/HFA/Markus10%2C17-22) | [Markus 10,17–22 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/c93153a736e3310270ed743e02532c76efbfdf3e.mp3), ab 2:10 Minute bis 3:26 Minute |
| Der Einzug in Jerusalem | [Markus 11,1–11 HfA](https://www.bibleserver.com/HFA/Markus11%2C1-11) | [Markus 11,1–11 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/960ccea614193171ad130cb423d0173c69624f33.mp3), bis 1:55 Minute |
| Die Tempelreinigung | [Markus 11,15–19 HfA](https://www.bibleserver.com/HFA/Markus11%2C15-19) | [Markus 11,15–19 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/960ccea614193171ad130cb423d0173c69624f33.mp3), ab 2:27 Minute bis 3:22 Minute |
| Zur Frage nach der kaiserlichen Steuer | [Markus 12,13–17 HfA](https://www.bibleserver.com/HFA/Markus12%2C13-17) | [Markus 12,13–17 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/88b0318adb5d3a25bd91aa3246b4f53c7dbb199b.mp3), ab 2:21 Minute bis 3:37 Minute |
| Die Frage nach dem höchsten Gebot | [Markus 12,28–43 HfA](https://www.bibleserver.com/HFA/Markus12%2C28-43) | [Markus 12,28–43 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/88b0318adb5d3a25bd91aa3246b4f53c7dbb199b.mp3), ab 5:16 Minute bis 6:44 Minute |
| Die Salbung in Betanien | [Markus 14,3–9 HfA](https://www.bibleserver.com/HFA/Markus14%2C3-9) | [Markus 14,3–9 HfA Hörbuch](https://bs1.erf-cdn.de/resources/ahfa/4296734a401732d6825ed9ffae70ee3a230a4edd.mp3), ab 0:29 Minute bis 1:49 Minute |

nach: Konf. Wir leben in Beziehungen. Arbeitshilfe Konfirmation. Evangelisch-reformierte Landeskirche des Kantons Zürich 2014, M 3.2.8.