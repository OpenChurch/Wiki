# Konfirmandenarbeit

## Online-Konfimaterial

- [Konfikurs Online von Gerhard Beck](https://blogs.rpi-virtuell.de/konfikurs/)
- [Konfis auf Gottsuche (Hans-Ulrich Keßler und Burkhardt Nolte) - Handbuch, Videos, Materialien](https://www.randomhouse.de/Konfis-auf-Gottsuche/aid81281.rhd)	
- Minetest-Welten, die die Welt verändern. Wie man das freie Open-World-Spiel für Bildung nutzen kann: https://blogs.rpi-virtuell.de/minetest/


## Hardware für Konfis

- Tablets: https://www.afbshop.de/notebooks/tablets


