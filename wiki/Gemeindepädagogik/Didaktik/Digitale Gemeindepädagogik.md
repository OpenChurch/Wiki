# Digitale Gemeindepädagogik

## Lernplattformen

- [Moodle](https://moodle.org/), gehostet vom TecKids e.V.: https://lms.teckids.org/
    - Selbstverwaltung von Kursen; Accounts unter https://ticdesk.teckids.org/account/register
    - Quelle: https://floss.social/@teckids_eV/103820256209309576


### Learning Management Systems (LMS)

#### Preamble

Freie Software für freie Lehre!  
Gemeinsame Erklärung der deutschen, communitygestützten Open-Source-Bildungsplattformen  
https://www.opensourcelms.de/


#### Reine LMS

- [Chamilo](https://chamilo.org) - [Quellcode](https://github.com/chamilo/chamilo-lms) - [Wikipedia](https://de.wikipedia.org/wiki/Chamilo)
- [Moodle](https://moodle.org/) - [Wikipedia](https://de.wikipedia.org/wiki/Moodle) - [Moodlebox](https://moodlebox.net/de/) (Raspberry Pi Hardwarelösung)
- [OLAT](https://olat.org/) / [OpenOLAT](https://www.openolat.com/) - [Wikipedia](https://de.m.wikipedia.org/wiki/OLAT)
- [Sakai](https://www.sakailms.org/) - [Wikipedia](https://de.m.wikipedia.org/wiki/Sakai_(Software))
- [ILIAS](https://www.ilias.de/)
- [ELMS Learning Network](https://www.elmsln.org/)
- [OpenEDX](https://open.edx.org/get-started/get-started-self-managed/)

#### Wiki-basierte LMS

- [MediaWiki](https://www.mediawiki.org) (modifiziert), Beispiel [unterrichten.zum.de](https://unterrichten.zum.de/wiki/Hauptseite)?

## Blogs

- Gerhard Beck – Digitale Hilfsmittel für Religionsunterricht und Gemeindearbeit. Tests und Versuche aus der Praxis: https://blogs.rpi-virtuell.de/digital
- Medienpädagogik Open Praxis-Blog – https://www.medienpaedagogik-praxis.de/

## Ressourcen, Tools und Materialien

- https://ebildungslabor.de/
- https://rpi-virtuell.de/
- https://codeberg.org/lerntools Die Open-Source Software Lerntools bietet eine Ideensammlung (änlich Padlet), eine Umfrage und eine ABCD Quiz (ähnlich Kahoot). Dabei wurde sehr auf Datenschutz geachtet. Man kann sie unter https://www.lerntools.org ausprobieren.
- Online Warm Ups & Energizers: https://blog.mural.co/online-warm-ups-energizers
    - Zum Zeichnen geht auch https://draw.chat/ oder https://aggie.io/

## Chat

- Matrix-Austausch: [#digitales-lernen:synod.im](https://matrix.to/#/#digitales-lernen:synod.im)

## Konferenzen und Kommunikation

- Mumble von TecKids e.V.: https://floss.social/@teckids_eV/103817764787963416
- Jitsi Meet von TecKids e.V.: https://meet.teckids.org/


## Bildungsapps für die Primarstufe

- https://www.gcompris.net/index-de.html


## Literatur

- Nele Hirsch, Unterricht digital. Methoden, Didaktik und Praxisbeispiele für das Lernen mit Online-Tools, Mülheim an der Ruhr 2020. [Dropbox Download](https://www.dropbox.com/s/n07ve8uqiy1ymbq/Nele_Hirsch-Unterricht_digital.pdf?dl=0)
