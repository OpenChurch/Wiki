Hardware für Homelearning
=========================

## Projekte

- Deutschland: https://computertruhe.de/
- Weltweit: https://www.labdoo.org/de/LabdooD
- Schulen: https://pc-spende.das-macht-schule.net/ - https://www.das-macht-schule.net/gratis-hardware/


## SoC-Kleinstcomputer

### Raspberry Pi 4 (4GB)

- Starterset: https://www.pollin.de/p/raspberry-pi-4b-4gb-starter-set-811129
- Maus/Tastatur: https://www.pollin.de/p/tastatur-und-maus-set-inter-tech-kb-118-schwarz-750823
- Funk-Maus-/Tastatur: https://www.pollin.de/p/funktastatur-und-maus-set-logilink-id0161-slim-schwarz-750509

### Olimex (Open Source Hardware)

- Olimex OLinuXino A20 LIME2 (technisch eher wie Raspberry Pi 2): https://www.olimex.com/Products/OLinuXino/A20/open-source-hardware


### Displays (günstigste, mit Gehäuse)

- realistischste Möglichkeit ist die Nutzung des Flachbild-TVs mit HDMI zuhause (den die allermeisten Haushalte besitzen)
- 10,1 Zoll: https://www.pollin.de/p/raspberry-pi-lc-monitor-10-1-25-65-cm-inkl-fernbedienung-121564 (80€)
- Kuman 7 Zoll
    - Gehäuse: https://www.amazon.de/Kuman-Raspberry-Screen-Gehäuse-Acrylic/dp/B07K6GHJFH/
    - Gesamt: https://www.amazon.de/Raspberry-Capacitive-Bildschirm-800x480-Display/ (60€)
- Kuman 5 Zoll
    - Gesamt: https://www.amazon.de/Kuman-Resistive-Protective-Touchscreen-Bildschirm/dp/B07M8QFKHK/ (45€)
- zum Basteln: https://www.olimex.com/Products/OLinuXino/LCD/open-source-hardware
- ansonsten besteht die Möglichkeit, günstigste VGA Displays mit Adapter zu verwenden


## Gebrauchte Laptops

### Bezugsadressen

- https://www.afbshop.de/
- https://www.notebookgalerie.de/notebooks.html
- https://www.lapstore.de/
- https://www.notebooksbilliger.de/
- s. auch https://computertruhe.de/2020/04/24/bund-unterstuetzt-beduerftige-schuelerinnen-mit-150-e-beim-computerkauf/


