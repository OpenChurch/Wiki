# Über

Gesucht sind: Alte und neue Texte, neue Vertonungen, Arragements,
Konzepte, Ideen… Hier darf alles rein, dessen Lizenz mit CC-BY-SA
kompatibel ist: gemeinfreie Werke, CC-0, CC-BY, CC-BY-SA.

Ebenso sind Links (aber nur Links!) zu anderen Quellen mit kostenfrei
verfügbarer Kirchenmusik erlaubt, die unter anderen oder ungeklärten
Lizenzen stehen, natürlich aber auch Links zu anderen Projekten mit
CC-BY-SA-freundlichen Werken.

## Andere Projekte mit Musik für den kirchlichen Gebrauch

 * Die Musikpiraten haben [jahreszeitliche Lieder](https://github.com/Musikpiraten/public-domain-season-songs)
   ins Netz gestellt, nicht nur kirchliche, aber ein paar bekannte Stücke zu
   den großen Festtagen sind dabei.
* im englischsprachigen Bereich gibt es [OpenHymnal](http://openhymnal.org/) als Schwesterprojekt mit einigen freien Noten und Texten

## Kostenlos nutzbar aber unklare Lizenz

 * Kinderlieder beim [Liederprojekt](https://www.liederprojekt.org/) des Carus-Verlags
 * [Cantico](https://www.cantico.me/) ist eine Gesangbuch-App
