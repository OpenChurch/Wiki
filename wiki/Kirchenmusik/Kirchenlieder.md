# Evangelisches Gesangbuch

Eine Liste aller Lieder gibt es in der Wikipedia auf der Seite [Liste_der_Kirchenlieder_im_Evangelischen_Gesangbuch](https://de.wikipedia.org/wiki/Liste_der_Kirchenlieder_im_Evangelischen_Gesangbuch).

Auf der Seite [4Bibeln](http://www.l4a.org/4bibeln/) gibt es unter dem Menüpunkt *Gesangbuch* ein alphabetisches Verzeichnis und die Möglichkeit der Volltextsuche.
Von fast allen Liedern gibt es hier den Text, die Noten der Melodie (ohne Akkordsymbol) und eine Midi-Datei.

# Gotteslob

Für das katholische Gesangbuch gibt es eine Liste aller Lieder des Stammteils auf der Seite [Liste der Gesänge im Stammteil des Gotteslobs](https://de.wikipedia.org/wiki/Liste_der_Ges%C3%A4nge_im_Stammteil_des_Gotteslobs).

# Offenes Gesangbuch

Es gibt [Pläne](Offenes-Gesangbuch.md) für ein (ökumenisches) Offenes Gesangbuch, auch wenn auf dem Weg noch viel Arbeit liegt.

# Forschungsstelle Kirchenlied und Gesangbuch

An der Uni Mainz gibt es eine [Forschungsstelle](https://www.gesangbucharchiv.uni-mainz.de/)
zum Thema, die auch einige meist sehr alte [Digitalisate](https://gutenberg-capture.ub.uni-mainz.de/provenienzen/nav/classification/305127) zur Verfügung stellt.
