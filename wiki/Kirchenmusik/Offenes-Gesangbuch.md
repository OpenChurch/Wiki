# Offenes Gesangbuch

Ein offenes Gesangbuch erlaubt die freie Nutzung und Bearbeitung der
Lieder des Gesangbuchs für jeden Zweck. Es können beliebig Kopien,
teilweise oder komplett, erstellt werden; ebenso Remixes, Veränderungen,
Modernisierungen und so weiter vorgenommen werden.

Eine Bedingung dafür ist, dass die Lieder des Gesangbuchs unter
entsprechenden Lizenzen verfügbar sind. Das ist für gemeinfreie Werke
der Fall, während Stücke, die dem Urheberrecht unterliegen in aller
Regel (wenn sie nicht unter einer freien Lizenz, zum Beispiel aus der
Creative Commons Lizenzfamilie, veröffentlicht wurden), für ein solches
Gesangbuch nicht geeignet sind.

Es gibt verschiedene Gesangbücher, zum Beispiel die Familie evangelischer
Gesangbücher mit einem gemeinsamen Teil und einem Teil, der regionale
Vorlieben abbildet, oder das katholische "Gotteslob", mit einigen
Überschneidungen bei den Stücken.

Zwar sind nicht alle Lieder gemeinfrei (oder geeignet lizenziert)
verfügbar, allerdings kann ein offenes Gesangbuch erstellt werden,
dass wenigstens die frei verfügbaren Teile enthält.

Das ist das Ziel dieses Projekts: ein online und offline benutzbares Gesangbuch; zumindest für die gemeinfreien Lieder.

## Versionen

Von Liedern können verschiedene Fassungen existieren, zum Beispiel die Melodie mit Text, wie in Gesangbüchern üblich; Choralsätze; Orgelsätze; Akkorde.

Das offene Gesangbuch sammelt alle möglichen Fassungen, auch könnten neue Fassungen aus dem vorhandenen Bestand geschaffen werden.

## Gemeinfreie Quellen

Ein Werk wird gemeinfrei, wenn es aus dem Urheberrecht fällt: In
Deutschland müssen dafür die Rechteinhaber seit 70 Kalenderjahren verstorben
sein, beziehungsweise bei juristischen Personen, die Erstveröffentlichung
70 Kalenderjahre zurückliegen.

Rechteinhaber sind zum Beispiel der Texter und Musiker, aber auch der
Verlag eines Drucks und ein Bearbeiter, der das Werk nachträglich
verändert hat.  Damit ist es auch möglich, dass ein Werk, dessen
Grundlage bereits gemeinfrei wurde, in einer neuen Version wieder Schutz
genießen kann.

Aus diesem Grund ist es sinnvoll, bei Quellen auf ein ausreichendes
Alter zu achten: Da Verlage in der Regel juristische Personen sind,
ist ein Druck, der über 70 Jahre alt ist, nicht mehr von Rechten des
Verlags betroffen.

Zu beachten ist dabei, dass auch Scans ein neues Urheberrecht (für
die Digitalisate) begründen können. Daher muss bei digitalen Quellen
(die allesamt jünger als 70 Jahre alt sind) darauf geachtet werden,
dass die Verbreitung dieser Scans unter einer freien Lizenz erfolgt,
beziehungsweise in die "Public Domain" gegeben wurde - eine Möglichkeit,
die Urheber im angelsächsischen Raum haben, die in Deutschland nicht
vorgesehen ist.

### Verwendbare Quellen

Viele Noten gibt es beim [International Music Score Library Project](https://imslp.org/wiki/Main_Page) und in den [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page), wobei darauf zu achten ist, dass die Digitalisate als "public domain" verbreitet werden.

An dieser Stelle werden einige Quellen gesammelt, die den oben genannten
Kriterien entsprechen, mit einem Kürzel, das im Folgenden genutzt wird,
um auf diese Quellen zu verweisen.

 * Das [evangelische Gesangbuch der Synode von Nord-Amerika von 1895 (eG-NA)](https://imslp.org/wiki/Evangelisches_Gesangbuch_(Various))
 * Das [evangelische Choralbuch aus Potsdam, 1855 (eC-P)](https://imslp.org/wiki/Evangelisches_Choralbuch_(Sch%C3%A4rtlich%2C_Johann_Christian))
 * Die [Choral Public Domain Library](http://www.cpdl.org) mit gemeinfreien Chorwerken, manchmal auch im MusicXML-Format
 * Das [Internet Music Score Library Project](https://imslp.org/) mit Musikstücken unter freien Lizenzen
 * Das Projekt [Nun singet und seid froh](https://nun-singet-und-seid-froh.info/) bei [GitHub](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh) sammelt gemeinfreie Chorsätze von Weihnachtsliedern im LilyPond-Format.

### Gemeinfreiheit auf Wikidata

In Wikidata gibt es für Werke die Aussage "Urheberrechtsstatus". Den Wikidata-Eintrag bekommt man am einfachsten über den Wikipedia-Eintrag des Liedes und dann im linken Menü "Wikidata-Datenobjekt" wählen.

Folgende Aussagen sind möglich:

* ist ein(e): geistliches Lied
* Genre:  z.B. Adventslied / Weihnachtscarole / ...
* basiert auf: z.B. Psalm 24
* zentrales Thema: z.B. Adventskranz
* Komponist (evtl. +Veröffentlichungsdatum) (evtl. Unbekannter, mit Qualifikatoren Ort und Veröffentlichkeitsdatum)
* verwendet eine Melodie aus
* Text von: ... (Person); (nicht: Librettist, das benutzt man bei größeren Werken)
* Liedtext (z.B. "Matthäus 21" mit Qualifikator Absatz, Artikel, Paragraph oder Vers: 1-9)
   * wenn ein Komponist oder Texter nur für bestimmte Verse gilt, kann dies mit "Absatz, Artikel, Paragraph oder Vers" als Qualifikator eingetragen werden
* Übersetzer
* Sprache des Werkes: Deutsch
* Veröffentlichkeitsdatum: ... (bei "ca. 1663" oder "um 1663" die Genauigkeit 1 Jahrzehnt wählen)
* veröffentlicht in: Evangelisches Gemeindelied oder Gotteslob (2013)
   * Ordnungsnummer: z.B. 41
   * Vorgänger: (Liedname)
   * Nachfolger: (Liedname)
* **Urheberrechtsstatus**: Gemeinfreiheit mit den Qualifikatoren
   * gehört zum Zuständändigkeitsbereich: 100 Jahre oder weniger nach dem Tod des Urhebers (das "weniger" stimmt)
   * Bestimmungsmethode: 100 Jahre nach Ableben des Urhebers (für jüngere Werke evtl. beides mit 70)

Fundstelle: eine der drei Möglichkeiten

 * nachgewiesen in: Evangelisches Gesangbuch oder Gotteslob (2013) (wenn Fundstelle außerhalb Wikidata)
 * abgeleitet von: ... (Fundstelle innerhalb der Wikidata & Wikipedia)
 * URL der Fundstelle: ...  mit Qualifikator "abgerufen am" 

Wikidata bezieht sich nur auf Melodie, Text und Übersetzung. Man muss man trotzdem noch überprüfen, ob noch Urheberrechte für des Satz oder den Scan bestehen.
Biografien der Urheber findet man im EG ab Seite 1554.

## Dateiformat und Verzeichnisstruktur

Die Lieder sollten im MusicXML-Format gespeichert werden, da dieses die größte
Flexibilität in der Weiterverwendung bietet. Da das Format in der Regel nur für
Export und Import in Programme genutzt wird, nicht als den Programmen eigenes
Format, muss darauf geachtet werden, dass der Export keine Verluste mitbringt.

Jedes Lied sollte in einer eigenen Datei gespeichert werden, bevorzugt dem
Schema `Dichter - Titel.musicxml` folgend, oder `Komponist - Titel.musicxml`,
falls der Komponist wichtiger ist (oder der Dichter unbekannt). Sind beide
nicht bekannt, ist `Unbekannt - Titel.musicxml` zu nutzen, es sei denn, es gibt
eine üblichere Art, die unbekannte Herkunft zu bezeichnen, zum Beispiel mit
`Traditional - Titel.musicxml` für afroamerikanische Stücke.

Die Zusammenstellung als Gesangbuch kann mit einem Buch-Werkzeug gesteuert
werden. Wenn zum Beispiel [lilypond](http://lilypond.org/) für den Notensatz
verwendet werden soll, kann `lilypond-book` genutzt werden, um die Lieder in
der richtigen Reihenfolge und mit numerierten Titeln zusammenzustellen.

# Evangelisches Gesangbuch

Eine Liste aller Lieder gibt es [in der Wikipedia](https://de.wikipedia.org/wiki/Liste_der_Kirchenlieder_im_Evangelischen_Gesangbuch)
unter CC-BY-SA ([Wikipedia-Autoren](https://de.wikipedia.org/w/index.php?date-range-to=2020-05-13&tagfilter=&title=Liste_der_Kirchenlieder_im_Evangelischen_Gesangbuch&action=history)),
die hier reproduziert wird:

Erstes Ziel dieses Projekts ist, diese Lieder - soweit rechtlich möglich -
zusammenzutragen, und einen Mechanismus zu schaffen, mit dem die Lieder
mit der jeweiligen Liednummer in richtiger Reihenfolge als Gesangbuch
zusammengefasst werden können.

Abkürzungen:

 * M: Komponist mit Todesjahr
 * T: Texter mit Todesjahr
 * WD: Wikidata-Link
 * **g: Melodie und Text sind gemeinfrei** (falls nicht gemeinfrei, bitte Kommentar)
 * eG-NA: Lied ist im *evangelischen Gesangbuch der Synode von Nord-Amerika* von 1895 zu finden
 * eC-P: Lied ist im *evangelischen Choralbuch aus Potsdam* von 1855 zu finden
 * CPDL: Lied ist im *Choral Public Domain Library* zu finden
 * IMSLP: Lied ist im *Internet Music Score Library Project* zu finden
 * NSUSF: Lied ist bei *Nun singet und seid froh* zu finden

Links zu den Quellen sie oben

[**Die EKD hat im Jahr 2020 eine pdf-Tabelle veröffentlicht, in der für jedes Lied des Stammteils eingetragen ist, ob es gemeinfrei ist.**](https://www.ekd.de/ekd_de/ds_doc/Liedrechte_Evangelisches_Gesangbuch_Stammteil.pdf)


## Gemeinsamer Teil

Der gemeinsame Teil ist nach Liednummer sortiert.

 * 1 [Macht hoch die Tür](Lieder/Georg%20Weissel%20-%20Macht%20hoch%20die%20Tür%20(Melodie,%20einfach).musicxml) (T: Georg Weissel +1635, M: älteste Quelle: Freylinghausen'sches Gesangbuch 1704, [WD](https://www.wikidata.org/wiki/Q1882741), **g**, eG-NA #44, [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 2 Er ist die rechte Freudensonn ([WD](https://www.wikidata.org/wiki/Q95248551), Komponist starb 2006 => nicht gemeinfrei)
 * 3 Gott, heilger Schöpfer aller Stern ([WD](https://www.wikidata.org/wiki/Q472720), **g**)
 * 4 Nun komm, der Heiden Heiland ([WD](https://www.wikidata.org/wiki/Q690357), **g**)
 * 5 Gottes Sohn ist kommen ([WD](https://www.wikidata.org/wiki/Q1538924), **g**)
 * 6 Ihr lieben Christen, freut euch nun ([WD](https://www.wikidata.org/wiki/Q1536801), **g**)
 * 7 O Heiland, reiß die Himmel auf ([WD](https://www.wikidata.org/wiki/Q2008322), **g**, [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 8 Es kommt ein Schiff, geladen ([WD](https://www.wikidata.org/wiki/Q404201), **g**, [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 9 [Nun jauchzet, all ihr Frommen](Lieder/Michael%20Schirmer%20-%20Nun%20jauchzet%20all,%20ihr%20Frommen%20(Melodie).musicxml) (T: Michael Schirmer +1673, [WD](https://www.wikidata.org/wiki/Q2005036), **g**, eG-NA #47)
 * 10 [Mit Ernst, o Menschenkinder](Lieder/Valentin%20Thilo%20der%20Jüngere%20-%20Mit%20Ernst,%20ihr%20Menschenkinder.musicxml) (T: Valentin Thilo +1662, [WD](https://www.wikidata.org/wiki/Q1939354), **g**, eG-NA #49)
 * 11 Wie soll ich dich empfangen ([WD](https://www.wikidata.org/wiki/Q2568149), **g**, eG-NA #45, [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 12 Gott sei Dank durch alle Welt ([WD](https://www.wikidata.org/wiki/Q1538888), **g**, eG-NA #50)
 * 13 Tochter Zion, freue dich ([WD](https://www.wikidata.org/wiki/Q42887781), **g**, [CPDL](http://www3.cpdl.org/wiki/index.php/Tochter_Zion,_freue_dich_(George_Frideric_Handel)))
 * 14 Dein König kommt in niedern Hüllen ([WD](https://www.wikidata.org/wiki/Q1183243), **g**, eG-NA #54)
 * 15 Tröstet, tröstet, spricht der Herr ([WD](https://www.wikidata.org/wiki/Q425926), Komponist starb 2006 => nicht gemeinfrei)
 * 16 Die Nacht ist vorgedrungen ([WD](https://www.wikidata.org/wiki/Q1215056), Komponist starb 1985 => nicht gemeinfrei)
 * 17 Wir sagen euch an den lieben Advent ([WD](https://www.wikidata.org/wiki/Q2585444), Komponist starb 1997 => nicht gemeinfrei)
 * 18 Seht, die gute Zeit ist nah ([WD](https://www.wikidata.org/wiki/Q1769569), Komponist starb 1984 => nicht gemeinfrei)
 * 19 O komm, o komm, du Morgenstern ([WD](https://www.wikidata.org/wiki/Q59753056), Texter lebt noch => nicht gemeinfrei)
 * 20 Das Volk, das noch im Finstern wandelt ([WD](https://www.wikidata.org/wiki/Q60982776), Texter starb 2015 => nicht gemeinfrei)
 * 21 Seht auf und erhebt eure Häupter ([WD](https://www.wikidata.org/wiki/Q95587173), Komponist starb 2018 => nicht gemeinfrei)
 * 22 Nun sei uns willkommen, Herre Christ ([WD](https://www.wikidata.org/wiki/Q2266435), Komponist starb 1955 => wird 2026 gemeinfrei)
 * 23 [Gelobet seist du, Jesu Christ](Lieder/Martin%20Luther%20-%20Gelobet%20seist%20du,%20Jesu%20Christ%20Melodie%20+%20Orgel.musicxml) (T+M: Martin Luther +1546, [WD](https://www.wikidata.org/wiki/Q1144499), **g**, eG-NA #56, eC-P #57)
 * 24 Vom Himmel hoch da komm ich her ([WD](https://www.wikidata.org/wiki/Q1815167), **g**, [CPDL](http://www0.cpdl.org/wiki/index.php/Vom_Himmel_hoch_da_komm_ich_her_(Martin_Luther)))
 * 25 Vom Himmel kam der Engel Schar (15. Jh.) ([WD](https://www.wikidata.org/wiki/Q2533019), **g**, eG-NA #57)
 * 26 Ehre sei Gott in der Höhe (Gebhardi) ([WD](https://www.wikidata.org/wiki/Q95634107), **g**)
 * 27 Lobt Gott, ihr Christen alle gleich ([WD](https://www.wikidata.org/wiki/Q15832617), **g**, eG-NA #58)
 * 28 Also hat Gott die Welt geliebt ([WD](https://www.wikidata.org/wiki/Q95638591), Komponist starb 2018 => nicht gemeinfrei), [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 29 Quem pastores laudavere|Den die Hirten lobeten sehr ([WD](https://www.wikidata.org/wiki/Q1523292), **g**)
 * 30 Es ist ein Ros entsprungen ([WD](https://www.wikidata.org/wiki/Q763041), **g**, [CPDL](http://www1.cpdl.org/wiki/index.php/Es_ist_ein_Ros_entsprungen_(Michael_Praetorius)), [IMSLP](https://imslp.org/wiki/Es_ist_ein_Ros_entsprungen_(Praetorius,_Michael)), [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 31 Es ist ein Ros entsprungen (Kanon) ([WD](https://www.wikidata.org/wiki/Q95676079), Texter starb 1970 => nicht gemeinfrei)
 * 32 Zu Bethlehem geboren ([WD](https://www.wikidata.org/wiki/Q227426), **g** [CPDL](http://www1.cpdl.org/wiki/index.php/Zu_Bethlehem_geboren_(Traditional)))
 * 33 Brich an, du schönes Morgenlicht ([WD](https://www.wikidata.org/wiki/Q95676255), **g** [CPDL](http://www1.cpdl.org/wiki/index.php/Zu_Bethlehem_geboren_(Traditional)), [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 34 Freuet euch, ihr Christen alle ([WD](https://www.wikidata.org/wiki/Q95690331), **g**, [IMSLP](https://imslp.org/wiki/Freuet_euch%2C_ihr_Christen_alle_(Hammerschmidt%2C_Andreas)))
 * 35 Nun singet und seid froh ([WD](https://www.wikidata.org/wiki/Q1660736), **g**, [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 36 Fröhlich soll mein Herze springen ([WD](https://www.wikidata.org/wiki/Q59784978), **g**, eG-NA #61, [CPDL](http://www2.cpdl.org/wiki/index.php/Fr%C3%B6hlich_soll_mein_Herze_springen_(Johann_Cr%C3%BCger)), [IMSLP](https://imslp.org/wiki/Fr%C3%B6hlich_soll_mein_Herze_springen_(Cr%C3%BCger,_Johann)))
 * 37 Ich steh an deiner Krippen hier ([WD](https://www.wikidata.org/wiki/Q915800), **g**, eG-NA #60, [CPDL](http://www2.cpdl.org/wiki/index.php/Ich_steh_an_deiner_Krippen_hier,_BWV_469_(Johann_Sebastian_Bach)), [IMSLP](https://imslp.org/wiki/Ich_steh_an_deiner_Krippen_hier,_BWV_469_(Bach,_Johann_Sebastian)), [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 38 Wunderbarer Gnadenthron ([WD](https://www.wikidata.org/wiki/Q95690073), **g**)
 * 39 Kommt und lasst uns Christum ehren ([WD](https://www.wikidata.org/wiki/Q1780309), **g**, [NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 40 Dies ist die Nacht, da mir erschienen (Langenöls) ([WD](https://www.wikidata.org/wiki/Q95689724), **g**)
 * 41 Jauchzet, ihr Himmel, frohlocket, ihr Engel (Mauersberger) ([WD](https://www.wikidata.org/wiki/Q48732725), Komponist starb 1971 => nicht gemeinfrei, eG-NA #62)
 * 42 Dies ist der Tag, den Gott gemacht ([WD](https://www.wikidata.org/wiki/Q1221339), **g**, eG-NA #63)
 * 43 Ihr Kinderlein, kommet ([WD](https://www.wikidata.org/wiki/Q457952), **g**, [CPDL](http://www1.cpdl.org/wiki/index.php/Ihr_Kinderlein_kommet_(Johann_Abraham_Peter_Schulz)))
 * 44 O du fröhliche ([WD](https://www.wikidata.org/wiki/Q17051537), **g**)
 * 45 Herbei, o ihr Gläub’gen ([WD](https://www.wikidata.org/wiki/Q96252855), **g**, [CPDL](http://www2.cpdl.org/wiki/index.php/Adeste_fideles_(Traditional)))
 * 46 Stille Nacht, heilige Nacht
 * 47 Freu dich, Erd und Sternenzelt
 * 48 Kommet, ihr Hirten
 * 49 Der Heiland ist geboren
 * 50 Du Kind, zu dieser heilgen Zeit
 * 51 Also liebt Gott die arge Welt
 * 52 Wisst ihr noch, wie es geschehen?
 * 53 Als die Welt verloren
 * 54 Hört, der Engel helle Lieder
 * 55 O Bethlehem, du kleine Stadt
 * 56 Weil Gott in tiefster Nacht erschienen
 * 57 Uns wird erzählt von Jesus Christ
 * 58 Nun lasst uns gehn und treten (eG-NA #403)
 * 59 [Das alte Jahr vergangen ist](Lieder/Johann%20Steuerlein%20-%20Das%20alte%20Jahr%20vergangen%20ist.musicxml) (Johann Steuerlein +1613, eG-NA #399)
 * 60 Freut euch, ihr lieben Christen all
 * 61 Hilf, Herr Jesu, lass gelingen (eG-NA #467)
 * 62 [Jesus soll die Losung sein](Lieder/Benjamin%20Schmolck%20-%20Jesus%20soll%20die%20Losung%20sein.musicxml) (Benjamin Schmolck +1737, eG-NA #404)
 * 63 Das Jahr geht still zu Ende
 * 64 Der du die Zeit in Händen hast
 * 65 Von guten Mächten treu und still umgeben
 * 66 Jesus ist kommen, Grund ewiger Freude
 * 67 Herr Christ, der einig Gotts Sohn
 * 68 O lieber Herre Jesu Christ
 * 69 Der Morgenstern ist aufgedrungen
 * 70 Wie schön leuchtet der Morgenstern (eG-NA #300)
 * 71 O König aller Ehren
 * 72 O Jesu Christe, wahres Licht (eG-NA #180)
 * 73 Auf, Seele, auf und säume nicht
 * 74 Du Morgenstern, du Licht vom Licht
 * 75 Ehre sei dir, Christe
 * 76 O Mensch, bewein dein Sünde groß
 * 77 Christus, der uns selig macht
 * 78 Jesu Kreuz, Leiden und Pein
 * 79 Wir danken dir, Herr Jesu Christ, dass du für uns gestorben (eG-NA #95)
 * 80 O Traurigkeit, o Herzeleid!
 * 81 Herzliebster Jesu, was hast du verbrochen (eG-NA #84)
 * 82 Wenn meine Sünd’ mich kränken (eG-NA #96)
 * 83 Ein Lämmlein geht und trägt die Schuld (eG-NA #85)
 * 84 O Welt, sieh hier dein Leben (eG-NA #89)
 * 85 O Haupt voll Blut und Wunden (eG-NA #86)
 * 86 Jesu, meines Lebens Leben (eG-NA #97)
 * 87 Du großer Schmerzensmann
 * 88 Jesu, deine Passion will ich jetzt bedenken (Vulpius)
 * 89 Herr Jesu, deine Angst und Pein
 * 90 Ich grüße dich am Kreuzesstamm
 * 91 Herr, stärke mich, dein Leiden zu bedenken
 * 92 Christe, du Schöpfer aller Welt
 * 93 Nun gehören unsre Herzen
 * 94 Das Kreuz ist aufgerichtet
 * 95 Seht hin, er ist allein im Garten
 * 96 Du schöner Lebensbaum
 * 97 Holz auf Jesu Schulter
 * 98 Korn, das in die Erde
 * 99 Christ ist erstanden (eG-NA #107)
 * 100 Wir wollen alle fröhlich sein
 * 101 Christ lag in Todesbanden
 * 102 Jesus Christus, unser Heiland, der den Tod überwand
 * 103 Gelobt sei Gott im höchsten Thron
 * 104 Singen wir heut mit einem Mund
 * 105 Erstanden ist der heilig Christ
 * 106 Erschienen ist der herrlich Tag
 * 107 Wir danken dir, Herr Jesu Christ, dass du vom Tod erstanden (ev-na #95)
 * 108 Mit Freuden zart zu dieser Fahrt
 * 109 Heut triumphieret Gottes Sohn
 * 110 Die ganze Welt, Herr Jesu Christ
 * 111 Frühmorgens, da die Sonn aufgeht (eG-NA #108)
 * 112 Auf, auf, mein Herz, mit Freuden
 * 113 O Tod, wo ist dein Stachel nun? (eG-NA #109)
 * 114 Wach auf, mein Herz, die Nacht ist hin (eG-NA #110)
 * 115 Jesus lebt, mit ihm auch ich (eG-NA #115)
 * 116 Er ist erstanden, Halleluja
 * 117 Der schöne Ostertag
 * 118 Der Herr ist auferstanden (Marx)
 * 119 Gen Himmel aufgefahren ist
 * 120 Christ fuhr gen Himmel
 * 121 Wir danken dir, Herr Jesu Christ, dass du gen Himmel g’fahren (eG-NA #95)
 * 122 Auf Christi Himmelfahrt allein
 * 123 Jesus Christus herrscht als König (eG-NA #132)
 * 124 Nun bitten wir den Heiligen Geist (eG-NA #139)
 * 125 Komm, Heiliger Geist, Herre Gott (eG-NA #138)
 * 126 Komm, Gott Schöpfer, Heiliger Geist
 * 127 Jauchz, Erd, und Himmel, juble hell
 * 128 Heilger Geist, du Tröster mein
 * 129 Freut euch, ihr Christen alle, Gott schenkt uns seinen Sohn
 * 130 O Heilger Geist, kehr bei uns ein (eG-NA #142)
 * 131 O heiliger Geist, o heiliger Gott (eG-NA #140)
 * 132 Ihr werdet die Kraft des heiligen Geistes empfangen
 * 133 Zieh ein zu deinen Toren (eG-NA #141)
 * 134 Komm, o komm, du Geist des Lebens (eG-NA #144)
 * 135 Schmückt das Fest mit Maien (Witt) (eG-NA #145)
 * 136 O komm, du Geist der Wahrheit (eG-NA #199)
 * 137 Geist des Glaubens (eG-NA #151)
 * 138 Gott der Vater steh uns bei (eG-NA #154)
 * 139 Gelobet sei der Herr, mein Gott (eG-NA #155)
 * 140 Brunn alles Heils, dich ehren wir (eG-NA #156)
 * 141 Wir wollen singn ein’ Lobgesang
 * 142 Gott, aller Schöpfung
 * 143 Heut singt die liebe Christenheit
 * 144 Aus tiefer Not lasst uns zu Gott
 * 145 Wach auf, wach auf, du deutsches Land
 * 146 Nimm von uns, Herr, du treuer Gott
 * 147 Wachet auf, ruft uns die Stimme (eG-NA #134)
 * 148 Herzlich tut mich erfreuen (eG-NA #493)
 * 149 Es ist gewisslich an der Zeit (eG-NA #519)
 * 150 Jerusalem, du hochgebaute Stadt (eG-NA #524)
 * 151 Ermuntert euch, ihr Frommen (Wittenberg) (eG-NA #135)
 * 152 Wir warten dein, o Gottes Sohn (eG-NA #136)
 * 153 Der Himmel, der ist, ist nicht der Himmel, der kommt
 * 154 Herr, mach uns stark
 * 155 Herr Jesu Christ, dich zu uns wend (eG-NA #2)
 * 156 Komm, Heiliger Geist, erfüll die Herzen
 * 157 Lass mich dein sein und bleiben (eG-NA #253)
 * 158 O Christe, Morgensterne
 * 159 Fröhlich wir nun all fangen an
 * 160 Gott Vater, dir sei Dank gesagt
 * 161 [Liebster Jesu, wir sind hier, dich und dein Wort anzuhören](Lieder/Tobias%20Clausnitzer%20-%20Liebster%20Jesu,%20wir%20sind%20hier%20Melodie%20+%20Orgel.musicxml) (Tobias Clausnitzer +1684, eG-NA #3, eC-P #27)
 * 162 Gott Lob, der Sonntag kommt herbei (eG-NA #211)
 * 163 Unsern Ausgang segne Gott
 * 164 Jesu, stärke deine Kinder
 * 165 Gott ist gegenwärtig (eG-NA #6)
 * 166 Tut mir auf die schöne Pforte (eG-NA #4)
 * 167 Wir wollen fröhlich singen Gott
 * 168 Du hast uns, Herr, gerufen
 * 169 Der Gottesdienst soll fröhlich sein
 * 170 Komm, Herr, segne uns
 * 171 Bewahre uns, Gott
 * 172 Sende dein Licht
 * 173 Der Herr behüte deinen Ausgang und Eingang
 * 174 Es segne und behüte uns Gott
 * 175 Ausgang und Eingang
 * 177 .1 Ehr sei dem Vater (Soest)
 * 177 .2 Ehr sei dem Vater (Bayern)
 * 177 .3 Ehr sei dem Vater (Wiese)
 * 178 .1 Kyrie eleison (Gregorianisch)
 * 178 .2 Kyrie eleison (Straßburg)
 * 178 .3 Kyrie eleison (Luther 1526)
 * 178 .4 Kyrie, Gott Vater in Ewigkeit
 * 178 .5 Herr, erbarme dich (Rohr)
 * 178 .6 Tau aus Himmelshöhn
 * 178 .7 Der am Kreuze starb
 * 178 .8 Send uns deine Geist
 * 178 .9 Kyrie eleison (Ukraine)
 * 178 .10 Herr, erbarme dich (Seuffert)
 * 178 .11 Herr, erbarme dich (Janssens)
 * 178 .12 Kyrie eleison (Taizé 1)
 * 178 .13 Kyrie eleison (Weiss)
 * 178 .14 Kyrie eleison (Beuerle)
 * 179 Allein Gott in der Höh sei Ehr (eG-NA #1)
 * 180 .1 Ehre sei Gott in der Höhe (Straßburg)
 * 180 .2 Gott in der Höh sei Preis und Ehr
 * 180 .3 Ehre sei Gott in der Höhe (Schweden)
 * 180 .4 Allein Gott in der Höh sei Ehr (Kanon) (eG-NA #1)
 * 181 .1 Halleluja (Gregorianisch)
 * 181 .2 Halleluja (Gregorianisch)
 * 181 .3 Halleluja (Gregorianisch)
 * 181 .4 Halleluja (Orthodox)
 * 181 .5 Halleluja (Maraire)
 * 181 .6 Laudate omnes gentes
 * 181 .7 Jubilate Deo (Praetorius)
 * 181 .8 Halleluja (mündl. überliefert)
 * 182 Halleluja: Suchet zuerst Gottes Reich
 * 183 Wir glauben all an einen Gott (eG-NA #153)
 * 184 Wir glauben Gott im höchsten Thron
 * 185 .1 Heilig, heilig, heilig (Neuenrade)
 * 185 .2 Heilig, heilig, heilig (Gregorianisch)
 * 185 .3 Heilig, heilig, heilig (Steinau)
 * 185 .4 Agios o Theos
 * 185 .5 Sanctus (Kanon)
 * 186 Vater unser (Gregorianische Melodie)
 * 187 Vater unser (Frankfurt/ Main)
 * 188 Vater unser, Vater im Himmel
 * 189 Geheimnis des Glaubens (Syrien)
 * 190 .1 O Lamm Gottes, unschuldig (eG-NA #83)
 * 190 .2 Christe, du Lamm Gottes (eG-NA #240)
 * 190 .3 Lamm Gottes, du nimmst hinweg
 * 190 .4 Siehe, das ist Gottes Lamm (Schweizer)
 * 192 Kyrie eleison (Luther 1528)
 * 193 Erhalt uns, Herr, bei deinem Wort (eG-NA #10)
 * 194 O Gott, du höchster Gnadenhort
 * 195 Allein auf Gottes Wort
 * 196 Herr, für dein Wort sei hoch gepreist
 * 197 Herr, öffne mir die Herzenstür
 * 198 Herr, dein Wort, die edle Gabe (Halle) (eG-NA #208)
 * 199 Gott hat das erste Wort
 * 200 Ich bin getauft auf deinen Namen (eG-NA #233)
 * 201 Gehet hin in alle Welt
 * 202 Christ unser Herr zum Jordan kam
 * 203 Ach lieber Herre Jesu Christ (Königsberg)
 * 204 Herr Christ, dein bin ich eigen
 * 205 Gott Vater, höre unsre Bitt
 * 206 Liebster Jesu, wir sind hier, deinem Worte nachzuleben (eG-NA #222)
 * 207 Nun schreib ins Buch des Lebens
 * 208 Gott Vater, du hast deinen Namen
 * 209 Ich möcht’, dass einer mit mir geht
 * 210 Du hast mich, Herr, zu dir gerufen
 * 211 Gott, der du alles Leben schufst
 * 212 Voller Freude über dieses Wunder
 * 213 Kommt her, ihr seid geladen
 * 214 Gott sei gelobet und gebenedeiet
 * 215 Jesus Christus, unser Heiland, der von uns den Gotteszorn wandt
 * 216 Du hast uns Leib und Seel gespeist
 * 217 Herr Jesu Christe, mein getreuer Hirte
 * 218 Schmücke dich, o liebe Seele (eG-NA #241)
 * 219 Herr Jesu Christ, du höchstes Gut ... wir kommen, deinen Lei
 * 220 Herr, du wollest uns bereiten (eG-NA #238)
 * 221 Das sollt ihr, Jesu Jünger, nie vergessen
 * 222 Im Frieden dein, o Herre mein
 * 223 Das Wort geht von dem Vater aus
 * 224 Du hast zu deinem Abendmahl (Veigel)
 * 225 Komm, sag es allen weiter
 * 226 Seht, das Brot, das wir hier teilen
 * 227 Dank sei dir, Vater, für das ewge Leben
 * 228 Er ist das Brot, er ist der Wein (Schwarz)
 * 229 Kommt mit Gaben und Lobgesang
 * 230 Schaffe in mir, Gott (eG-NA #267)
 * 231 Dies sind die heilgen zehn Gebot
 * 232 Allein zu dir, Herr Jesu Christ (eG-NA #262)
 * 233 Ach Gott und Herr, wie groß und schwer (eG-NA #264)
 * 234 So wahr ich lebe, spricht dein Gott (eG-NA #274)
 * 235 O Herr, nimm unsre Schuld
 * 236 Ohren gabst du mir (Petzold)
 * 237 Und suchst du meine Sünde
 * 238 Herr, vor dein Antlitz treten zwei
 * 239 Freuet euch im Herren
 * 240 Du hast uns, Herr, in dir verbunden
 * 241 Wach auf, du Geist der ersten Zeugen (eG-NA #181)
 * 242 Herr, nun selbst den Wagen halt
 * 243 Lob Gott getrost mit Singen
 * 244 Wach auf, wach auf, ’s ist hohe Zeit
 * 245 Preis, Lob und Dank sei Gott dem Herren
 * 246 Ach bleib bei uns, Herr Jesu Christ (eG-NA #196)
 * 247 [Herr, unser Gott, lass nicht zuschanden werden](Lieder/Johann%20Heermann%20-%20Herr,%20unser%20Gott!%20Lass%20nicht%20zuschanden%20werden.musicxml) (eG-NA #348)
 * 248 Treuer Wächter Israel’
 * 249 Verzage nicht, du Häuflein klein (eG-NA #197)
 * 250 Ich lobe dich von ganzer Seelen
 * 251 Herz und Herz vereint zusammen (eG-NA #174)
 * 252 Jesu, der du bist alleine Haupt und König (eG-NA #173)
 * 253 Ich glaube, dass die Heiligen (eG-NA #175)
 * 254 Wir wolln uns gerne wagen
 * 255 O dass doch bald dein Feuer brennte (eG-NA #182)
 * 256 Einer ist's, an dem wir hangen (eG-NA #186)
 * 257 Der du in Todesnächten erkämpft das Heil der Welt
 * 258 Zieht in Frieden eure Pfade
 * 259 Kommt her, des Königs Aufgebot
 * 260 Gleich wie mich mein Vater gesandt hat
 * 261 Herr, wohin, Herr, wohin sollen wir gehen?
 * 262 Sonne der Gerechtigkeit
 * 263 Sonne der Gerechtigkeit
 * 264 Die Kirche steht gegründet
 * 265 Nun singe Lob, du Christenheit
 * 266 Der Tag, mein Gott, ist nun vergangen
 * 267 Herr, du hast darum gebetet
 * 268 Strahlen brechen viele aus einem Licht
 * 269 Christus ist König
 * 270 Herr, unser Herrscher
 * 271 Wie herrlich gibst du, Herr
 * 272 Ich lobe meinen Gott von ganzem Herzen
 * 273 Ach Gott vom Himmel sieh darein (eG-NA #160)
 * 274 Der Herr ist mein getreuer Hirt (eG-NA #344)
 * 275 In dich hab ich gehoffet, Herr
 * 276 Ich will, solang ich lebe
 * 277 Herr, deine Güte reicht, so weit der Himmel ist (Beuerle)
 * 278 Wie der Hirsch lechzt nach frischem Wasser
 * 279 Jauchzt, alle Lande, Gott zu Ehren
 * 280 Es wolle Gott uns gnädig sein (eG-NA #179)
 * 281 Erhebet er sich, unser Gott
 * 282 Wie lieblich schön, Herr Zebaoth
 * 283 Herr, der du vormals hast dein Land (eG-NA #418)
 * 284 Das ist köstlich, dir zu sagen Lob und Preis!
 * 285 Das ist ein köstlich Ding
 * 286 Singt, singt dem Herren neue Lieder
 * 287 Psalm 98|Singet dem Herrn ein neues Lied, denn er tut Wunder
 * 288 Nun jauchzt dem Herren, alle Welt! (eG-NA #379)
 * 289 Nun lob, mein Seel, den Herren (eG-NA #377)
 * 290 Nun danket Gott, erhebt und preiset
 * 291 Ich will dir danken, Herr
 * 292 Das ist mir lieb
 * 293 Lobt Gott den Herrn, ihr Heiden all
 * 294 Nun saget Dank
 * 295 Wohl denen, die da wandeln
 * 296 Ich heb mein Augen sehnlich auf
 * 297 Wo Gott der Herr nicht bei uns hält (eG-NA #195)
 * 298 Wenn der Herr
 * 299 Aus tiefer Not schrei ich zu dir (eG-NA #261)
 * 300 Lobt Gott, den Herrn der Herrlichkeit
 * 301 Danket Gott, denn er ist gut
 * 302 Du meine Seele, singe, wohlauf und singe schön
 * 303 Lobe den Herren, o meine Seele! (eG-NA #389)
 * 304 Lobet den Herren, denn er ist sehr freundlich
 * 305 Singt das Lied der Freude (Hechtenberg)
 * 306 Singt das Lied der Freude (Bietz)
 * 307 Gedenk an uns, o Herr
 * 308 Mein Seel, o Herr, muss loben dich
 * 309 Hoch hebt den Herrn
 * 310 Meine Seele erhebt den Herren (Ruppel)
 * 311 Abraham, Abraham, verlass dein Land
 * 312 Kam einst zum Ufer
 * 313 Jesus, der zu den Fischern lief
 * 314 Jesus zieht in Jerusalem ein
 * 315 Ich will zu meinem Vater
 * 316 Lobe den Herren, den mächtigen König der Ehren (Ökumenische Textfassung) (eG-NA #384)
 * 317 Lobe den Herren, den mächtigen König der Ehren (eG-NA #384)
 * 318 O gläubig Herz, gebenedei
 * 319 Die beste Zeit im Jahr ist mein
 * 320 Nun lasst uns Gott dem Herren (eG-NA #457)
 * 321 Nun danket alle Gott (eG-NA #378)
 * 322 Nun danket all und bringet Ehr (eG-NA #380)
 * 323 Man lobt dich in der Stille (eG-NA #382)
 * 324 Ich singe dir mit Herz und Mund (eG-NA #27)
 * 325 Sollt ich meinem Gott nicht singen? (eG-NA #381)
 * 326 Sei Lob und Ehr dem höchsten Gut (eG-NA #383)
 * 327 Wunderbarer König, Herrscher von uns allen (eG-NA #385)
 * 328 Dir, dir, o Höchster, will ich singen (eG-NA #388)
 * 329 Bis hierher hat mich Gott gebracht
 * 330 O dass ich tausend Zungen hätte (eG-NA #386)
 * 331 Großer Gott, wir loben dich
 * 332 Lobt froh den Herrn
 * 333 Danket dem Herrn! Wir danken dem Herrn
 * 334 Danke für diesen guten Morgen
 * 335 Ich will den Herrn loben allezeit
 * 336 Danket, danket dem Herrn
 * 337 Lobet und preiset, ihr Völker
 * 338 Alte mit den Jungen
 * 339 Mein Herz ist bereit, Gott
 * 340 Ich will dem Herrn singen mein Leben lang
 * 341 Nun freut euch, lieben Christen g’mein (eG-NA #39)
 * 342 Es ist das Heil uns kommen her (eG-NA #273)
 * 343 Ich ruf zu dir, Herr Jesu Christ (eG-NA #252)
 * 344 Vater unser im Himmelreich
 * 345 Auf meinen lieben Gott trau ich (eG-NA #350)
 * 346 Such, wer da will, ein ander Ziel (eG-NA #302)
 * 347 Ach bleib mit deiner Gnade (eG-NA #11)
 * 348 Gott verspricht: Ich will dich segnen
 * 349 Ich freu mich in dem Herren
 * 350 Christi Blut und Gerechtigkeit (eG-NA #289)
 * 351 Ist Gott für mich, so trete|Ist Gott für mich, so trete gleich alles wider mich (eG-NA #353)
 * 352 Alles ist an Gottes Segen (eG-NA #359)
 * 353 Jesus nimmt die Sünder an (eG-NA #237)
 * 354 Ich habe nun den Grund gefunden (eG-NA #282)
 * 355 Mir ist Erbarmung widerfahren (eG-NA #286)
 * 356 Es ist in keinem andern Heil
 * 357 Ich weiß, woran ich glaube
 * 358 Es kennt der Herr die Seinen
 * 359 In dem Herren freuet euch
 * 360 Die ganze Welt hast du uns überlassen
 * 361 Befiehl du deine Wege (eG-NA #352)
 * 362 [Ein feste Burg ist unser Gott](Lieder/Martin%20Luther%20-%20Ein'%20feste%20Burg%20ist%20unser%20Gott%20Melodie%20+%20Orgel.musicxml) (Martin Luther +1546, eG-NA #194, eC-P #4)
 * 363 Kommt her zu mir, spricht Gottes Sohn
 * 364 Was mein Gott will, das g’scheh allzeit|Was mein Gott will, gescheh allzeit (eG-NA #343)
 * 365 Von Gott will ich nicht lassen (eG-NA #345)
 * 366 Wenn wir in höchsten Nöten sein (eG-NA #342)
 * 367 Herr, wie du willst, so schick's mit mir (eG-NA #254)
 * 368 In allen meinen Taten (eG-NA #355)
 * 369 [Wer nur den lieben Gott lässt walten](Lieder/Georg%20Neumark%20-%20Wer%20nur%20den%20lieben%20Gott%20lässt%20walten%20Melodie%20+%20Orgel.musicxml) (Georg Neumark +1681, eG-NA #356, eC-P #19, andere Fassung eC-P #76)
 * 370 Warum sollt ich mich denn grämen (eG-NA #354)
 * 371 Gib dich zufrieden
 * 372 Was Gott tut, das ist wohlgetan (eG-NA #358 od. 411)
 * 373 Jesu, hilf siegen (eG-NA #335)
 * 374 Ich steh in meines Herren Hand (eG-NA #375)
 * 375 Dass Jesus siegt, bleibt ewig ausgemacht (Ahle)
 * 376 So nimm denn meine Hände
 * 377 Zieh an die Macht
 * 378 Es mag sein, dass alles fällt
 * 379 Gott wohnt in einem Lichte (Straßburg)
 * 380 Ja, ich will euch tragen
 * 381 Gott, mein Gott, warum hast du mich verlassen
 * 382 Ich steh vor dir
 * 383 Herr, du hast mich angerührt
 * 384 Lasset uns mit Jesus ziehen
 * 385 Mir nach, spricht Christus, unser Held (eG-NA #75)
 * 386 Eins ist not! Ach Herr, dies Eine (eG-NA #312)
 * 387 Mache dich, mein Geist, bereit (eG-NA #339)
 * 388 O Durchbrecher aller Bande (eG-NA #334)
 * 389 Ein reines Herz, Herr, schaff in mir (eG-NA #331)
 * 390 Erneure mich, o ewigs Licht
 * 391 Jesu, geh voran auf der Lebensbahn (eG-NA #79)
 * 392 Gott rufet noch. Sollt ich nicht endlich hören?
 * 393 Kommt, Kinder, lasst uns gehen (eG-NA #480)
 * 394 Nun aufwärts froh den Blick gewandt
 * 395 Vertraut den neuen Wegen
 * 396 Jesu, meine Freude (eG-NA #305)
 * 397 Herzlich lieb hab ich dich, o Herr (eG-NA #299)
 * 398 In dir ist Freude in allem Leide
 * 399 O Lebensbrünnlein tief und groß
 * 400 Ich will dich lieben, meine Stärke (eG-NA #307)
 * 401 Liebe, die du mich zum Bilde deiner Gottheit hast gemacht (eG-NA #306)
 * 402 Meinen Jesus lass ich nicht (eG-NA #304)
 * 403 Schönster Herr Jesu
 * 404 Herr Jesu, Gnadensonne (eG-NA #257)
 * 405 Halt im Gedächtnis Jesus Christ (eG-NA #311)
 * 406 Bei dir, Jesu, will ich bleiben (eG-NA #233)
 * 407 Stern, auf den ich schaue
 * 408 Meinem Gott gehört die Welt
 * 409 Gott liebt diese Welt
 * 410 Christus, das Licht der Welt
 * 411 Gott, weil er groß ist
 * 412 So jemand spricht: Ich liebe Gott
 * 413 Ein wahrer Glaube Gotts Zorn stillt
 * 414 Lass mich, o Herr, in allen Dingen
 * 415 Liebe, du ans Kreuz für uns erhöhte
 * 416 Gebet des heiligen Franziskus|O Herr, mach mich zu einem Werkzeug deines Friedens
 * 417 Lass die Wurzel unsers Handelns
 * 418 Brich dem Hungrigen dein Brot (Häussler)
 * 419 Hilf, Herr meines Lebens
 * 420 Brich mit den Hungrigen dein Brot
 * 421 Verleih uns Frieden gnädiglich
 * 422 Du Friedefürst, Herr Jesu Christ
 * 423 Herr, höre, Herr, erhöre (eG-NA #258)
 * 424 Deine Hände, großer Gott
 * 425 Gib uns Frieden jeden Tag
 * 426 Es wird sein in den letzten Tagen
 * 427 Solange es Menschen gibt
 * 428 Komm in unsre stolze Welt
 * 429 Lobt und preist die herrlichen Taten
 * 430 Gib Frieden, Herr
 * 431 Gott, unser Ursprung, Herr des Raums
 * 432 Gott gab uns Atem (Baltruweit)
 * 433 Hevenu shalom alechem
 * 434 Shalom chaverim
 * 435 Dona nobis pacem (Kanon)|Dona nobis pacem (unbekannt)
 * 436 Herr, gib uns deinen Frieden
 * 437 Die helle Sonn leucht’ jetzt herfür (eG-NA #428)
 * 438 Der Tag bricht an und zeiget sich
 * 439 Es geht daher des Tages Schein
 * 440 All Morgen ist ganz frisch und neu
 * 441 Du höchstes Licht, du ewger Schein
 * 442 Steht auf, ihr lieben Kinderlein
 * 443 Aus meines Herzens Grunde (eG-NA #427)
 * 444 Die güldene Sonne bringt Leben und Wonne
 * 445 Gott des Himmels und der Erden (eG-NA #429)
 * 446 Wach auf, mein Herz, und singe (eG-NA #430)
 * 447 Lobet den Herren alle, die ihn ehren
 * 448 Lobet den Herren alle, die ihn ehren (Kanon nach Nr. 447)
 * 449 Die güldne Sonne voll Freud und Wonne (eG-NA #431)
 * 450 Morgenglanz der Ewigkeit (eG-NA #432)
 * 451 Mein erst Gefühl sei Preis und Dank (eG-NA #438)
 * 452 Er weckt mich alle Morgen
 * 453 Schon bricht des Tages Glanz hervor
 * 454 Auf, und macht die Herzen weit
 * 455 Morgenlicht leuchtet
 * 456 Vom Aufgang der Sonne
 * 457 Der Tag ist seiner Höhe nah
 * 458 Wir danken Gott für seine Gaben (Davantes) (eG-NA #458)
 * 459 Die Sonn hoch an dem Himmel steht
 * 460 Lobet den Herrn und dankt ihm seine Gaben
 * 461 Aller Augen warten auf dich
 * 462 Wir danken dir, Herr Jesu Christ, dass du unser Gast gewesen (eG-NA #95)
 * 463 Alle guten Gaben
 * 464 Herr, gib uns unser täglich Brot
 * 465 Komm, Herr Jesu, sei du unser Gast
 * 466 Segne, Herr, was deine Hand
 * 467 Hinunter ist der Sonne Schein (eG-NA #441)
 * 468 Ach lieber Herre Jesu Christ (Straßburg)
 * 469 Christe, du bist der helle Tag (eG-NA #440)
 * 470 Der du bist drei in Einigkeit
 * 471 Die Nacht ist kommen, drin wir ruhen sollen
 * 472 Der Tag hat sich geneiget
 * 473 Mein schönste Zier und Kleinod bist
 * 474 Mit meinem Gott geh ich zur Ruh
 * 475 Werde munter, mein Gemüte (eG-NA #443)
 * 476 Die Sonn hat sich mit ihrem Glanz gewendet
 * 477 Nun ruhen alle Wälder (eG-NA #442)
 * 478 Nun sich der Tag geendet hat und keine Sonn mehr scheint (eG-NA #445)
 * 479 Der lieben Sonne Licht und Pracht (Halle)
 * 480 Nun schläfet man
 * 481 Nun sich der Tag geendet, mein Herz sich zu dir wendet
 * 482 Der Mond ist aufgegangen (eG-NA #449)
 * 483 Herr, bleibe bei uns (Thate)
 * 484 Müde bin ich, geh zur Ruh (eG-NA #450)
 * 485 Du Schöpfer aller Wesen
 * 486 Ich liege, Herr, in deiner Hut
 * 487 Abend ward, bald kommt die Nacht
 * 488 Bleib bei mir, Herr!
 * 489 Gehe ein in deinen Frieden
 * 490 Der Tag ist um, die Nacht kehrt wieder
 * 491 Bevor die Sonne sinkt
 * 492 Ruhet von des Tages Müh (Hesekiel)
 * 493 Eine ruhige Nacht und ein seliges Ende
 * 494 In Gottes Namen fang ich an
 * 495 O Gott, du frommer Gott (eG-NA #255)
 * 496 Lass dich, Herr Jesu Christ
 * 497 Ich weiß, mein Gott, dass all mein Tun
 * 498 In Gottes Namen fahren wir
 * 499 Erd und Himmel sollen singen
 * 500 Lobt Gott in allen Landen
 * 501 Wie lieblich ist der Maien
 * 502 Nun preiset alle Gottes Barmherzigkeit!
 * 503 Geh aus, mein Herz, und suche Freud (eG-NA #394)
 * 504 Himmel, Erde, Luft und Meer (eG-NA #26)
 * 505 Die Ernt ist nun zu Ende
 * 506 Wenn ich, o Schöpfer, deine Macht (eG-NA #30)
 * 507 Himmelsau, licht und blau|Himmels Au, licht und blau
 * 508 Wir pflügen und wir streuen
 * 509 Kein Tierlein ist auf Erden dir
 * 510 Freuet euch der schönen Erde (eG-NA #396)
 * 511 Weißt du, wieviel Sternlein stehen
 * 512 Herr, die Erde ist gesegnet
 * 513 Das Feld ist weiß; vor ihrem Schöpfer neigen
 * 514 Gottes Geschöpfe, kommt zuhauf
 * 516 Christus der ist mein Leben (eG-NA #496)
 * 517 Ich wollt, dass ich daheime wär
 * 518 Mitten wir im Leben sind mit dem Tod umfangen (eG-NA #486)
 * 519 Mit Fried und Freud ich fahr dahin
 * 520 Nun legen wir den Leib
 * 521 O Welt, ich muss dich lassen (eG-NA #487)
 * 522 Wenn mein Stündlein vorhanden ist (eG-NA #490)
 * 523 Valet will ich dir geben (eG-NA #492)
 * 524 Freu dich sehr, o meine Seele (eG-NA #495)
 * 525 Mach's mit mir, Gott, nach deiner Güt (eG-NA #494)
 * 526 Jesus, meine Zuversicht (eG-NA #515)
 * 527 Die Herrlichkeit der Erden
 * 528 Ach wie flüchtig, ach wie nichtig
 * 529 Ich bin ein Gast auf Erden (eG-NA #478)
 * 530 Wer weiß, wie nahe mir mein Ende! (eG-NA #499)
 * 531 Noch kann ich es nicht fassen
 * 532 Nun sich das Herz von allem löste
 * 533 Du kannst nicht tiefer fallen
 * 534 Herr, lehre uns, dass wir sterben müssen
 * 535 Gloria sei dir gesungen

## Regionale Teile

Die Lieder dieser Sammlung sind nach Titel sortiert, damit leichter
nachvollziehbar ist, welche Lieder mehrfach vorkommen. Neben dem Titel ist
auch die Ausgabe und die dort verwendete Liednummer verzeichnet.

 * 673  (Baden/Elsass/Lothringen) Abend ist es Herr; die Stunde
 * 673  (Pfalz) Abend ist es Herr; die Stunde
 * 662  (Österreich) Ach Gott, die armen Kinder dein
 * 627  (Österreich) Ach Gott, verlass mich nicht
 * 619  (Österreich) Ach Herre Gott, wir Kinder dein
 * 648  (Württemberg) Ach komm, füll unsre Seelen ganz
 * 656  (Baden/Elsass/Lothringen) Ach komm, füll unsre Seelen ganz
 * 656  (Pfalz) Ach komm, füll unsre Seelen ganz
 * 636  (Baden/Elsass/Lothringen) Ach lass mich weise werden
 * 636  (Pfalz) Ach lass mich weise werden
 * 563  (Nordelbien) Ach leewe Herre Jesus Christ
 * 545  (Württemberg) Ach mein Herr Jesu, wenn ich dich nicht hätte
 * 660  (Niedersachsen/Bremen) Agnus Dei
 * 660  (Oldenburg) Agnus Dei
 * 626  (Baden/Elsass/Lothringen) Allein Gott in der Höh sei Ehr (eG-NA #1)
 * 626  (Pfalz) Allein Gott in der Höh sei Ehr (eG-NA #1)
 * 633  (Baden/Elsass/Lothringen) Alle Knospen springen auf
 * 633  (Pfalz) Alle Knospen springen auf
 * 637  (Hessen-Nassau) Alle Knospen springen auf
 * 637  (Kurhessen-Waldeck) Alle Knospen springen auf
 * 668.3  (Nordelbien) Alleluja
 * 642  (Nordelbien) Alle Menschen müssen sterben (eG-NA #498)
 * 667  (Österreich) Alle Menschen müssen sterben (eG-NA #498)
 * 686  (Baden/Elsass/Lothringen) Alle Menschen müssen sterben (eG-NA #498)
 * 686  (Pfalz) Alle Menschen müssen sterben (eG-NA #498)
 * 694  (reformierte) Alle Menschen müssen sterben (eG-NA #498)
 * 694  (Rheinland/Westfalen/Lippe) Alle Menschen müssen sterben (eG-NA #498)
 * 605  (Württemberg) Alles auf Erden hat seine Zeit
 * 596  (Baden/Elsass/Lothringen) Alles hast du mir vergeben
 * 596  (Pfalz) Alles hast du mir vergeben
 * 543  (Hessen-Nassau) Alles ist eitel, du aber bleibst
 * 543  (Kurhessen-Waldeck) Alles ist eitel, du aber bleibst
 * 559  (Württemberg) Alles ist eitel, du aber bleibst
 * 647  (reformierte) Alles ist eitel, du aber bleibst
 * 647  (Rheinland/Westfalen/Lippe) Alles ist eitel, du aber bleibst
 * 693  (reformierte) Alles, was Odem hat
 * 693  (Rheinland/Westfalen/Lippe) Alles, was Odem hat
 * 621  (Baden/Elsass/Lothringen) Alles, was Odem hat, lobe den Herrn
 * 621  (Pfalz) Alles, was Odem hat, lobe den Herrn
 * 641  (reformierte) Alles, was Odem hat, lobe den Herrn
 * 641  (Rheinland/Westfalen/Lippe) Alles, was Odem hat, lobe den Herrn
 * 608  (Hessen-Nassau) Alles, was wir sind, hat Gott geschenkt
 * 608  (Kurhessen-Waldeck) Alles, was wir sind, hat Gott geschenkt
 * 631  (Bayern/Thüringen) All eure Sorgen
 * 635  (Baden/Elsass/Lothringen) All Morgen ist ganz frisch und neu
 * 635  (Pfalz) All Morgen ist ganz frisch und neu
 * 543  (Nordelbien) Als ich bei meinen Schafen wacht
 * 553  (Österreich) Als ich bei meinen Schafen wacht
 * 607  (Österreich) Als Israel in Ägypten war
 * 548  (Bayern/Thüringen) Als Jesus auf die Erde kam
 * 549  (Niedersachsen/Bremen) Am Abend nach dem Lobgesang
 * 549  (Oldenburg) Am Abend nach dem Lobgesang
 * 544  (Mecklenburg) Am Abend steigt unser Gebet
 * 572  (Österreich) Amen, Amen
 * 558  (Nordelbien) Amen, Gott Vater und Sohne
 * 554  (Niedersachsen/Bremen) Am hellen Tag kam Jesu Geist
 * 554  (Oldenburg) Am hellen Tag kam Jesu Geist
 * 566  (Bayern/Thüringen) Am hellen Tag kam Jesu Geist
 * 569  (Baden/Elsass/Lothringen) Am hellen Tag kam Jesu Geist
 * 569  (Pfalz) Am hellen Tag kam Jesu Geist
 * 567  (reformierte) Am Pfingsttag unter Sturmgebraus
 * 567  (Rheinland/Westfalen/Lippe) Am Pfingsttag unter Sturmgebraus
 * 591  (Nordelbien) Asante sana Yesu: Wir danken dir, Herr Jesu (eG-NA #95)
 * 654  (Österreich) Auf, auf, den Herrn zu loben
 * 536  (Niedersachsen/Bremen) Auf, auf, ihr Christen alle (variante eG-NA #48)
 * 536  (Oldenburg) Auf, auf, ihr Christen alle (variante eG-NA #48)
 * 536  (reformierte) Auf, auf, ihr Christen alle (variante eG-NA #48)
 * 536  (Rheinland/Westfalen/Lippe) Auf, auf, ihr Christen alle (variante eG-NA #48)
 * 536  (Württemberg) Auf, auf, ihr Christen alle (variante eG-NA #48)
 * 538  (Nordelbien) Auf, auf, ihr Christen alle (variante eG-NA #48)
 * 542  (Baden/Elsass/Lothringen) Auf, auf, ihr Christen alle (variante eG-NA #48)
 * 542  (Pfalz) Auf, auf, ihr Christen alle (variante eG-NA #48)
 * 616  (Hessen-Nassau) Auf der Spur des Hirten
 * 616  (Kurhessen-Waldeck) Auf der Spur des Hirten
 * 552  (Württemberg) Auf diesen Tag bedenken wir
 * 565  (reformierte) Auf diesen Tag bedenken wir
 * 565  (Rheinland/Westfalen/Lippe) Auf diesen Tag bedenken wir
 * 561  (Baden/Elsass/Lothringen) Auferstanden, auferstanden (eG-NA #119)
 * 561  (Pfalz) Auferstanden, auferstanden (eG-NA #119)
 * 678  (Württemberg) Auferstehn, ja auferstehn wirst du (eG-NA #517)
 * 687  (Baden/Elsass/Lothringen) Auferstehn, ja auferstehn wirst du (eG-NA #517)
 * 687  (Pfalz) Auferstehn, ja auferstehn wirst du (eG-NA #517)
 * 567  (Niedersachsen/Bremen) Aufgetan ist die Welt
 * 567  (Oldenburg) Aufgetan ist die Welt
 * 602  (Württemberg) Auf, Seele, Gott zu loben
 * 690  (reformierte) Auf, Seele, Gott zu loben
 * 690  (Rheinland/Westfalen/Lippe) Auf, Seele, Gott zu loben
 * 600  (Niedersachsen/Bremen) Aus der Tiefe, Herr und Gott
 * 600  (Oldenburg) Aus der Tiefe, Herr und Gott
 * 597  (Niedersachsen/Bremen) Aus der Tiefe rufe ich zu dir
 * 597  (Oldenburg) Aus der Tiefe rufe ich zu dir
 * 629  (Bayern/Thüringen) Aus der Tiefe rufe ich zu dir
 * 655  (reformierte) Aus der Tiefe rufe ich zu dir
 * 655  (Rheinland/Westfalen/Lippe) Aus der Tiefe rufe ich zu dir
 * 619  (Bayern/Thüringen) Aus Gnaden soll ich selig werden (eG-NA #291)
 * 620  (Österreich) Aus Gnaden soll ich selig werden (eG-NA #291)
 * 646  (Württemberg) Aus Gottes guten Händen
 * 578  (Niedersachsen/Bremen) Aus meines Jammers Tiefe
 * 578  (Oldenburg) Aus meines Jammers Tiefe
 * 540  (Hessen-Nassau) Aus tausend Traurigkeiten
 * 540  (Kurhessen-Waldeck) Aus tausend Traurigkeiten
 * 542  (Niedersachsen/Bremen) Aus tausend Traurigkeiten
 * 542  (Oldenburg) Aus tausend Traurigkeiten
 * 578  (Hessen-Nassau) Aus ungewissen Pfaden
 * 578  (Kurhessen-Waldeck) Aus ungewissen Pfaden
 * 592  (Baden/Elsass/Lothringen) Bau dein Reich in dieser Zeit
 * 592  (Pfalz) Bau dein Reich in dieser Zeit
 * 650  (Hessen-Nassau) Begrabt den Leib in seine Gruft
 * 650  (Kurhessen-Waldeck) Begrabt den Leib in seine Gruft
 * 658  (Niedersachsen/Bremen) Benedictus, qui venit
 * 658  (Oldenburg) Benedictus, qui venit
 * 666  (Württemberg) Bescher uns, Herr, das täglich Brot (eG-NA #454)
 * 671  (Baden/Elsass/Lothringen) Bescher uns, Herr, das täglich Brot (eG-NA #454)
 * 671  (Pfalz) Bescher uns, Herr, das täglich Brot (eG-NA #454)
 * 553  (Hessen-Nassau) Besiegt hat Jesus Tod und Nacht
 * 553  (Kurhessen-Waldeck) Besiegt hat Jesus Tod und Nacht
 * 605  (Österreich) Besser als ich mich kenne
 * 614  (Württemberg) Betgemeinde, heilge dich (eG-NA #259)
 * 543  (Mecklenburg) Bevor des Tages Licht vergeht
 * 665  (Bayern/Thüringen) Bevor des Tages Licht vergeht
 * 686  (reformierte) Bevor des Tages Licht vergeht
 * 686  (Rheinland/Westfalen/Lippe) Bevor des Tages Licht vergeht
 * 542  (Württemberg) Bleib bei uns, wenn der Tag entweicht
 * 585  (reformierte) Bleibet hier und wachet mit mir
 * 585  (Rheinland/Westfalen/Lippe) Bleibet hier und wachet mit mir
 * 586  (reformierte) Bleib mit deiner Gnade bei uns
 * 586  (Rheinland/Westfalen/Lippe) Bleib mit deiner Gnade bei uns
 * 689  (Baden/Elsass/Lothringen) Bleibt bei dem, der euretwillen (eG-NA #82)
 * 689  (Pfalz) Bleibt bei dem, der euretwillen (eG-NA #82)
 * 648  (Österreich) Breite segnend deine Hände
 * 562  (Baden/Elsass/Lothringen) Brich an, du hohes Feste
 * 562  (Pfalz) Brich an, du hohes Feste
 * 643  (Niedersachsen/Bremen) Brich herein, süßer Schein
 * 643  (Oldenburg) Brich herein, süßer Schein
 * 671  (Österreich) Brich herein, süßer Schein
 * 680  (Württemberg) Brich herein, süßer Schein
 * 572  (reformierte) Brich herein, süßer Schein selger Ewigkeit
 * 572  (Rheinland/Westfalen/Lippe) Brich herein, süßer Schein selger Ewigkeit
 * 632  (Österreich) Brunnquell aller Liebe
 * 582  (Nordelbien) Cantai ao Senhor: Singt Gott, unserm Herrn
 * 564  (reformierte) Christ, der Herr, ist heut erstanden
 * 564  (Rheinland/Westfalen/Lippe) Christ, der Herr, ist heut erstanden
 * 562  (Österreich) Christ, der Herr, ist heut erstanden. Halleluja
 * 633  (Nordelbien) Christe, der du bist Tag und Licht
 * 621  (Württemberg) Christen erwarten in allerlei Fällen (eG-NA #417)
 * 624  (Bayern/Thüringen) Christen erwarten in allerlei Fällen (eG-NA #417)
 * 612  (Württemberg) Christ ist der Weg, das Licht, die Pfort
 * 620  (Bayern/Thüringen) Christ ist der Weg, das Licht, die Pfort
 * 539  (reformierte) Christum wir sollen loben schon
 * 539  (Rheinland/Westfalen/Lippe) Christum wir sollen loben schon
 * 540  (Nordelbien) Christum wir sollen loben schon
 * 551  (Niedersachsen/Bremen) Christus is opstahn, is ut sien Graff gahn
 * 551  (Oldenburg) Christus is opstahn, is ut sien Graff gahn
 * 549  (Württemberg) Christus ist auferstanden
 * 550  (Niedersachsen/Bremen) Christus ist auferstanden
 * 550  (Oldenburg) Christus ist auferstanden
 * 564  (Baden/Elsass/Lothringen) Christus ist auferstanden
 * 564  (Pfalz) Christus ist auferstanden
 * 559  (reformierte) Christus ist auferstanden.
 * 559  (Rheinland/Westfalen/Lippe) Christus ist auferstanden.
 * 555  (Bayern/Thüringen) Christus ist auferstanden! Freud ist in allen Landen
 * 545  (Nordelbien) Christus ist geboren
 * 560  (Bayern/Thüringen) Christus lebt, drum lasst das Jammern
 * 652  (Hessen-Nassau) Christus spricht: Ich bin die Auferstehung
 * 652  (Kurhessen-Waldeck) Christus spricht: Ich bin die Auferstehung
 * 644  (Niedersachsen/Bremen) Christus spricht: Ich bin die Auferstehung und das Leben
 * 644  (Oldenburg) Christus spricht: Ich bin die Auferstehung und das Leben
 * 548  (Nordelbien) Da Jesus an dem Kreuze stund
 * 541  (Mecklenburg) Damit aus Fremden Freunde werden
 * 612  (Baden/Elsass/Lothringen) Damit aus Fremden Freunde werden
 * 612  (Pfalz) Damit aus Fremden Freunde werden
 * 619  (Niedersachsen/Bremen) Damit aus Fremden Freunde werden
 * 619  (Oldenburg) Damit aus Fremden Freunde werden
 * 639  (Hessen-Nassau) Damit aus Fremden Freunde werden
 * 639  (Kurhessen-Waldeck) Damit aus Fremden Freunde werden
 * 644  (Österreich) Damit aus Fremden Freunde werden
 * 657  (Bayern/Thüringen) Damit aus Fremden Freunde werden
 * 657  (Württemberg) Damit aus Fremden Freunde werden
 * 674  (reformierte) Damit aus Fremden Freunde werden
 * 674  (Rheinland/Westfalen/Lippe) Damit aus Fremden Freunde werden
 * 687  (reformierte) Danke, Herr! Ich will dir danken
 * 687  (Rheinland/Westfalen/Lippe) Danke, Herr! Ich will dir danken
 * 599  (Baden/Elsass/Lothringen) Danket dem Herrn
 * 599  (Pfalz) Danket dem Herrn
 * 676.1  (Nordelbien) Danket dem Herrn
 * 676.2  (Nordelbien) Danket dem Herrn
 * 585  (Nordelbien) Danket dem Herrn, denn er ist freundlich
 * 661.3  (Niedersachsen/Bremen) Danket dem Herrn, denn er ist freundlich, Halleluja
 * 661.3  (Oldenburg) Danket dem Herrn, denn er ist freundlich, Halleluja
 * 605  (Hessen-Nassau) Danket dem Herrn und lobsingt seinem Namen
 * 605  (Kurhessen-Waldeck) Danket dem Herrn und lobsingt seinem Namen
 * 635  (Bayern/Thüringen) Danket dem Schöpfer unsrer Welt
 * 611  (Bayern/Thüringen) Danket Gott, danket Gott
 * 582  (Bayern/Thüringen) Dank sei dir, Gott der Freude
 * 557  (reformierte) Dank sei dir, Herr, durch alle Zeiten
 * 557  (Rheinland/Westfalen/Lippe) Dank sei dir, Herr, durch alle Zeiten
 * 630  (reformierte) Dankt, dankt dem Herrn, jauchzt volIe Chöre
 * 630  (Rheinland/Westfalen/Lippe) Dankt, dankt dem Herrn, jauchzt volIe Chöre
 * 627  (reformierte) Dankt, dankt dem Herrn und ehret
 * 627  (Rheinland/Westfalen/Lippe) Dankt, dankt dem Herrn und ehret
 * 625  (Nordelbien) Da pacem, Domine
 * 626  (Niedersachsen/Bremen) Da pacem, Domine
 * 626  (Oldenburg) Da pacem, Domine
 * 551  (reformierte) Das Jahr geht hin, nun segne du
 * 551  (Rheinland/Westfalen/Lippe) Das Jahr geht hin, nun segne du
 * 550  (Hessen-Nassau) Das könnte den Herren der Welt ja so passen
 * 550  (Kurhessen-Waldeck) Das könnte den Herren der Welt ja so passen
 * 580  (Hessen-Nassau) Dass du mich einstimmen lässt in deinen Jubel
 * 580  (Kurhessen-Waldeck) Dass du mich einstimmen lässt in deinen Jubel
 * 597  (Baden/Elsass/Lothringen) Dass du mich einstimmen lässt in deinen Jubel
 * 597  (Pfalz) Dass du mich einstimmen lässt in deinen Jubel
 * 609  (Württemberg) Dass du mich einstimmen lässt in deinen Jubel
 * 580  (Bayern/Thüringen) Dass du mich einstimmen lässt in deinen Jubel, o Herr
 * 569  (Württemberg) Dass Erde und Himmel dir blühen
 * 606  (Hessen-Nassau) Dass ich springen darf und mich freuen
 * 606  (Kurhessen-Waldeck) Dass ich springen darf und mich freuen
 * 614  (Österreich) Dass ich springen darf und mich freuen
 * 667  (Bayern/Thüringen) Das walte Gott, der helfen kann (eG-NA #452)
 * 675  (Württemberg) Das walte Gott, der helfen kann (eG-NA #452)
 * 679  (Baden/Elsass/Lothringen) Das walte Gott, der helfen kann (eG-NA #452)
 * 679  (Pfalz) Das walte Gott, der helfen kann (eG-NA #452)
 * 579  (Hessen-Nassau) Das Weizenkorn muss sterben
 * 579  (Kurhessen-Waldeck) Das Weizenkorn muss sterben
 * 585  (Württemberg) Das Weizenkorn muss sterben
 * 608  (Niedersachsen/Bremen) Das wünsch ich sehr
 * 608  (Oldenburg) Das wünsch ich sehr
 * 630  (Österreich) Das wünsch ich sehr
 * 683  (Baden/Elsass/Lothringen) Dein ist der Himmel und die Erd
 * 683  (Pfalz) Dein ist der Himmel und die Erd
 * 587  (Bayern/Thüringen) Dein Wort, o Herr, bringt uns zusammen (eG-NA #178)
 * 599  (Österreich) Dem Herrn gehört unsre Erde
 * 614  (reformierte) Dem Herrn gehört unsre Erde
 * 614  (Rheinland/Westfalen/Lippe) Dem Herrn gehört unsre Erde
 * 604  (Bayern/Thüringen) Den Herren will ich loben
 * 597  (Bayern/Thüringen) Dennoch bleib ich stets an dir
 * 673  (Württemberg) Der Abend kommt
 * 645  (Hessen-Nassau) Der Abend kommt, die Sonne sich verdecket
 * 645  (Kurhessen-Waldeck) Der Abend kommt, die Sonne sich verdecket
 * 549  (Nordelbien) Der du, Herr Jesu, ruh und Rast
 * 547  (Hessen-Nassau) Der Eselreiter!
 * 547  (Kurhessen-Waldeck) Der Eselreiter!
 * 618  (Niedersachsen/Bremen) Der Friede, den Gott gibt
 * 618  (Oldenburg) Der Friede, den Gott gibt
 * 674.1  (Nordelbien) Der Friede des Herren sei mit euch allen
 * 661.2  (Niedersachsen/Bremen) Der Friede des Herrn sei mit euch allen
 * 661.2  (Oldenburg) Der Friede des Herrn sei mit euch allen
 * 674.2  (Nordelbien) Der Friede des Herrn sei mit euch allezeit
 * 554  (Württemberg) Der Geist des Herrn erfüllt das All
 * 566  (reformierte) Der Geist des Herrn erfüllt das All
 * 566  (Rheinland/Westfalen/Lippe) Der Geist des Herrn erfüllt das All
 * 556  (Württemberg) Der Geist von Gott weht wie der Wind
 * 670  (Österreich) Der Gerechten Seelen
 * 594  (Nordelbien) Der Glaub ist ein lebendig Kraft
 * 571  (Baden/Elsass/Lothringen) Der Herr bricht ein um Mitternachta (eG-NA #137)
 * 571  (Pfalz) Der Herr bricht ein um Mitternacht (eG-NA #137)
 * 548  (Hessen-Nassau) Der Herr ist auferstanden
 * 548  (Kurhessen-Waldeck) Der Herr ist auferstanden
 * 631  (Württemberg) Der Herr ist gut, in dessen Dienst wir stehn
 * 623  (reformierte) Der Herr ist König, hoch erhöht
 * 623  (Rheinland/Westfalen/Lippe) Der Herr ist König, hoch erhöht
 * 612  (reformierte) Der Herr ist mein getreuer Hirt (eG-NA #344)
 * 612  (Rheinland/Westfalen/Lippe) Der Herr ist mein getreuer Hirt (eG-NA #344)
 * 540  (Mecklenburg) Der Herr ist mein Hirte
 * 574  (Niedersachsen/Bremen) Der Herr ist mein Hirte
 * 574  (Oldenburg) Der Herr ist mein Hirte
 * 578  (Nordelbien) Der Herr ist mein Hirte
 * 579  (Nordelbien) Der Herr ist mein Hirte
 * 595  (Bayern/Thüringen) Der Herr ist mein Hirte
 * 598  (Österreich) Der Herr ist mein Hirte
 * 599  (Württemberg) Der Herr ist mein Hirte
 * 615  (Baden/Elsass/Lothringen) Der Herr ist mein Hirte
 * 615  (Pfalz) Der Herr ist mein Hirte
 * 594  (Bayern/Thüringen) Der Herr, mein Hirte, führet mich
 * 613  (reformierte) Der Herr mein Hirt! So will ich Gott besingen
 * 613  (Rheinland/Westfalen/Lippe) Der Herr mein Hirt! So will ich Gott besingen
 * 563  (Württemberg) Der Herr segne dich und behüte dich
 * 570  (Bayern/Thüringen) Der Herr segne dich und behüte dich
 * 570  (Österreich) Der Herr segne dich und behüte dich
 * 661.1  (Niedersachsen/Bremen) Der Herr sei mit euch
 * 661.1  (Oldenburg) Der Herr sei mit euch
 * 671  (Nordelbien) Der Herr sei mit euch
 * 562  (Bayern/Thüringen) Der Himmel geht über allen auf
 * 588  (Niedersachsen/Bremen) Der Himmel geht über allen auf
 * 588  (Oldenburg) Der Himmel geht über allen auf
 * 594  (Hessen-Nassau) Der Himmel geht über allen auf
 * 594  (Kurhessen-Waldeck) Der Himmel geht über allen auf
 * 611  (reformierte) Der Himmel geht über allen auf
 * 611  (Rheinland/Westfalen/Lippe) Der Himmel geht über allen auf
 * 573.2  (Österreich) Der im Wort uns weist
 * 637  (Nordelbien) Der Lärm verebbt, und die Last wird leichter
 * 676  (Baden/Elsass/Lothringen) Der Lärm verebbt, und die Last wird leichter
 * 676  (Pfalz) Der Lärm verebbt, und die Last wird leichter
 * 538  (Württemberg) Der Tag, der ist so freudenreich
 * 541  (Nordelbien) Der Tag, der ist so freudenreich
 * 546  (Baden/Elsass/Lothringen) Der Tag, der ist so freudenreich
 * 546  (Pfalz) Der Tag, der ist so freudenreich
 * 655  (Österreich) Der Tag ist hin (eG-NA #444)
 * 635  (Niedersachsen/Bremen) Der Tag ist hin; mein Jesu, bei mir bleibe (eG-NA #444)
 * 635  (Oldenburg) Der Tag ist hin; mein Jesu, bei mir bleibe (eG-NA #444)
 * 544  (Hessen-Nassau) Der Weg ist so lang
 * 544  (Kurhessen-Waldeck) Der Weg ist so lang
 * 648  (Hessen-Nassau) Des Jahres schöner Schmuck entweicht (eG-NA #398)
 * 648  (Kurhessen-Waldeck) Des Jahres schöner Schmuck entweicht (eG-NA #398)
 * 558  (Württemberg) Des Menschen Sohn wird kommen
 * 636  (Niedersachsen/Bremen) Des Tages Glanz erloschen ist
 * 636  (Nordelbien) Des Tages Glanz erloschen ist
 * 636  (Oldenburg) Des Tages Glanz erloschen ist
 * 649  (Österreich) Die Erde, die du schufst, war gut
 * 653  (Bayern/Thüringen) Die Erde, die du schufst, war gut
 * 623  (Niedersachsen/Bremen) Die Erde ist des Herrn
 * 623  (Oldenburg) Die Erde ist des Herrn
 * 624  (Niedersachsen/Bremen) Die Erde ist des Herrn
 * 624  (Oldenburg) Die Erde ist des Herrn
 * 634  (Hessen-Nassau) Die Erde ist des Herrn
 * 634  (Kurhessen-Waldeck) Die Erde ist des Herrn
 * 650  (Österreich) Die Erde ist des Herrn
 * 654  (Bayern/Thüringen) Die Erde ist des Herrn
 * 659  (Baden/Elsass/Lothringen) Die Erde ist des Herrn
 * 659  (Pfalz) Die Erde ist des Herrn
 * 659  (Württemberg) Die Erde ist des Herrn
 * 677  (reformierte) Die Erde ist des Herrn
 * 677  (Rheinland/Westfalen/Lippe) Die Erde ist des Herrn
 * 595  (Hessen-Nassau) Die Erde ist des Herrn und was darinnen ist
 * 595  (Kurhessen-Waldeck) Die Erde ist des Herrn und was darinnen ist
 * 677  (Württemberg) Die Ernt ist da, es winkt der Halm
 * 570  (Württemberg) Die Gnade unsers Herrn Jesu Christi
 * 576  (reformierte) Die Gnade unsers Herrn Jesu Christi
 * 576  (Rheinland/Westfalen/Lippe) Die Gnade unsers Herrn Jesu Christi
 * 579  (Baden/Elsass/Lothringen) Die Gnade unsers Herrn Jesu Christi
 * 579  (Pfalz) Die Gnade unsers Herrn Jesu Christi
 * 561  (Hessen-Nassau) Die Gnade unsers Herrn Jesus Christus
 * 561  (Kurhessen-Waldeck) Die Gnade unsers Herrn Jesus Christus
 * 642  (Österreich) Die Güte des Herrn hat kein Ende
 * 547  (Baden/Elsass/Lothringen) Die Herrlichkeit des Herrn bleibe ewiglich
 * 547  (Pfalz) Die Herrlichkeit des Herrn bleibe ewiglich
 * 603  (Österreich) Die Herrlichkeit des Herrn bleibe ewiglich
 * 613  (Bayern/Thüringen) Die Herrlichkeit des Herrn bleibe ewiglich
 * 640  (reformierte) Die Herrlichkeit des Herrn bleibe ewiglich
 * 640  (Rheinland/Westfalen/Lippe) Die Herrlichkeit des Herrn bleibe ewiglich
 * 546  (Bayern/Thüringen) Die Hirten hielten auf dem Feld
 * 667  (Württemberg) Die ihr bei Jesus bleibet
 * 659  (Österreich) Die Nacht ist da. Ich suche deine Nähe
 * 537  (Nordelbien) Die Nacht ist hin, der Tag bricht an
 * 590  (Österreich) Die Sach ist dein, Herr Jesu Christ
 * 593  (Württemberg) Die Sach ist dein, Herr Jesu Christ
 * 606  (Baden/Elsass/Lothringen) Die Sach ist dein, Herr Jesu Christ
 * 606  (Pfalz) Die Sach ist dein, Herr Jesu Christ
 * 675  (Baden/Elsass/Lothringen) Diesen Tag, Herr
 * 675  (Pfalz) Diesen Tag, Herr
 * 671  (Württemberg) Diesen Tag, Herr, leg ich zurück in deine Hände
 * 545  (Niedersachsen/Bremen) Dies ist die Nacht der Engel
 * 545  (Oldenburg) Dies ist die Nacht der Engel
 * 550  (Württemberg) Die Sonne geht auf: Christ ist erstanden
 * 556  (Bayern/Thüringen) Die Sonne geht auf: Christ ist erstanden
 * 573  (Nordelbien) Die Stunde ist da, Jesus Christus
 * 586  (Bayern/Thüringen) Die Väter weihten dieses Haus
 * 661  (Baden/Elsass/Lothringen) Die Vögel unterm Himmel
 * 661  (Pfalz) Die Vögel unterm Himmel
 * 553  (reformierte) Die Weisen aus dem Morgenland
 * 548  (reformierte) Die Weisen sind gegangen
 * 548  (Rheinland/Westfalen/Lippe) Die Weisen sind gegangen
 * 568  (Österreich) Die wir uns allhier beisammen finden
 * 573  (reformierte) Die wir uns allhier beisammen finden
 * 573  (Rheinland/Westfalen/Lippe) Die wir uns allhier beisammen finden
 * 651  (Nordelbien) Dir, ewger Vater droben
 * 667.2  (Nordelbien) Dir Gott im Himmel Preis und Ehr
 * 661  (Nordelbien) Dir, Gott Vater droben
 * 658  (Nordelbien) Dir, Herr, sei Lob und Ehre heut und morgen
 * 664  (Nordelbien) Dir, Hirt und Hüter
 * 659  (Nordelbien) Dir, Vater, der du ewig bist
 * 669  (Baden/Elsass/Lothringen) Du Abglanz aller Herrlichkeiten
 * 669  (Pfalz) Du Abglanz aller Herrlichkeiten
 * 577.3  (Österreich) Du bist das Licht der Welt
 * 623  (Hessen-Nassau) Du bist da, wo Menschen leben
 * 623  (Kurhessen-Waldeck) Du bist da, wo Menschen leben
 * 619  (Württemberg) Du bist der Weg und die Wahrheit und das Leben
 * 575  (Niedersachsen/Bremen) Du bist, Herr, mein Licht und meine Freiheit
 * 575  (Oldenburg) Du bist, Herr, mein Licht und meine Freiheit
 * 668  (Württemberg) Du gabst der Welt das Leben
 * 649  (Hessen-Nassau) Du gibst die Saat und auch die Ernte
 * 649  (Kurhessen-Waldeck) Du gibst die Saat und auch die Ernte
 * 683  (reformierte) Du Glanz aus Gottes Herrlichkeiten
 * 683  (Rheinland/Westfalen/Lippe) Du Glanz aus Gottes Herrlichkeiten
 * 614  (Niedersachsen/Bremen) Du Gott der Liebe, Friedensheld
 * 614  (Oldenburg) Du Gott der Liebe, Friedensheld
 * 592  (Hessen-Nassau) Du Gott stützt mich
 * 592  (Kurhessen-Waldeck) Du Gott stützt mich
 * 630  (Württemberg) Du Gott stützt mich
 * 602  (Hessen-Nassau) Du hast gesagt: »Ich bin der Weg«
 * 602  (Kurhessen-Waldeck) Du hast gesagt: »Ich bin der Weg«
 * 640  (Niedersachsen/Bremen) Du hast uns deine Welt geschenkt
 * 640  (Oldenburg) Du hast uns deine Welt geschenkt
 * 676  (reformierte) Du hast uns deine Welt geschenkt
 * 676  (Rheinland/Westfalen/Lippe) Du hast uns deine Welt geschenkt
 * 612  (Bayern/Thüringen) Du hast uns Deine Welt geschenkt
 * 569  (Nordelbien) Du hast uns, Herr, gerufen
 * 609  (reformierte) Du hast vereint in allen Zonen
 * 609  (Rheinland/Westfalen/Lippe) Du hast vereint in allen Zonen
 * 570  (reformierte) Du, Herr, gabst uns dein festes Wort
 * 570  (Rheinland/Westfalen/Lippe) Du, Herr, gabst uns dein festes Wort
 * 617  (Nordelbien) Du hest hendal di laten to mi
 * 679  (Württemberg) Du kamst, du gingst mit leiser Spur
 * 569  (Bayern/Thüringen) Du neigest dich herab
 * 567  (Nordelbien) Du öffnest, Herr, die Türen
 * 593  (Nordelbien) Durch Adams Fall ist ganz verderbt (eG-NA #38)
 * 592  (Bayern/Thüringen) Du schenkst uns Zeit
 * 652  (Bayern/Thüringen) Du schufst, Herr, unsre Erde gut
 * 654  (Württemberg) Du schufst, Herr, unsre Erde gut
 * 536  (Mecklenburg) Du starker Herrscher, wahrer Gott
 * 663  (Bayern/Thüringen) Du starker Herrscher, wahrer Gott
 * 647  (Württemberg) Du stellst meine Füße auf weiten Raum
 * 583  (Niedersachsen/Bremen) Du tust den Weg des Lebens kund
 * 583  (Oldenburg) Du tust den Weg des Lebens kund
 * 623  (Österreich) Du weißt, mein Herz, du weißt es
 * 554  (Österreich) Du wesentliches Wort
 * 632  (Württemberg) Du Wort des Vaters, rede du
 * 577.1  (Österreich) Ehre sei dir, Herr
 * 577.2  (Österreich) Ehre sei dir, Herr
 * 669.2  (Nordelbien) Ehre sei dir, Herr
 * 669.1  (Nordelbien) Ehre sei dir, Herre
 * 567  (Hessen-Nassau) Ehre sei Gott in der Höhe
 * 567  (Kurhessen-Waldeck) Ehre sei Gott in der Höhe
 * 575.1  (Österreich) Ehre sei Gott in der Höhe
 * 645  (Niedersachsen/Bremen) Ehre sei Gott in der Höhe
 * 645  (Oldenburg) Ehre sei Gott in der Höhe
 * 667.1  (Nordelbien) Ehre sei Gott in der Höhe
 * 645  (Nordelbien) Ehr sei dem Vater
 * 646  (Nordelbien) Ehr sei dem Vater
 * 647  (Nordelbien) Ehr sei Gott Vater
 * 652  (Nordelbien) Ehr sei im Himmel droben
 * 558  (Niedersachsen/Bremen) Eine freudige Nachricht breitet sich aus
 * 558  (Oldenburg) Eine freudige Nachricht breitet sich aus
 * 580  (Württemberg) Eine freudige Nachricht breitet sich aus
 * 649  (Baden/Elsass/Lothringen) Eine freudige Nachricht breitet sich aus
 * 649  (Pfalz) Eine freudige Nachricht breitet sich aus
 * 552  (Hessen-Nassau) Einer ist unser Leben
 * 552  (Kurhessen-Waldeck) Einer ist unser Leben
 * 618  (Nordelbien) Einer muss wachen in langer Nacht
 * 635  (Österreich) Einer muss wachen in langer Nacht
 * 546  (Württemberg) Eines wünsch ich mir vor allem andern (eG-NA #322)
 * 547  (Niedersachsen/Bremen) Eines wünsch ich mir vor allem andern (eG-NA #322)
 * 547  (Oldenburg) Eines wünsch ich mir vor allem andern (eG-NA #322)
 * 554  (reformierte) Eines wünsch ich mir vor allem andern (eG-NA #322)
 * 554  (Rheinland/Westfalen/Lippe) Eines wünsch ich mir vor allem andern (eG-NA #322)
 * 557  (Baden/Elsass/Lothringen) Eines wünsch ich mir vor allem andern (eG-NA #322)
 * 557  (Pfalz) Eines wünsch ich mir vor allem andern (eG-NA #322)
 * 575  (Bayern/Thüringen) Ein Kind ist angekommen
 * 575  (Hessen-Nassau) Ein Kind ist angekommen
 * 575  (Kurhessen-Waldeck) Ein Kind ist angekommen
 * 590  (Baden/Elsass/Lothringen) Ein Kind ist angekommen
 * 590  (Pfalz) Ein Kind ist angekommen
 * 595  (reformierte) Ein Kind ist angekommen
 * 595  (Rheinland/Westfalen/Lippe) Ein Kind ist angekommen
 * 547  (Bayern/Thüringen) Ein Kindlein liegt in dem armen Stall
 * 576  (Hessen-Nassau) Ein kleines Kind, du großer Gott
 * 576  (Kurhessen-Waldeck) Ein kleines Kind, du großer Gott
 * 555  (Württemberg) Ein Licht geht uns auf in der Dunkelheit
 * 557  (Hessen-Nassau) Ein Licht geht uns auf in der Dunkelheit
 * 557  (Kurhessen-Waldeck) Ein Licht geht uns auf in der Dunkelheit
 * 580  (Niedersachsen/Bremen) Ein Lied hat die Freude sich ausgedacht
 * 580  (Oldenburg) Ein Lied hat die Freude sich ausgedacht
 * 670  (Baden/Elsass/Lothringen) Ein neuer Tag beginnt
 * 670  (Pfalz) Ein neuer Tag beginnt
 * 591  (Hessen-Nassau) Einsam bist du klein
 * 591  (Kurhessen-Waldeck) Einsam bist du klein
 * 572  (Niedersachsen/Bremen) Ein Schiff, das sich Gemeinde nennt
 * 572  (Oldenburg) Ein Schiff, das sich Gemeinde nennt
 * 589  (Bayern/Thüringen) Ein Schiff, das sich Gemeinde nennt
 * 595  (Württemberg) Ein Schiff, das sich Gemeinde nennt
 * 604  (reformierte) Ein Schiff, das sich Gemeinde nennt
 * 604  (Rheinland/Westfalen/Lippe) Ein Schiff, das sich Gemeinde nennt
 * 609  (Baden/Elsass/Lothringen) Ein Schiff, das sich Gemeinde nennt
 * 609  (Pfalz) Ein Schiff, das sich Gemeinde nennt
 * 612  (Nordelbien) Ein Schiff, dass sich Gemeinde nennt
 * 574  (Baden/Elsass/Lothringen) Eins hätten wir von Herzen gern
 * 574  (Pfalz) Eins hätten wir von Herzen gern
 * 638  (reformierte) Erd und Himmel klinge
 * 638  (Rheinland/Westfalen/Lippe) Erd und Himmel klinge
 * 636  (reformierte) Erfreue dich, Himmel, erfreue dich, Erden
 * 636  (Rheinland/Westfalen/Lippe) Erfreue dich, Himmel, erfreue dich, Erden
 * 619  (Hessen-Nassau) Er hält die ganze Welt in seiner Hand
 * 619  (Kurhessen-Waldeck) Er hält die ganze Welt in seiner Hand
 * 588  (Österreich) Erheb dein Herz, tu auf dein Ohren
 * 657  (reformierte) Erheb dein Herz, tu auf dein Ohren
 * 657  (Rheinland/Westfalen/Lippe) Erheb dein Herz, tu auf dein Ohren
 * 619  (reformierte) Erhör, o Gott, mein Flehen
 * 619  (Rheinland/Westfalen/Lippe) Erhör, o Gott, mein Flehen
 * 608  (reformierte) Erleuchte und bewege uns
 * 608  (Rheinland/Westfalen/Lippe) Erleuchte und bewege uns
 * 583  (Hessen-Nassau) Er ruft die vielen her
 * 583  (Kurhessen-Waldeck) Er ruft die vielen her
 * 586  (Baden/Elsass/Lothringen) Es ist ein Wort ergangen
 * 586  (Pfalz) Es ist ein Wort ergangen
 * 590  (reformierte) Es ist ein Wort ergangen
 * 590  (Rheinland/Westfalen/Lippe) Es ist ein Wort ergangen
 * 543  (Niedersachsen/Bremen) Es ist für uns eine Zeit angekommen
 * 543  (Oldenburg) Es ist für uns eine Zeit angekommen
 * 545  (reformierte) Es ist für uns eine Zeit angekommen
 * 545  (Rheinland/Westfalen/Lippe) Es ist für uns eine Zeit angekommen
 * 552  (Bayern/Thüringen) Es ist vollbracht (eG-NA #497 od. #92)
 * 647  (Österreich) Es kann nicht Friede werden
 * 560  (Hessen-Nassau) Es kommt die Zeit
 * 560  (Kurhessen-Waldeck) Es kommt die Zeit
 * 604  (Baden/Elsass/Lothringen) Es muss uns doch gelingen
 * 604  (Pfalz) Es muss uns doch gelingen
 * 582  (Baden/Elsass/Lothringen) Es sind doch selig alle, die
 * 582  (Pfalz) Es sind doch selig alle, die
 * 558  (Österreich) Es sungen drei Engel
 * 557  (Österreich) Es werden kommen
 * 576  (Nordelbien) Ewig steht fest der Kirche Haus
 * 638  (Baden/Elsass/Lothringen) Fortgekämpft und fortgerungen (eG-NA #371)
 * 638  (Pfalz) Fortgekämpft und fortgerungen (eG-NA #371)
 * 626  (Hessen-Nassau) Freude, die überfließt
 * 626  (Kurhessen-Waldeck) Freude, die überfließt
 * 657  (Baden/Elsass/Lothringen) Freuen können sich alle
 * 657  (Pfalz) Freuen können sich alle
 * 579  (reformierte) Freuet euch im Herrn
 * 579  (Rheinland/Westfalen/Lippe) Freuet euch im Herrn
 * 542  (Mecklenburg) Freunde, dass der Mandelzweig
 * 606  (Nordelbien) Freunde, dass der Mandelzweig
 * 613  (Hessen-Nassau) Freunde, dass der Mandelzweig
 * 613  (Kurhessen-Waldeck) Freunde, dass der Mandelzweig
 * 620  (Niedersachsen/Bremen) Freunde, dass der Mandelzweig
 * 620  (Oldenburg) Freunde, dass der Mandelzweig
 * 651  (reformierte) Freunde, dass der Mandelzweig
 * 651  (Rheinland/Westfalen/Lippe) Freunde, dass der Mandelzweig
 * 655  (Württemberg) Freunde, dass der Mandelzweig
 * 659  (Bayern/Thüringen) Freunde, dass der Mandelzweig
 * 616  (Baden/Elsass/Lothringen) Freut euch des Herrn, ihr Christen all
 * 616  (Pfalz) Freut euch des Herrn, ihr Christen all
 * 551  (Österreich) Freut euch, ihr liebe Christen
 * 542  (Nordelbien) Freut euch ihr lieben Christen
 * 540  (Niedersachsen/Bremen) Freut euch, ihr lieben Christen
 * 540  (Oldenburg) Freut euch, ihr lieben Christen
 * 540  (reformierte) Freut euch, ihr lieben Christen
 * 540  (Rheinland/Westfalen/Lippe) Freut euch, ihr lieben Christen
 * 548  (Baden/Elsass/Lothringen) Freut euch, ihr lieben Christen
 * 548  (Pfalz) Freut euch, ihr lieben Christen
 * 611  (Baden/Elsass/Lothringen) Freut euch, wir sind Gottes Volk
 * 611  (Pfalz) Freut euch, wir sind Gottes Volk
 * 641  (Hessen-Nassau) Friede mit dir
 * 641  (Kurhessen-Waldeck) Friede mit dir
 * 664  (Württemberg) Früh am Morgen Jesus gehet
 * 640  (Württemberg) Für Christus leben, sterben für ihn
 * 595  (Niedersachsen/Bremen) Fürchte dich nicht
 * 595  (Oldenburg) Fürchte dich nicht
 * 612  (Hessen-Nassau) Fürchte dich nicht
 * 612  (Kurhessen-Waldeck) Fürchte dich nicht
 * 629  (Württemberg) Fürchte dich nicht
 * 630  (Bayern/Thüringen) Fürchte dich nicht
 * 631  (Österreich) Fürchte dich nicht
 * 643  (Baden/Elsass/Lothringen) Fürchte dich nicht
 * 643  (Pfalz) Fürchte dich nicht
 * 656  (reformierte) Fürchte dich nicht
 * 656  (Rheinland/Westfalen/Lippe) Fürchte dich nicht
 * 607  (Nordelbien) Fürchte dich nicht, gefangen in deiner Angst
 * 661  (reformierte) Für dich sei ganz mein Herz und Leben
 * 661  (Rheinland/Westfalen/Lippe) Für dich sei ganz mein Herz und Leben
 * 676  (Württemberg) Geh aus, mein Herz, und suche Freud (eG-NA #394)
 * 668  (reformierte) Gehet hin an alle Enden
 * 668  (Rheinland/Westfalen/Lippe) Gehet hin an alle Enden
 * 661.6  (Niedersachsen/Bremen) Gehet hin im Frieden
 * 661.6  (Oldenburg) Gehet hin im Frieden
 * 677.4  (Nordelbien) Gehet hin im Frieden
 * 661.4  (Niedersachsen/Bremen) Gehet hin im Frieden des Herrn
 * 661.4  (Oldenburg) Gehet hin im Frieden des Herrn
 * 661.5  (Niedersachsen/Bremen) Gehet hin im Frieden des Herrn
 * 661.5  (Oldenburg) Gehet hin im Frieden des Herrn
 * 677.1  (Nordelbien) Gehet hin im Frieden des Herrn
 * 677.2  (Nordelbien) Gehet hin im Frieden des Herrn
 * 677.3  (Nordelbien) Gehet hin im Frieden des Herrn
 * 560  (Niedersachsen/Bremen) Gehn wir in Frieden
 * 560  (Oldenburg) Gehn wir in Frieden
 * 688  (reformierte) Geht der Tag ganz leis zu Ende
 * 688  (Rheinland/Westfalen/Lippe) Geht der Tag ganz leis zu Ende
 * 593  (Baden/Elsass/Lothringen) Geht hin, geht hin in alle Welt
 * 593  (Pfalz) Geht hin, geht hin in alle Welt
 * 632  (Baden/Elsass/Lothringen) Geht hin, ihr gläubigen Gedanken
 * 632  (Pfalz) Geht hin, ihr gläubigen Gedanken
 * 543  (Württemberg) Geh unter der Gnade
 * 628  (Nordelbien) Gelobt sei deine Treu
 * 630  (Niedersachsen/Bremen) Gelobt sei deine Treu
 * 630  (Oldenburg) Gelobt sei deine Treu
 * 665  (Württemberg) Gelobt sei deine Treu
 * 681  (reformierte) Gelobt sei deine Treu
 * 681  (Rheinland/Westfalen/Lippe) Gelobt sei deine Treu
 * 622  (Niedersachsen/Bremen) Giff Freden uns, du Gnaadengott
 * 622  (Oldenburg) Giff Freden uns, du Gnaadengott
 * 632  (Nordelbien) Glädjens Herre, varen gäst: Herr der Freude, Herr der Welt
 * 575.2  (Österreich) Gloria, gloria
 * 575.4  (Österreich) Gloria, gloria in excelsis Deo
 * 566  (Hessen-Nassau) Gloria, gloria in excelsis Deo!
 * 566  (Kurhessen-Waldeck) Gloria, gloria in excelsis Deo!
 * 572  (Württemberg) Gloria in excelsis Deo
 * 580  (reformierte) Gloria in excelsis Deo
 * 580  (Rheinland/Westfalen/Lippe) Gloria in excelsis Deo
 * 632  (reformierte) Glückliche Stunde, darin ich vernommen
 * 632  (Rheinland/Westfalen/Lippe) Glückliche Stunde, darin ich vernommen
 * 637  (Österreich) Gott, deine Güte reicht so weit (eG-NA #260)
 * 587  (Nordelbien) Gott, den Vater, ewig preist
 * 638  (Niedersachsen/Bremen) Gott, der du Berg und Hügel
 * 638  (Oldenburg) Gott, der du Berg und Hügel
 * 598  (Bayern/Thüringen) Gott der Herr ist Sonne und Schild
 * 625  (reformierte) Gott der Herr regiert
 * 625  (Rheinland/Westfalen/Lippe) Gott der Herr regiert
 * 660  (Nordelbien) Gott, dir sei Preis und Ehre
 * 615  (Österreich) Gott, du bist Sonne und Schild
 * 652  (Österreich) Gott, du hast das Land gegeben
 * 610  (Niedersachsen/Bremen) Gottes Liebe ist wie die Sonne
 * 610  (Oldenburg) Gottes Liebe ist wie die Sonne
 * 611  (Niedersachsen/Bremen) Gottes Liebe ist wie die Sonne
 * 611  (Oldenburg) Gottes Liebe ist wie die Sonne
 * 620  (Hessen-Nassau) Gottes Liebe ist wie die Sonne
 * 620  (Kurhessen-Waldeck) Gottes Liebe ist wie die Sonne
 * 654  (Baden/Elsass/Lothringen) Gottes Liebe ist wie die Sonne
 * 654  (Pfalz) Gottes Liebe ist wie die Sonne
 * 601  (Hessen-Nassau) Gottes Lob wandert
 * 601  (Kurhessen-Waldeck) Gottes Lob wandert
 * 550  (Österreich) Gottes Lob wandert, und Erde darf hören
 * 586  (Nordelbien) Gottes Lob wandert, und Erde darf hören
 * 566  (Württemberg) Gottes Ruhetag
 * 567  (Bayern/Thüringen) Gottes Ruhetag
 * 631  (Niedersachsen/Bremen) Gottes Ruhetag
 * 631  (Oldenburg) Gottes Ruhetag
 * 553  (Württemberg) Gottes Stimme lasst uns sein
 * 562  (reformierte) Gottes Stimme lasst uns sein
 * 562  (Rheinland/Westfalen/Lippe) Gottes Stimme lasst uns sein
 * 596  (Österreich) Gottes Stimme lasst uns sein
 * 554  (Hessen-Nassau) Gottes Volk geht nicht allein
 * 554  (Kurhessen-Waldeck) Gottes Volk geht nicht allein
 * 572  (Hessen-Nassau) Gottes Wort ist wie Licht in der Nacht
 * 572  (Kurhessen-Waldeck) Gottes Wort ist wie Licht in der Nacht
 * 591  (reformierte) Gottes Wort ist wie Licht in der Nacht
 * 591  (Rheinland/Westfalen/Lippe) Gottes Wort ist wie Licht in der Nacht
 * 586  (Württemberg) Gott gibt ein Fest
 * 629  (reformierte) Gott hab ich lieb, er hörte mein Gebet
 * 629  (Rheinland/Westfalen/Lippe) Gott hab ich lieb, er hörte mein Gebet
 * 605  (Nordelbien) Gott is bi di. Wees man nich bang
 * 616  (Württemberg) Gott ist getreu (eG-NA #24)
 * 637  (Baden/Elsass/Lothringen) Gott ist getreu (eG-NA #24)
 * 637  (Pfalz) Gott ist getreu (eG-NA #24)
 * 566  (Baden/Elsass/Lothringen) Gott ist König, sein ist alle Macht
 * 566  (Pfalz) Gott ist König, sein ist alle Macht
 * 588  (Nordelbien) Gott ist König, sein ist alle Macht über diese Welt
 * 598  (Hessen-Nassau) Gott ist mein Lied (eG-NA #15)
 * 598  (Kurhessen-Waldeck) Gott ist mein Lied (eG-NA #15)
 * 567  (Österreich) Gott ist unsre Zuversicht
 * 613  (Württemberg) Gott lebet! Sein Name gibt Leben und Stärke
 * 537  (Bayern/Thüringen) Gott Lob, ein neues Kirchenjahr
 * 639  (Baden/Elsass/Lothringen) Gott, mein Trost und mein Vertrauen
 * 639  (Pfalz) Gott, mein Trost und mein Vertrauen
 * 562  (Nordelbien) Gott, mien König, maak mi still
 * 587  (Hessen-Nassau) Gott ruft dich, priesterliche Schar
 * 587  (Kurhessen-Waldeck) Gott ruft dich, priesterliche Schar
 * 678  (Baden/Elsass/Lothringen) Gott, segne unser Wandern
 * 678  (Pfalz) Gott, segne unser Wandern
 * 583  (Bayern/Thüringen) Gott, unser Festtag ist gekommen
 * 589  (Österreich) Gott, unser Festtag ist gekommen
 * 601  (Baden/Elsass/Lothringen) Gott, unser Festtag ist gekommen
 * 601  (Pfalz) Gott, unser Festtag ist gekommen
 * 663  (Nordelbien) Gott Vater, der du ewig bist
 * 557  (Württemberg) Gott Vater, Herr, wir danken dir
 * 650  (Nordelbien) Gott Vater sei Lob, Ehr und Preis
 * 620  (Württemberg) Gott will’s machen (eG-NA #367)
 * 590  (Württemberg) Gott, wir preisen deine Wunder
 * 601  (reformierte) Gott, wir preisen deine Wunder
 * 601  (Rheinland/Westfalen/Lippe) Gott, wir preisen deine Wunder
 * 602  (Baden/Elsass/Lothringen) Gott, wir preisen deine Wunder
 * 602  (Pfalz) Gott, wir preisen deine Wunder
 * 653  (Nordelbien) Gott wir wollen ehren
 * 565  (Baden/Elsass/Lothringen) Große Leute, kleine Leute
 * 565  (Pfalz) Große Leute, kleine Leute
 * 591  (Württemberg) Großer Hirte aller Herden (eG-NA #218)
 * 618  (Bayern/Thüringen) Guter Gott, dankeschön
 * 576.1  (Österreich) Halleluja
 * 576.2  (Österreich) Halleluja
 * 576.3  (Österreich) Halleluja
 * 581  (reformierte) Halleluja
 * 581  (Rheinland/Westfalen/Lippe) Halleluja
 * 646  (Niedersachsen/Bremen) Halleluja
 * 646  (Oldenburg) Halleluja
 * 668.1  (Nordelbien) Halleluja
 * 668.2  (Nordelbien) Halleluja
 * 635  (reformierte) Halleluja, Gott zu loben
 * 635  (Rheinland/Westfalen/Lippe) Halleluja, Gott zu loben
 * 572  (Baden/Elsass/Lothringen) Halleluja, schöner Morgen (eG-NA #213)
 * 572  (Pfalz) Halleluja, schöner Morgen (eG-NA #213)
 * 641  (Bayern/Thüringen) Halte zu mir, guter Gott
 * 593  (Niedersachsen/Bremen) Harre, meine Seele
 * 593  (Oldenburg) Harre, meine Seele
 * 611  (Hessen-Nassau) Harre, meine Seele
 * 611  (Kurhessen-Waldeck) Harre, meine Seele
 * 623  (Württemberg) Harre, meine Seele
 * 628  (Österreich) Harre, meine Seele
 * 640  (Baden/Elsass/Lothringen) Harre, meine Seele
 * 640  (Pfalz) Harre, meine Seele
 * 596  (Bayern/Thüringen) Harre, meine Seele, harre des Herrn
 * 602  (Nordelbien) Harre, meine Seele, harre des Herrn
 * 553  (Rheinland/Westfalen/Lippe) Heilige Drei Könige|Die Weisen aus dem Morgenland
 * 596  (Württemberg) Heilig, heilig, heilig
 * 672.3  (Nordelbien) Heilig, heilig, heilig Gott, Herr aller Mächte und Gewalten
 * 655  (Niedersachsen/Bremen) Heilig, heilig, heilig ist Gott
 * 655  (Oldenburg) Heilig, heilig, heilig ist Gott
 * 672.2  (Nordelbien) Heilig, heilig, heilig ist Gott der Herr Zebaoth
 * 580.2  (Österreich) Heilig ist Gott der Vater
 * 672.1  (Nordelbien) Heilig ist Gott der Vater
 * 579  (Österreich) Heilig ist Gott in Herrlichkeit
 * 546  (Nordelbien) Helft mir Gotts Güte preisen
 * 549  (reformierte) Helft mir Gotts Güte preisen
 * 549  (Rheinland/Westfalen/Lippe) Helft mir Gotts Güte preisen
 * 552  (Baden/Elsass/Lothringen) Helft mir Gotts Güte preisen
 * 552  (Pfalz) Helft mir Gotts Güte preisen
 * 641  (Nordelbien) Herlich tut mich verlangen
 * 551  (Bayern/Thüringen) Herr Christus, treuer Heiland wert
 * 610  (Hessen-Nassau) Herr, deine Liebe ist wie Gras und Ufer
 * 610  (Kurhessen-Waldeck) Herr, deine Liebe ist wie Gras und Ufer
 * 623  (Nordelbien) Herr, deine Liebe ist wie Gras und Ufer
 * 638  (Bayern/Thüringen) Herr, deine Liebe ist wie Gras und Ufer
 * 643  (Württemberg) Herr, deine Liebe ist wie Gras und Ufer
 * 653  (Baden/Elsass/Lothringen) Herr, deine Liebe ist wie Gras und Ufer
 * 653  (Pfalz) Herr, deine Liebe ist wie Gras und Ufer
 * 663  (reformierte) Herr, deine Liebe ist wie Gras und Ufer
 * 663  (Rheinland/Westfalen/Lippe) Herr, deine Liebe ist wie Gras und Ufer
 * 586  (Hessen-Nassau) Herr, der du einst gekommen bist
 * 586  (Kurhessen-Waldeck) Herr, der du einst gekommen bist
 * 583  (Württemberg) Herr, dieses Kind, dir dargebracht
 * 598  (Baden/Elsass/Lothringen) Herr, du hast dich gern mit vielen
 * 598  (Pfalz) Herr, du hast dich gern mit vielen
 * 596  (Nordelbien) Herr, durch den Glauben wohn in mir
 * 573.1  (Österreich) Herr, erbarme dich
 * 682  (reformierte) Herr, gib, dass ich auch diesen Tag
 * 682  (Rheinland/Westfalen/Lippe) Herr, gib, dass ich auch diesen Tag
 * 649  (Bayern/Thüringen) Herr, gib du uns Augen
 * 612  (Niedersachsen/Bremen) Herr, gib mir Mut zum Brückenbauen
 * 612  (Oldenburg) Herr, gib mir Mut zum Brückenbauen
 * 628  (Hessen-Nassau) Herr, gib mir Mut zum Brückenbauen
 * 628  (Kurhessen-Waldeck) Herr, gib mir Mut zum Brückenbauen
 * 646  (Bayern/Thüringen) Herr, gib mir Mut zum Brückenbauen
 * 647  (Bayern/Thüringen) Herr, gib mir Mut zum Brückenbauen
 * 649  (Württemberg) Herr, gib mir Mut zum Brückenbauen
 * 669  (reformierte) Herr, gib mir Mut zum Brückenbauen
 * 669  (Rheinland/Westfalen/Lippe) Herr, gib mir Mut zum Brückenbauen
 * 588  (Bayern/Thüringen) Herr, gib uns Mut zum Hören
 * 605  (reformierte) Herr, gib uns Mut zum Hören
 * 605  (Rheinland/Westfalen/Lippe) Herr, gib uns Mut zum Hören
 * 598  (Nordelbien) Herr, giff mi Kraft to’n Beden
 * 570  (Nordelbien) Herr Godt, nun sy gepryset
 * 618  (Österreich) Herr Gott, der du mein Vater bist
 * 557  (Nordelbien) Herr Gott, dich loben alle wir
 * :191 Herr Gott, dich loben wir (Luther)|Herr Gott, dich loben wir (eG-NA #376 od 419)
 * 609  (Österreich) Herr Gott, dich loben wir (Franck)|Herr Gott, dich loben wir <!-- auch [[Herr Gott, dich loben wir; regier, Herr, unsre Stimmen]] --> (eG-NA #419)
 * 623  (Baden/Elsass/Lothringen) Herr Gott, dich loben wir (Franck)|Herr Gott, dich loben wir <!-- auch [[Herr Gott, dich loben wir; regier, Herr, unsre Stimmen]] --> (eG-NA #419)
 * 623  (Pfalz) Herr Gott, dich loben wir (Franck)|Herr Gott, dich loben wir <!-- auch [[Herr Gott, dich loben wir; regier, Herr, unsre Stimmen]] --> (eG-NA #419)
 * 663  (Österreich) Herr Gott, du Herrscher aller Welt
 * 680  (Baden/Elsass/Lothringen) Herr Gott, du Herrscher aller Welt
 * 680  (Pfalz) Herr Gott, du Herrscher aller Welt
 * 630  (Nordelbien) Herr Gott, gib uns das täglich Brot
 * 633  (Niedersachsen/Bremen) Herr Gott, gib uns das täglich Brot
 * 633  (Oldenburg) Herr Gott, gib uns das täglich Brot
 * 607  (Bayern/Thüringen) Herr Gott, wir danken dir
 * 601  (Niedersachsen/Bremen) Herr, hilf uns heilen
 * 601  (Oldenburg) Herr, hilf uns heilen
 * 646  (Österreich) Herr, hilf uns heilen
 * 621  (Österreich) Herr, ich bin dein Eigentum
 * 651  (Österreich) Herr, ich sehe deine Welt
 * 632  (Niedersachsen/Bremen) Herr, ich werfe meine Freude
 * 632  (Oldenburg) Herr, ich werfe meine Freude
 * 584  (Bayern/Thüringen) Herr Jesu, der du selbst von Gott als Lehrer kommen
 * 660  (Österreich) Herr, lass auf Erden
 * 685  (reformierte) Herr, lass auf Erden
 * 685  (Rheinland/Westfalen/Lippe) Herr, lass auf Erden
 * 634  (Württemberg) Herr, lass mich deine Heiligung
 * 569  (Niedersachsen/Bremen) Herr, lass uns deiner Nähe inne werden
 * 569  (Oldenburg) Herr, lass uns deiner Nähe inne werden
 * 575  (Nordelbien) Herr, lass uns füreinander leben
 * 615  (Niedersachsen/Bremen) Herr, mach die Kirche zum Werkzeug deines Friedens
 * 615  (Oldenburg) Herr, mach die Kirche zum Werkzeug deines Friedens
 * 592  (Österreich) Herr, mit deinem Schutz begleite uns
 * 620  (reformierte) Herr, unser Gott, auf den wir trauen
 * 620  (Rheinland/Westfalen/Lippe) Herr, unser Gott, auf den wir trauen
 * 619  (Nordelbien) Herr, von dir kommt alles Leben
 * 640  (Österreich) Herr, von dir kommt alles Leben
 * 625  (Württemberg) Herr, weil mich festhält deine starke Hand
 * 639  (Niedersachsen/Bremen) Herr, wenn das Wasser uns bedroht
 * 639  (Oldenburg) Herr, wenn das Wasser uns bedroht
 * 561  (Niedersachsen/Bremen) Herr, wir bitten: Komm und segne uns
 * 561  (Oldenburg) Herr, wir bitten: Komm und segne uns
 * 565  (Württemberg) Herr, wir bitten: Komm und segne uns
 * 571  (Österreich) Herr, wir bitten: Komm und segne uns
 * 572  (Bayern/Thüringen) Herr, wir bitten: Komm und segne uns
 * 590  (Hessen-Nassau) Herr, wir bitten: Komm und segne uns
 * 590  (Kurhessen-Waldeck) Herr, wir bitten: Komm und segne uns
 * 607  (reformierte) Herr, wir bitten: Komm und segne uns
 * 607  (Rheinland/Westfalen/Lippe) Herr, wir bitten: Komm und segne uns
 * 610  (Baden/Elsass/Lothringen) Herr, wir bitten: Komm und segne uns
 * 610  (Pfalz) Herr, wir bitten: Komm und segne uns
 * 554  (Bayern/Thüringen) Herr, wir denken an dein Leiden
 * 594  (Württemberg) Herr, wir stehen Hand in Hand
 * 595  (Österreich) Herr, wir stehen Hand in Hand
 * 602  (Niedersachsen/Bremen) Herr, wir stehen Hand in Hand
 * 602  (Oldenburg) Herr, wir stehen Hand in Hand
 * 607  (Baden/Elsass/Lothringen) Herr, wir stehen Hand in Hand
 * 607  (Pfalz) Herr, wir stehen Hand in Hand
 * 665  (Österreich) Herzlich tut mich verlangen (eG-NA #493)
 * 684  (Baden/Elsass/Lothringen) Herzlich tut mich verlangen (eG-NA #493)
 * 684  (Pfalz) Herzlich tut mich verlangen (eG-NA #493)
 * 646  (Hessen-Nassau) Heut war ein schöner Tag
 * 646  (Kurhessen-Waldeck) Heut war ein schöner Tag
 * 666  (Bayern/Thüringen) Heut war ein schöner Tag
 * 672  (Württemberg) Heut war ein schöner Tag
 * 674  (Baden/Elsass/Lothringen) Heut war ein schöner Tag
 * 674  (Pfalz) Heut war ein schöner Tag
 * 624  (Österreich) Hilf, Helfer, hilf in Angst und Not (eG-NA #346)
 * 625  (Bayern/Thüringen) Hilf, Helfer, hilf in Angst und Not (eG-NA #346)
 * 601  (Nordelbien) Hilf uns, Herr, in allen Dingen
 * 647  (Hessen-Nassau) Hilf uns, Herr, in allen Dingen
 * 647  (Kurhessen-Waldeck) Hilf uns, Herr, in allen Dingen
 * 556  (Niedersachsen/Bremen) Hilige Geist, kumm un faat mi
 * 556  (Oldenburg) Hilige Geist, kumm un faat mi
 * 584  (Nordelbien) Hineh ma tow uma naim: Schön ist’s, wenn Brüder und Schwestern
 * 670  (Württemberg) Hirte deiner Schafe (eG-NA #446)
 * 578  (Baden/Elsass/Lothringen) Höchster Gott, wir danken dir
 * 578  (Pfalz) Höchster Gott, wir danken dir
 * 568  (Baden/Elsass/Lothringen) Höchster Tröster, komm hernieder
 * 568  (Pfalz) Höchster Tröster, komm hernieder
 * 628  (Bayern/Thüringen) Hoffnung die dunkle Nacht erhellt
 * 565  (Hessen-Nassau) Höre, höre uns, Gott
 * 565  (Kurhessen-Waldeck) Höre, höre uns, Gott
 * 670  (reformierte) Hört, wen Jesus glücklich preist
 * 670  (Rheinland/Westfalen/Lippe) Hört, wen Jesus glücklich preist
 * 657  (Niedersachsen/Bremen) Hosanna, hosanna
 * 657  (Oldenburg) Hosanna, hosanna
 * 536  (Bayern/Thüringen) Hosianna Davids Sohne
 * 672.5  (Nordelbien) Hosianna|Hosanna
 * 615  (Nordelbien) Ich bete an die Macht der Liebe
 * 617  (Hessen-Nassau) Ich bete an die Macht der Liebe
 * 617  (Kurhessen-Waldeck) Ich bete an die Macht der Liebe
 * 641  (Württemberg) Ich bete an die Macht der Liebe
 * 651  (Baden/Elsass/Lothringen) Ich bete an die Macht der Liebe
 * 651  (Pfalz) Ich bete an die Macht der Liebe
 * 587  (Württemberg) Ich bin das Brot, lade euch ein
 * 621  (Bayern/Thüringen) Ich bin durch die Welt gegangen
 * 625  (Österreich) Ich bin ein armer Exulant
 * 627  (Niedersachsen/Bremen) Ich danke dir durch deinen Sohn
 * 627  (Oldenburg) Ich danke dir durch deinen Sohn
 * 661  (Bayern/Thüringen) Ich danke dir, Herr Gott, in deinem Throne
 * 611  (Österreich) Ich danke Gott und freue mich
 * 600  (Österreich) Ich erhebe mein Gemüte
 * 668  (Österreich) Ich freue mich der frohen Zeit (eG-NA #516)
 * 589  (Nordelbien) Ich freue mich und springe
 * 599  (Niedersachsen/Bremen) Ich gehöre dazu
 * 599  (Oldenburg) Ich gehöre dazu
 * 651  (Niedersachsen/Bremen) Ich glaube an Gott, den Vater
 * 651  (Oldenburg) Ich glaube an Gott, den Vater
 * 652  (Niedersachsen/Bremen) Ich glaube an Gott, den Vater
 * 652  (Oldenburg) Ich glaube an Gott, den Vater
 * 578  (Österreich) Ich glaube an Gott, den Vater, den Allmächtigen
 * 670.1  (Nordelbien) Ich glaube an Gott, den Vater, den Allmächtigen
 * 661  (Württemberg) Ich glaube fest, dass alles anders wird
 * 558  (Bayern/Thüringen) Ich hör die Botschaft: Jesus lebt
 * 563  (Österreich) Ich hör die Botschaft: Jesus lebt
 * 585  (Niedersachsen/Bremen) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 585  (Oldenburg) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 611  (Württemberg) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 615  (Bayern/Thüringen) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 628  (Baden/Elsass/Lothringen) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 628  (Pfalz) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 638  (Hessen-Nassau) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 638  (Kurhessen-Waldeck) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 673  (reformierte) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 673  (Rheinland/Westfalen/Lippe) Ich lobe meinen Gott, der aus der Tiefe mich holt
 * 596  (Niedersachsen/Bremen) Ich möchte Glauben haben
 * 596  (Oldenburg) Ich möchte Glauben haben
 * 622  (Bayern/Thüringen) Ich möchte Glauben haben, der über Zweifel siegt
 * 585  (Hessen-Nassau) Ich rede, wenn ich schweigen sollte
 * 585  (Kurhessen-Waldeck) Ich rede, wenn ich schweigen sollte
 * 647  (Baden/Elsass/Lothringen) Ich rede, wenn ich schweigen sollte
 * 647  (Pfalz) Ich rede, wenn ich schweigen sollte
 * 631  (reformierte) Ich schau nach jenen Bergen gern
 * 631  (Rheinland/Westfalen/Lippe) Ich schau nach jenen Bergen gern
 * 589  (Niedersachsen/Bremen) Ich singe dir mit Herz und Mund (eG-NA #27)
 * 589  (Oldenburg) Ich singe dir mit Herz und Mund (eG-NA #27)
 * 624  (Baden/Elsass/Lothringen) Ich singe dir mit Herz und Mund (eG-NA #27)
 * 624  (Pfalz) Ich singe dir mit Herz und Mund (eG-NA #27)
 * 643  (reformierte) Ich singe dir mit Herz und Mund (eG-NA #27)
 * 643  (Rheinland/Westfalen/Lippe) Ich singe dir mit Herz und Mund (eG-NA #27)
 * 622  (reformierte) Ich sing in Ewigkeit von des Erbarmers Huld
 * 622  (Rheinland/Westfalen/Lippe) Ich sing in Ewigkeit von des Erbarmers Huld
 * 603  (Bayern/Thüringen) Ich sitze oder stehe
 * 556  (reformierte) Ich steh an deinem Kreuz, Herr Christ
 * 556  (Rheinland/Westfalen/Lippe) Ich steh an deinem Kreuz, Herr Christ
 * 581  (Bayern/Thüringen) Ich und mein Haus, wir sind bereit (eG-NA #464)
 * 600  (Baden/Elsass/Lothringen) Ich und mein Haus, wir sind bereit (eG-NA #464)
 * 600  (Pfalz) Ich und mein Haus, wir sind bereit (eG-NA #464)
 * 640  (Nordelbien) Ich weiß ein lieblich Engelspiel
 * 627  (Württemberg) Ich werfe meine Fragen hinüber
 * 632  (Bayern/Thüringen) Ich will glauben: Du bist da
 * 580  (Nordelbien) Ich will Gott loben allezeit
 * 538  (Mecklenburg) I danced in the morning
 * 636  (Bayern/Thüringen) Ihr seid das Volk, das der Herr sich ausersehn
 * 564  (Hessen-Nassau) Im Frieden mach uns eins
 * 564  (Kurhessen-Waldeck) Im Frieden mach uns eins
 * 643  (Nordelbien) Im Himmelreich, im Himmelreich
 * 604  (Württemberg) Im Lande der Knechtschaft
 * 680  (reformierte) Im Lande der Knechtschaft
 * 680  (Rheinland/Westfalen/Lippe) Im Lande der Knechtschaft
 * 550  (Baden/Elsass/Lothringen) Immanuel! Der Herr ist hier ([NSUSF](https://github.com/nun-singet-und-seid-froh/nun-singet-und-seid-froh/tree/master/scores))
 * 550  (Pfalz) Immanuel! Der Herr ist hier
 * 626  (Bayern/Thüringen) In Ängsten die einen
 * 573  (Niedersachsen/Bremen) In Christus gilt nicht Ost noch West
 * 573  (Oldenburg) In Christus gilt nicht Ost noch West
 * 597  (Württemberg) In Christus gilt nicht Ost noch West
 * 658  (Bayern/Thüringen) In Christus gilt nicht Ost noch West
 * 560  (Baden/Elsass/Lothringen) In der Welt habt ihr Angst
 * 560  (Pfalz) In der Welt habt ihr Angst
 * 547  (reformierte) In einer Höhle zu Bethlehem
 * 547  (Rheinland/Westfalen/Lippe) In einer Höhle zu Bethlehem
 * 631  (Hessen-Nassau) In Gottes Namen wolln wir finden
 * 631  (Kurhessen-Waldeck) In Gottes Namen wolln wir finden
 * 629  (Österreich) In jeder Nacht, die mich bedroht
 * 603  (Niedersachsen/Bremen) Ins Wasser fällt ein Stein
 * 603  (Oldenburg) Ins Wasser fällt ein Stein
 * 620  (Nordelbien) Ins Wasser fällt ein Stein
 * 621  (Hessen-Nassau) Ins Wasser fällt ein Stein
 * 621  (Kurhessen-Waldeck) Ins Wasser fällt ein Stein
 * 637  (Württemberg) Ins Wasser fällt ein Stein
 * 645  (Bayern/Thüringen) Ins Wasser fällt ein Stein
 * 648  (Baden/Elsass/Lothringen) Ins Wasser fällt ein Stein
 * 648  (Pfalz) Ins Wasser fällt ein Stein
 * 659  (reformierte) Ins Wasser fällt ein Stein
 * 659  (Rheinland/Westfalen/Lippe) Ins Wasser fällt ein Stein
 * 581  (Nordelbien) Ja, ich will singen
 * 590  (Niedersachsen/Bremen) Ja, ich will singen
 * 590  (Oldenburg) Ja, ich will singen
 * 639  (reformierte) Ja, ich will singen
 * 639  (Rheinland/Westfalen/Lippe) Ja, ich will singen
 * 616  (reformierte) Jauchzt alle, Gott sei hoch erhoben
 * 616  (Rheinland/Westfalen/Lippe) Jauchzt alle, Gott sei hoch erhoben
 * 628  (reformierte) Jauchzt Halleluja, lobt den Herrn
 * 628  (Rheinland/Westfalen/Lippe) Jauchzt Halleluja, lobt den Herrn
 * 635  (Hessen-Nassau) Jeder Teil dieser Erde
 * 635  (Kurhessen-Waldeck) Jeder Teil dieser Erde
 * 655  (Bayern/Thüringen) Jeder Teil dieser Erde
 * 672  (reformierte) Jeder Teil dieser Erde
 * 672  (Rheinland/Westfalen/Lippe) Jeder Teil dieser Erde
 * 562  (Württemberg) Jesu, Jesu, Brunn des Lebens
 * 631  (Baden/Elsass/Lothringen) Jesu, meiner Seele Leben
 * 631  (Pfalz) Jesu, meiner Seele Leben
 * 581  (Hessen-Nassau) Jesus Brot
 * 581  (Kurhessen-Waldeck) Jesus Brot
 * 605  (Niedersachsen/Bremen) Jesus Christus, das Leben der Welt
 * 605  (Oldenburg) Jesus Christus, das Leben der Welt
 * 683  (Württemberg) Jesus Christus gestern und heute
 * 664  (Baden/Elsass/Lothringen) Jesus Christus ist unser Friede
 * 664  (Pfalz) Jesus Christus ist unser Friede
 * 564  (Österreich) Jesus Christus, König und Herr
 * 590  (Bayern/Thüringen) Jesus Christus, König und Herr
 * 560  (Württemberg) Jesu, Seelenfreund der Deinen (eG-NA #7)
 * 573  (Baden/Elsass/Lothringen) Jesu, Seelenfreund der Deinen (eG-NA #7)
 * 573  (Pfalz) Jesu, Seelenfreund der Deinen (eG-NA #7)
 * 609  (Niedersachsen/Bremen) Jesus hat die Kinder lieb
 * 609  (Oldenburg) Jesus hat die Kinder lieb
 * 639  (Österreich) Jesus hat die Kinder lieb
 * 644  (Württemberg) Jesus hat die Kinder lieb
 * 610  (reformierte) Jesus hat seine Herrschaft bestellt
 * 610  (Rheinland/Westfalen/Lippe) Jesus hat seine Herrschaft bestellt
 * 574  (reformierte) Jesus, Haupt und Herr der Deinen
 * 574  (Rheinland/Westfalen/Lippe) Jesus, Haupt und Herr der Deinen
 * 546  (Niedersachsen/Bremen) Jesus ist kommen, Grund ewiger Freude
 * 546  (Oldenburg) Jesus ist kommen, Grund ewiger Freude
 * 552  (Nordelbien) Jesus unser Trost und Leben
 * 561  (reformierte) Jesus, unser Trost und Leben
 * 561  (Rheinland/Westfalen/Lippe) Jesus, unser Trost und Leben
 * 621  (Niedersachsen/Bremen) Jesus, uns’ Heiland
 * 621  (Oldenburg) Jesus, uns’ Heiland
 * 548  (Niedersachsen/Bremen) Jetzt, da die Zeit sich nähert deiner Leiden
 * 548  (Oldenburg) Jetzt, da die Zeit sich nähert deiner Leiden
 * 541  (Bayern/Thüringen) Jetzt fangen wir zum Singen an
 * 581  (Niedersachsen/Bremen) Jona, Jona, auf nach Ninive
 * 581  (Oldenburg) Jona, Jona, auf nach Ninive
 * 557  (Bayern/Thüringen) Jubilate coeli
 * 649  (Niedersachsen/Bremen) Jubilate coeli
 * 649  (Oldenburg) Jubilate coeli
 * 584  (reformierte) Jubilate Deo
 * 584  (Rheinland/Westfalen/Lippe) Jubilate Deo
 * 583  (Nordelbien) Jubilate Deo:Jauchze Erd und Himmel
 * 602  (Österreich) Jubilate Deo omnis terra
 * 617  (Bayern/Thüringen) Jubilate Deo omnis terra
 * 650  (Niedersachsen/Bremen) Jubilate Deo omnis terra
 * 650  (Oldenburg) Jubilate Deo omnis terra
 * 589  (reformierte) Kehret um
 * 589  (Rheinland/Westfalen/Lippe) Kehret um
 * 606  (Niedersachsen/Bremen) Kehret um
 * 606  (Oldenburg) Kehret um
 * 615  (Hessen-Nassau) Kehret um, kehret um
 * 615  (Kurhessen-Waldeck) Kehret um, kehret um
 * 650  (Baden/Elsass/Lothringen) Kehret um, kehret um, und ihr werdet leben
 * 650  (Pfalz) Kehret um, kehret um, und ihr werdet leben
 * 604  (Hessen-Nassau) Kennt ihr die Legende von Christophorus
 * 604  (Kurhessen-Waldeck) Kennt ihr die Legende von Christophorus
 * 539  (Mecklenburg) Kind, du bist uns anvertraut
 * 566  (Nordelbien) Kind, du bist uns anvertraut
 * 576  (Bayern/Thüringen) Kind, du bist uns anvertraut
 * 577  (Hessen-Nassau) Kind, du bist uns anvertraut
 * 577  (Kurhessen-Waldeck) Kind, du bist uns anvertraut
 * 582  (Württemberg) Kind, du bist uns anvertraut
 * 591  (Baden/Elsass/Lothringen) Kind, du bist uns anvertraut
 * 591  (Pfalz) Kind, du bist uns anvertraut
 * 596  (reformierte) Kind, du bist uns anvertraut
 * 596  (Rheinland/Westfalen/Lippe) Kind, du bist uns anvertraut
 * 589  (Hessen-Nassau) Komm, bau ein Haus
 * 589  (Kurhessen-Waldeck) Komm, bau ein Haus
 * 640  (Bayern/Thüringen) Komm, bau ein Haus
 * 575  (Württemberg) Komm, göttliches Licht
 * 552  (Niedersachsen/Bremen) Komm, Heilger Geist, der Leben schafft
 * 552  (Oldenburg) Komm, Heilger Geist, der Leben schafft
 * 563  (Bayern/Thüringen) Komm, Heilger Geist, der Leben schafft
 * 564  (Bayern/Thüringen) Komm, heilger Geist mit deiner Kraft, die uns verbindet
 * 555  (Nordelbien) Komm, Heiliger Geist, der Leben schafft
 * 594  (Baden/Elsass/Lothringen) Komm mein Herz, in Jesu Leiden (eG-NA #248)
 * 594  (Pfalz) Komm mein Herz, in Jesu Leiden (eG-NA #248)
 * 584  (Württemberg) Komm, mein Herz, in Jesu Leiden (eG-NA #248)
 * 639  (Württemberg) Kommt, atmet auf, ihr sollt leben
 * 577  (reformierte) Kommt herbei, singt dem Herrn
 * 577  (Rheinland/Westfalen/Lippe) Kommt herbei, singt dem Herrn
 * 599  (Bayern/Thüringen) Kommt herbei, singt dem Herrn
 * 601  (Württemberg) Kommt herbei, singt dem Herrn
 * 617  (Baden/Elsass/Lothringen) Kommt herbei, singt dem Herrn
 * 617  (Pfalz) Kommt herbei, singt dem Herrn
 * 585  (Bayern/Thüringen) Kommt her, ihr Christen, voller Freud
 * 550  (Nordelbien) Kommt, ihr Menschen, nehmt zu Herzen
 * 579  (Bayern/Thüringen) Kommt, wir teilen das Brot am Tisch des Herrn
 * 585  (Österreich) Kommt, wir teilen das Brot am Tisch des Herrn
 * 565  (Bayern/Thüringen) Komm zu uns, Heilger Geist
 * 548  (Württemberg) Kreuz, auf das ich schaue
 * 598  (Niedersachsen/Bremen) Kreuz, auf das ich schaue
 * 598  (Oldenburg) Kreuz, auf das ich schaue
 * 621  (Nordelbien) Kumbaya, my Lord: Komm zu mir, mein Gott
 * 540  (Bayern/Thüringen) Kündet allen in der Not
 * 666.1  (Nordelbien) Kyrie eleison
 * 666.2  (Nordelbien) Kyrie eleison
 * 666.3  (Nordelbien) Kyrie eleison
 * 573.3  (Österreich) Kyrie, Kyrie eleison
 * 675.1  (Nordelbien) Lamm Gottes
 * 582.2  (Österreich) Lamm Gottes! Der du trägst die Sünde der ganzen Welt
 * 574  (Bayern/Thüringen) Lasset mich mit Freuden sprechen
 * 606  (reformierte) Lasst die Kinder zu mir kommen
 * 606  (Rheinland/Westfalen/Lippe) Lasst die Kinder zu mir kommen
 * 608  (Österreich) Lasst die Kinder zu mir kommen
 * 569  (Österreich) Lasst, eh wir gehn, uns fest verbinden
 * 582  (Hessen-Nassau) Lasst uns Brot brechen und Gott dankbar sein
 * 582  (Kurhessen-Waldeck) Lasst uns Brot brechen und Gott dankbar sein
 * 563  (Niedersachsen/Bremen) Lasst uns miteinander
 * 563  (Oldenburg) Lasst uns miteinander
 * 607  (Hessen-Nassau) Lasst uns miteinander
 * 607  (Kurhessen-Waldeck) Lasst uns miteinander
 * 645  (reformierte) Lasst uns miteinander
 * 645  (Rheinland/Westfalen/Lippe) Lasst uns miteinander
 * 610  (Bayern/Thüringen) Lasst uns miteinander, lasst uns miteinander
 * 640  (Hessen-Nassau) Lass uns den Weg der Gerechtigkeit gehn
 * 640  (Kurhessen-Waldeck) Lass uns den Weg der Gerechtigkeit gehn
 * 658  (Württemberg) Lass uns den Weg der Gerechtigkeit gehn
 * 675  (reformierte) Lass uns den Weg der Gerechtigkeit gehn
 * 675  (Rheinland/Westfalen/Lippe) Lass uns den Weg der Gerechtigkeit gehn
 * 577  (Nordelbien) Lass uns in deinem Namen, Herr
 * 614  (Hessen-Nassau) Lass uns in deinem Namen, Herr
 * 614  (Kurhessen-Waldeck) Lass uns in deinem Namen, Herr
 * 634  (Bayern/Thüringen) Lass uns in deinem Namen, Herr
 * 658  (reformierte) Lass uns in deinem Namen, Herr
 * 658  (Rheinland/Westfalen/Lippe) Lass uns in deinem Namen, Herr
 * 569  (Hessen-Nassau) Laudamus te Domine
 * 569  (Kurhessen-Waldeck) Laudamus te Domine
 * 596  (Hessen-Nassau) Laudate Dominum
 * 596  (Kurhessen-Waldeck) Laudate Dominum
 * 515 Laudato si (Lied)|Laudato si
 * 597  (Nordelbien) Lebenssonne, deren Strahlen
 * 536  (Nordelbien) Leitlied des nordelbischen Liederteils: Gott ist mein Lied (eG-NA #15)
 * 550  (Bayern/Thüringen) Licht, das in die Welt gekommen (eG-NA #190)
 * 552  (reformierte) Licht, das in die Welt gekommen (eG-NA #190)
 * 552  (Rheinland/Westfalen/Lippe) Licht, das in die Welt gekommen (eG-NA #190)
 * 554  (Baden/Elsass/Lothringen) Licht, das in die Welt gekommen (eG-NA #190)
 * 554  (Pfalz) Licht, das in die Welt gekommen (eG-NA #190)
 * 592  (Württemberg) Licht, das in die Welt gekommen (eG-NA #190)
 * 593  (Hessen-Nassau) Licht, das in die Welt gekommen (eG-NA #190)
 * 593  (Kurhessen-Waldeck) Licht, das in die Welt gekommen (eG-NA #190)
 * 613  (Niedersachsen/Bremen) Liebe ist nicht nur ein Wort
 * 613  (Oldenburg) Liebe ist nicht nur ein Wort
 * 629  (Hessen-Nassau) Liebe ist nicht nur ein Wort
 * 629  (Kurhessen-Waldeck) Liebe ist nicht nur ein Wort
 * 650  (Bayern/Thüringen) Liebe ist nicht nur ein Wort
 * 650  (Württemberg) Liebe ist nicht nur ein Wort
 * 665  (reformierte) Liebe ist nicht nur ein Wort.
 * 665  (Rheinland/Westfalen/Lippe) Liebe ist nicht nur ein Wort.
 * 592  (Nordelbien) Lieber Gott, ich danke dir
 * 624  (Hessen-Nassau) Lieber Gott, ich danke dir
 * 624  (Kurhessen-Waldeck) Lieber Gott, ich danke dir
 * 645  (Württemberg) Lieber Gott, ich danke dir
 * 672  (Baden/Elsass/Lothringen) Lieber Herr, wir bitten dich
 * 672  (Pfalz) Lieber Herr, wir bitten dich
 * 627  (Bayern/Thüringen) Liebster Herr Jesu Christ
 * 610  (Württemberg) Lob, Anbetung, Ruhm und Ehre
 * 619  (Baden/Elsass/Lothringen) Lobe den Herrn, mein Leben
 * 619  (Pfalz) Lobe den Herrn, mein Leben
 * 655  (Nordelbien) Lob, Ehr sei Gott im höchsten Throne
 * 656  (Nordelbien) Lob, Ehr sei Gott im höchsten Throne
 * 648  (Nordelbien) Lob, Ehr und Preis in süßem Ton
 * 654  (Nordelbien) Lob, Ehr und Preis sei Gott bereit’
 * 555  (reformierte) Loben wollen wir und ehren
 * 555  (Rheinland/Westfalen/Lippe) Loben wollen wir und ehren
 * 691  (reformierte) Lob, meine Seele, lobe den Herrn
 * 691  (Rheinland/Westfalen/Lippe) Lob, meine Seele, lobe den Herrn
 * 550  (reformierte) Lobpreiset all zu dieser Zeit
 * 550  (Rheinland/Westfalen/Lippe) Lobpreiset all zu dieser Zeit
 * 649  (Nordelbien) Lob sei in’s Himmels Throne
 * 538  (Niedersachsen/Bremen) Lobt den Herrn
 * 538  (Oldenburg) Lobt den Herrn
 * 573  (Hessen-Nassau) Lobt den Herrn
 * 573  (Kurhessen-Waldeck) Lobt den Herrn
 * 589  (Baden/Elsass/Lothringen) Lobt Gott in seinem Heiligtum
 * 589  (Pfalz) Lobt Gott in seinem Heiligtum
 * 537  (reformierte) Mache dich auf und werde licht
 * 537  (Rheinland/Westfalen/Lippe) Mache dich auf und werde licht
 * 539  (Bayern/Thüringen) Mache dich auf und werde licht
 * 545  (Baden/Elsass/Lothringen) Mache dich auf und werde licht
 * 545  (Pfalz) Mache dich auf und werde licht
 * 573  (Württemberg) Magnificat
 * 579  (Niedersachsen/Bremen) Magnificat
 * 579  (Oldenburg) Magnificat
 * 605  (Bayern/Thüringen) Magnificat
 * 600  (Hessen-Nassau) Magnificat anima mea
 * 600  (Kurhessen-Waldeck) Magnificat anima mea
 * 588  (reformierte) Magnificat, magnificat
 * 588  (Rheinland/Westfalen/Lippe) Magnificat, magnificat
 * 606  (Österreich) Magnificat, magnificat
 * 622  (Baden/Elsass/Lothringen) Magnificat, magnificat
 * 622  (Pfalz) Magnificat, magnificat
 * 594  (Niedersachsen/Bremen) Manchmal kennen wir Gottes Willen
 * 594  (Oldenburg) Manchmal kennen wir Gottes Willen
 * 626  (Württemberg) Manchmal kennen wir Gottes Willen
 * 633  (Österreich) Manchmal kennen wir Gottes Willen
 * 642  (Baden/Elsass/Lothringen) Manchmal kennen wir Gottes Willen
 * 642  (Pfalz) Manchmal kennen wir Gottes Willen
 * 609  (Hessen-Nassau) Masithi Amen
 * 609  (Kurhessen-Waldeck) Masithi Amen
 * 664  (Bayern/Thüringen) Mein Auge wacht jetzt in der stillen Nacht
 * 574  (Österreich) Meine engen Grenzen
 * 584  (Hessen-Nassau) Meine engen Grenzen
 * 584  (Kurhessen-Waldeck) Meine engen Grenzen
 * 589  (Württemberg) Meine engen Grenzen
 * 600  (reformierte) Meine engen Grenzen
 * 600  (Rheinland/Westfalen/Lippe) Meine engen Grenzen
 * 641  (Österreich) Meine Hoffnung und meine Freude
 * 576  (Württemberg) Meine Hoffnung und meine Freude – El Senyor és la meva força
 * 588  (Württemberg) Meine Seele in der Höhle
 * 615  (reformierte) Meine Seele steigt auf Erden
 * 615  (Rheinland/Westfalen/Lippe) Meine Seele steigt auf Erden
 * 628  (Württemberg) Meine Zeit steht in deinen Händen
 * 644  (Baden/Elsass/Lothringen) Meine Zeit steht in deinen Händen
 * 644  (Pfalz) Meine Zeit steht in deinen Händen
 * 597  (Hessen-Nassau) Mein ganzes Herz erhebet dich
 * 597  (Kurhessen-Waldeck) Mein ganzes Herz erhebet dich
 * 604  (Österreich) Mein ganzes Herz erhebet dich
 * 620  (Baden/Elsass/Lothringen) Mein ganzes Herz erhebet dich
 * 620  (Pfalz) Mein ganzes Herz erhebet dich
 * 634  (reformierte) Mein ganzes Herz erhebet dich
 * 634  (Rheinland/Westfalen/Lippe) Mein ganzes Herz erhebet dich
 * 626  (Österreich) Mein Gott, wie bist du so verborgen
 * 586  (Niedersachsen/Bremen) Mein Herz und Geist erheben dich
 * 586  (Oldenburg) Mein Herz und Geist erheben dich
 * 614  (Baden/Elsass/Lothringen) Mein Hirt ist Gott der Herre mein
 * 614  (Pfalz) Mein Hirt ist Gott der Herre mein
 * 565  (Niedersachsen/Bremen) Mein Schöpfer, steh mir bei
 * 565  (Oldenburg) Mein Schöpfer, steh mir bei
 * 593  (reformierte) Mein Schöpfer, steh mir bei
 * 593  (Rheinland/Westfalen/Lippe) Mein Schöpfer, steh mir bei
 * 547  (Württemberg) Menschen gehen zu Gott in ihrer Not
 * 639  (Bayern/Thüringen) Mir ist ein Licht aufgegangen
 * 544  (reformierte) Mit den Hirten will ich gehen
 * 544  (Rheinland/Westfalen/Lippe) Mit den Hirten will ich gehen
 * 663  (Württemberg) Mit Freuden will ich singen
 * 604  (Nordelbien) Mit Gott will ik mien’n Weg nu gahn
 * 603  (Baden/Elsass/Lothringen) Mit Kraft, Herr, wollst du selbst umgeben
 * 603  (Pfalz) Mit Kraft, Herr, wollst du selbst umgeben
 * 691  (Baden/Elsass/Lothringen) Näher, mein Gott, zu Dir
 * 691  (Pfalz) Näher, mein Gott, zu Dir
 * 621  (reformierte) Neig zu mir, Herr, deine Ohren
 * 621  (Rheinland/Westfalen/Lippe) Neig zu mir, Herr, deine Ohren
 * 574  (Württemberg) Nichts soll dich ängsten
 * 688  (Baden/Elsass/Lothringen) Nimm, Erde, was dir angehört
 * 688  (Pfalz) Nimm, Erde, was dir angehört
 * 595  (Baden/Elsass/Lothringen) Nimm hin den Dank für deine Liebe
 * 595  (Pfalz) Nimm hin den Dank für deine Liebe
 * 576  (Niedersachsen/Bremen) Noch ehe die Sonne am Himmel stand
 * 576  (Oldenburg) Noch ehe die Sonne am Himmel stand
 * 669  (Bayern/Thüringen) Nun bringen wir den Leib zur Ruh (eG-NA #510)
 * 554  (Nordelbien) Nun freut euch, Gottes Kinder all
 * 608  (Bayern/Thüringen) Nun freut euch hier und überall
 * 544  (Bayern/Thüringen) Nun freut euch, ihr Christen
 * 571  (Hessen-Nassau) Nun geh uns auf, du Morgenstern
 * 571  (Kurhessen-Waldeck) Nun geh uns auf, du Morgenstern
 * 585  (Baden/Elsass/Lothringen) Nun geh uns auf, du Morgenstern
 * 585  (Pfalz) Nun geh uns auf, du Morgenstern
 * 579  (Württemberg) Nun gib uns Pilgern aus der Quelle
 * 551  (Nordelbien) Nun ist der Himmel aufgetan
 * 561  (Österreich) Nun ist der Himmel aufgetan
 * 644  (Hessen-Nassau) Nun ist vorbei die finstre Nacht
 * 644  (Kurhessen-Waldeck) Nun ist vorbei die finstre Nacht
 * 541  (Baden/Elsass/Lothringen) Nun kommt das neue Kirchenjahr (eG-NA #55)
 * 541  (Pfalz) Nun kommt das neue Kirchenjahr (eG-NA #55)
 * 695  (reformierte) Nun lässest du, o Herr
 * 695  (Rheinland/Westfalen/Lippe) Nun lässest du, o Herr
 * 571  (Bayern/Thüringen) Nun segne und behüte uns
 * 639  (Nordelbien) Nun steht in Laub und Blüte
 * 641  (Niedersachsen/Bremen) Nun steht in Laub und Blüte
 * 641  (Oldenburg) Nun steht in Laub und Blüte
 * 642  (Niedersachsen/Bremen) Nun steht in Laub und Blüte
 * 642  (Oldenburg) Nun steht in Laub und Blüte
 * 563  (Baden/Elsass/Lothringen) Nun werden die Engel im Himmel singen
 * 563  (Pfalz) Nun werden die Engel im Himmel singen
 * 563  (reformierte) Nun werden die Engel im Himmel singen
 * 563  (Rheinland/Westfalen/Lippe) Nun werden die Engel im Himmel singen
 * 603  (reformierte) Nun werde still, du kleine Schar
 * 603  (Rheinland/Westfalen/Lippe) Nun werde still, du kleine Schar
 * 556  (Österreich) Nun wolle Gott, dass unser Sang
 * 635  (Nordelbien) Nun wollen wir singen das Abendlied
 * 637  (Niedersachsen/Bremen) Nun wollen wir singen das Abendlied
 * 637  (Oldenburg) Nun wollen wir singen das Abendlied
 * 657  (Österreich) Nun wollen wir singen das Abendlied
 * 684  (reformierte) Nun wollen wir singen das Abendlied
 * 684  (Rheinland/Westfalen/Lippe) Nun wollen wir singen das Abendlied
 * 558  (reformierte) Nun ziehen wir die Straße
 * 558  (Rheinland/Westfalen/Lippe) Nun ziehen wir die Straße
 * 559  (Nordelbien) O Adoramus te, Domine
 * 648  (Niedersachsen/Bremen) O Adoramus te. Domine.
 * 648  (Oldenburg) O Adoramus te. Domine.
 * 591  (Österreich) O Christenheit, sei hocherfreut
 * 582  (reformierte) Oculi nostri ad Dominum Deum
 * 582  (Rheinland/Westfalen/Lippe) Oculi nostri ad Dominum Deum
 * 675.2  (Nordelbien) O du Gotteslamm
 * 559  (Österreich) O du Liebe meiner Liebe
 * 176 Öffne meine Augen (Lied)|Öffne meine Augen
 * 577  (Württemberg) Öffne meine Ohren, Heiliger Geist
 * 543  (reformierte) O freudenreicher Tag
 * 543  (Rheinland/Westfalen/Lippe) O freudenreicher Tag
 * 633  (Württemberg) O Gottes Sohn, du Licht und Leben
 * 668  (Bayern/Thüringen) O Gott, von dem wir alles haben (eG-NA #410)
 * 626  (Nordelbien) O heilige Dreifaltigkeit
 * 653  (Österreich) O heilige Dreifaltigkeit
 * 628  (Niedersachsen/Bremen) O Heilige Dreifaltigkeit
 * 628  (Oldenburg) O Heilige Dreifaltigkeit
 * 660  (Bayern/Thüringen) O Heilige Dreifaltigkeit
 * 559  (Baden/Elsass/Lothringen) O herrlicher Tag, o fröhliche Zeit
 * 559  (Pfalz) O herrlicher Tag, o fröhliche Zeit
 * 560  (reformierte) O herrlicher Tag, o fröhliche Zeit
 * 560  (Rheinland/Westfalen/Lippe) O herrlicher Tag, o fröhliche Zeit
 * 656  (Bayern/Thüringen) O Herr, mache mich zu einem Werkzeug deines Friedens
 * 626  (reformierte) O Herr, mein Gott, wie bist du groß
 * 626  (Rheinland/Westfalen/Lippe) O Herr, mein Gott, wie bist du groß
 * 576  (Baden/Elsass/Lothringen) O Herr, sei mitten unter uns
 * 576  (Pfalz) O Herr, sei mitten unter uns
 * 571  (Nordelbien) O Jesu, dir sei ewig Dank
 * 583  (Österreich) O Jesu, dir sei ewig Dank
 * 588  (Baden/Elsass/Lothringen) O komm, du Geist der Wahrheit (eG-NA #199)
 * 588  (Pfalz) O komm, du Geist der Wahrheit (eG-NA #199)
 * 582.1  (Österreich) O Lamm Gottes, unschuldig (eG-NA #83)
 * 559  (Bayern/Thüringen) O Licht der wunderbaren Nacht
 * 656  (Österreich) O selges Licht, Dreifaltigkeit (eG-NA #439)
 * 665  (Nordelbien) Preis, Ehr sei Gott im allerhöchsten Thron
 * 568  (Hessen-Nassau) Preisen lasst uns Gott, den Herrn
 * 568  (Kurhessen-Waldeck) Preisen lasst uns Gott, den Herrn
 * 602  (reformierte) Reich des Herrn (eG-NA #191)
 * 602  (Rheinland/Westfalen/Lippe) Reich des Herrn (eG-NA #191)
 * 605  (Baden/Elsass/Lothringen) Reich des Herrn (eG-NA #191)
 * 605  (Pfalz) Reich des Herrn (eG-NA #191)
 * 556  (Nordelbien) Sagt, wer kann den Wind sehn?
 * 565  (Österreich) Sagt, wer kann den Wind sehn?
 * 570  (Hessen-Nassau) Sanctus
 * 570  (Kurhessen-Waldeck) Sanctus
 * 656  (Niedersachsen/Bremen) Sanctus
 * 656  (Oldenburg) Sanctus
 * 583  (reformierte) Sanctus, sanctus, sanctus
 * 583  (Rheinland/Westfalen/Lippe) Sanctus, sanctus, sanctus
 * 580.1  (Österreich) Sanctus, Sanctus, Sanctus dominus
 * 672.4  (Nordelbien) Sanctus, sanctus sanctus Dominus: Heilig, heilig, heilig ist der Herr
 * 633  (Hessen-Nassau) Sanftmut den Männern
 * 633  (Kurhessen-Waldeck) Sanftmut den Männern
 * 627  (Hessen-Nassau) Schalom, Schalom! Wo die Liebe wohnt
 * 627  (Kurhessen-Waldeck) Schalom, Schalom! Wo die Liebe wohnt
 * 635  (Württemberg) Schenk uns Weisheit, schenk uns Mut
 * 662  (Baden/Elsass/Lothringen) Schenk uns Weisheit, schenk uns Mut
 * 662  (Pfalz) Schenk uns Weisheit, schenk uns Mut
 * 614  (Nordelbien) Schweige und höre
 * 556  (Baden/Elsass/Lothringen) Seele, mach dich heilig auf
 * 556  (Pfalz) Seele, mach dich heilig auf
 * 574  (Hessen-Nassau) Segne dieses Kind
 * 574  (Kurhessen-Waldeck) Segne dieses Kind
 * 581  (Württemberg) Segne dieses Kind
 * 565  (Nordelbien) Segne dieses Kind und hilf uns, ihm zu helfen
 * 575  (reformierte) Segne und behüte
 * 575  (Rheinland/Westfalen/Lippe) Segne und behüte
 * 580  (Baden/Elsass/Lothringen) Segne und behüte
 * 580  (Pfalz) Segne und behüte
 * 562  (Hessen-Nassau) Segne und behüte uns
 * 562  (Kurhessen-Waldeck) Segne und behüte uns
 * 564  (Württemberg) Segne uns, o Herr
 * 573  (Bayern/Thüringen) Segne uns, o Herr
 * 581  (Baden/Elsass/Lothringen) Segne uns, o Herr
 * 581  (Pfalz) Segne uns, o Herr
 * 689  (reformierte) Seht das große Sonnenlicht
 * 689  (Rheinland/Westfalen/Lippe) Seht das große Sonnenlicht
 * 551  (Hessen-Nassau) Seht, der Stein ist weggerückt
 * 551  (Kurhessen-Waldeck) Seht, der Stein ist weggerückt
 * 537  (Mecklenburg) Seht, die helle Sonne
 * 587  (Baden/Elsass/Lothringen) Sei Gott getreu, halt seinen Bund
 * 587  (Pfalz) Sei Gott getreu, halt seinen Bund
 * 644  (Bayern/Thüringen) Selig seid ihr
 * 651  (Württemberg) Selig seid ihr
 * 599  (Hessen-Nassau) Selig seid ihr, wenn ihr einfach lebt
 * 599  (Kurhessen-Waldeck) Selig seid ihr, wenn ihr einfach lebt
 * 613  (Nordelbien) Selig seid ihr, wenn ihr einfach lebt
 * 636  (Österreich) Selig seid ihr, wenn ihr einfach lebt
 * 666  (reformierte) Selig seid ihr, wenn ihr einfach lebt
 * 666  (Rheinland/Westfalen/Lippe) Selig seid ihr, wenn ihr einfach lebt
 * 667  (Baden/Elsass/Lothringen) Selig seid ihr, wenn ihr einfach lebt
 * 667  (Pfalz) Selig seid ihr, wenn ihr einfach lebt
 * 629  (Nordelbien) Sieh, da hebt die Sonne sich übers Meer
 * 537  (Württemberg) Sieh, dein König kommt zu dir
 * 538  (Bayern/Thüringen) Sieh, dein König kommt zu dir
 * 544  (Baden/Elsass/Lothringen) Sieh, dein König kommt zu dir
 * 544  (Pfalz) Sieh, dein König kommt zu dir
 * 675.3  (Nordelbien) Siehe, das ist Gottes Lamm
 * 685  (Baden/Elsass/Lothringen) Siehe, ich bin bei euch alle Tage
 * 685  (Pfalz) Siehe, ich bin bei euch alle Tage
 * 539  (Württemberg) Sieh nicht an, was du selber bist
 * 657  (Nordelbien) Singet all mit hellem Tone
 * 597  (reformierte) Singet, danket unserm Gott
 * 597  (Rheinland/Westfalen/Lippe) Singet, danket unserm Gott
 * 599  (reformierte) Singet dem Herrn ein neues Lied (Mul)|Singet dem Herrn ein neues Lied
 * 599  (Rheinland/Westfalen/Lippe) Singet dem Herrn ein neues Lied (Mul)|Singet dem Herrn ein neues Lied
 * 601  (Österreich) Singet dem Herrn ein neues Lied (Schütz)|Singet dem Herrn ein neues Lied
 * 624  (reformierte) Singet dem Herrn ein neues Lied (Schütz)|Singet dem Herrn ein neues Lied
 * 624  (Rheinland/Westfalen/Lippe) Singet dem Herrn ein neues Lied (Schütz)|Singet dem Herrn ein neues Lied
 * 537  (Hessen-Nassau) Singet frisch und wohlgemut
 * 537  (Kurhessen-Waldeck) Singet frisch und wohlgemut
 * 539  (Niedersachsen/Bremen) Singet frisch und wohlgemut
 * 539  (Oldenburg) Singet frisch und wohlgemut
 * 552  (Österreich) Singet frisch und wohlgemut
 * 536  (Hessen-Nassau) Singet fröhlich im Advent
 * 536  (Kurhessen-Waldeck) Singet fröhlich im Advent
 * 627  (Baden/Elsass/Lothringen) Singet und spielet dem Herrn
 * 627  (Pfalz) Singet und spielet dem Herrn
 * 642  (reformierte) Singet und spielet dem Herrn
 * 642  (Rheinland/Westfalen/Lippe) Singet und spielet dem Herrn
 * 584  (Niedersachsen/Bremen) Singt dem Herrn ein neues Lied
 * 584  (Oldenburg) Singt dem Herrn ein neues Lied
 * 587  (Niedersachsen/Bremen) Singt dem Herrn ein neues Lied
 * 587  (Oldenburg) Singt dem Herrn ein neues Lied
 * 613  (Österreich) Singt dem Herrn ein neues Lied
 * 601  (Bayern/Thüringen) Singt dem Herrn und lobt Seinen Namen
 * 600  (Bayern/Thüringen) Singt Gott, unserm Herrn
 * 576.4  (Österreich) Singt, ihr Christen, singt dem Herrn
 * 618  (reformierte) Singt mit froher Stimm, Völker jauchzet ihm
 * 618  (Rheinland/Westfalen/Lippe) Singt mit froher Stimm, Völker jauchzet ihm
 * 549  (Bayern/Thüringen) Singt nun alle, singt mit uns
 * 541  (Hessen-Nassau) Singt, singt, singt, singt
 * 541  (Kurhessen-Waldeck) Singt, singt, singt, singt
 * 638  (Nordelbien) So ist die Woche nun geschlossen (eG-NA #451)
 * 669  (Württemberg) So ist die Woche nun geschlossen (eG-NA #451)
 * 677  (Baden/Elsass/Lothringen) So ist die Woche nun geschlossen (eG-NA #451)
 * 677  (Pfalz) So ist die Woche nun geschlossen (eG-NA #451)
 * 682  (Baden/Elsass/Lothringen) Solange die Erde steht
 * 682  (Pfalz) Solange die Erde steht
 * 692  (reformierte) Sonne scheint ins Land hinein
 * 692  (Rheinland/Westfalen/Lippe) Sonne scheint ins Land hinein
 * 629  (Baden/Elsass/Lothringen) Sonne und Mond, Wasser und Wind
 * 629  (Pfalz) Sonne und Mond, Wasser und Wind
 * 643  (Bayern/Thüringen) So prüfet doch selbst
 * 653  (Württemberg) Soviel Freude hast du, Gott
 * 660  (Baden/Elsass/Lothringen) Soviel Freude hast du, Gott
 * 660  (Pfalz) Soviel Freude hast du, Gott
 * 574  (Nordelbien) Sprick sülm, Herr, uns to samen
 * 617  (Württemberg) Stark ist meines Jesu Hand
 * 540  (Württemberg) Stern über Bethlehem
 * 545  (Bayern/Thüringen) Stern über Bethlehem
 * 542  (Hessen-Nassau) Stern über Bethlehem, zeig uns den Weg
 * 542  (Kurhessen-Waldeck) Stern über Bethlehem, zeig uns den Weg
 * 544  (Niedersachsen/Bremen) Stern über Bethlehem, zeig uns den Weg
 * 544  (Oldenburg) Stern über Bethlehem, zeig uns den Weg
 * 546  (reformierte) Stern über Bethlehem, zeig uns den Weg
 * 546  (Rheinland/Westfalen/Lippe) Stern über Bethlehem, zeig uns den Weg
 * 551  (Baden/Elsass/Lothringen) Stern über Bethlehem, zeig uns den Weg
 * 551  (Pfalz) Stern über Bethlehem, zeig uns den Weg
 * 559  (Niedersachsen/Bremen) Stimmt mit ein
 * 559  (Oldenburg) Stimmt mit ein
 * 583  (Baden/Elsass/Lothringen) Teures Wort aus Gottes Munde
 * 583  (Pfalz) Teures Wort aus Gottes Munde
 * 549  (Hessen-Nassau) Trachtet nach dem, was droben ist
 * 549  (Kurhessen-Waldeck) Trachtet nach dem, was droben ist
 * 567  (Baden/Elsass/Lothringen) Trachtet nach dem, was droben ist
 * 567  (Pfalz) Trachtet nach dem, was droben ist
 * 633  (Bayern/Thüringen) Trachtet nach dem, was droben ist
 * 538  (reformierte) Tragt in die Welt nun ein Licht
 * 538  (Rheinland/Westfalen/Lippe) Tragt in die Welt nun ein Licht
 * 539  (Nordelbien) Tragt in die Welt nun ein Licht
 * 571  (Niedersachsen/Bremen) Tragt in die Welt nun ein Licht
 * 571  (Oldenburg) Tragt in die Welt nun ein Licht
 * 588  (Hessen-Nassau) Tragt in die Welt nun ein Licht
 * 588  (Kurhessen-Waldeck) Tragt in die Welt nun ein Licht
 * 593  (Österreich) Tragt in die Welt nun ein Licht
 * 560  (Österreich) Treuer Heiland, habe Dank
 * 561  (Württemberg) Treuer Heiland, wir sind hier
 * 587  (reformierte) Ubi caritas et amor
 * 587  (Rheinland/Westfalen/Lippe) Ubi caritas et amor
 * 608  (Baden/Elsass/Lothringen) Ubi caritas et amor
 * 608  (Pfalz) Ubi caritas et amor
 * 645  (Österreich) Ubi caritas et amor
 * 651  (Bayern/Thüringen) Ubi caritas et amor
 * 571.1  (Württemberg) Ubi caritas et amor – Wo die Liebe wohnt und Güte
 * 571.2  (Württemberg) Ubi caritas et amor – Wo die Liebe wohnt und Güte
 * 624  (Nordelbien) Ubi caritas et amor: Wo die Liebe wohnt und Güte
 * 625  (Niedersachsen/Bremen) Um Himmelswillen, gebt die Erde nicht auf
 * 625  (Oldenburg) Um Himmelswillen, gebt die Erde nicht auf
 * 679  (reformierte) Und richte unsere Füße
 * 679  (Rheinland/Westfalen/Lippe) Und richte unsere Füße
 * 617  (Niedersachsen/Bremen) Unfriede herrscht auf der Erde
 * 617  (Oldenburg) Unfriede herrscht auf der Erde
 * 663  (Baden/Elsass/Lothringen) Unfriede herrscht auf der Erde
 * 663  (Pfalz) Unfriede herrscht auf der Erde
 * 671  (reformierte) Unfriede herrscht auf der Erde.
 * 671  (Rheinland/Westfalen/Lippe) Unfriede herrscht auf der Erde.
 * 577  (Niedersachsen/Bremen) Unser Gott hört den, der zu ihm rufet.
 * 577  (Oldenburg) Unser Gott hört den, der zu ihm rufet.
 * 555  (Hessen-Nassau) Unser Leben sei ein Fest
 * 555  (Kurhessen-Waldeck) Unser Leben sei ein Fest
 * 557  (Niedersachsen/Bremen) Unser Leben sei ein Fest
 * 557  (Oldenburg) Unser Leben sei ein Fest
 * 571  (reformierte) Unser Leben sei ein Fest
 * 571  (Rheinland/Westfalen/Lippe) Unser Leben sei ein Fest
 * 636  (Württemberg) Unser Leben sei ein Fest
 * 647  (Niedersachsen/Bremen) Uns ist ein Kind geboren, Halleluja
 * 647  (Oldenburg) Uns ist ein Kind geboren, Halleluja
 * 662  (Nordelbien) Vater und Sohne
 * 562  (Niedersachsen/Bremen) Vater unser im Himmel
 * 562  (Oldenburg) Vater unser im Himmel
 * 581  (Österreich) Vater unser im Himmel
 * 590  (Nordelbien) Vater unser im Himmel
 * 616  (Österreich) Vater unser im Himmel
 * 630  (Baden/Elsass/Lothringen) Vater unser im Himmel
 * 630  (Pfalz) Vater unser im Himmel
 * 659  (Niedersachsen/Bremen) Vater unser im Himmel
 * 659  (Oldenburg) Vater unser im Himmel
 * 673.1  (Nordelbien) Vater unser im Himmel
 * 673.2  (Nordelbien) Vater unser im Himmel
 * 558  (Hessen-Nassau) Vater unser im Himmel, dir gehört unser Leben
 * 558  (Kurhessen-Waldeck) Vater unser im Himmel, dir gehört unser Leben
 * 616  (Bayern/Thüringen) Vater unser im Himmel, Dir gehört unser Leben
 * 553  (Niedersachsen/Bremen) Veni Creator
 * 553  (Oldenburg) Veni Creator
 * 602  (Bayern/Thüringen) Vergiss nicht zu danken
 * 608  (Württemberg) Vergiss nicht zu danken dem ewigen Herrn
 * 617  (Österreich) Vergiss nicht zu danken dem ewigen Herrn
 * 618  (Baden/Elsass/Lothringen) Vergiss nicht zu danken dem ewigen Herrn
 * 618  (Pfalz) Vergiss nicht zu danken dem ewigen Herrn
 * 644  (reformierte) Vergiss nicht zu danken dem ewigen Herrn
 * 644  (Rheinland/Westfalen/Lippe) Vergiss nicht zu danken dem ewigen Herrn
 * 607  (Niedersachsen/Bremen) Vertrauen wagen dürfen wir getrost
 * 607  (Oldenburg) Vertrauen wagen dürfen wir getrost
 * 643  (Hessen-Nassau) Viele kleine Leute
 * 643  (Kurhessen-Waldeck) Viele kleine Leute
 * 662  (Württemberg) Viele kleine Leute
 * 538  (Hessen-Nassau) Vom Himmel hoch, o Engel, kommt
 * 538  (Kurhessen-Waldeck) Vom Himmel hoch, o Engel, kommt
 * 542  (Bayern/Thüringen) Vom Himmel hoch, o Engel, kommt
 * 541  (reformierte) Vom Himmel hoch, o Engel, kommt|Vom Himmel hoch, o Engel, kommt!
 * 541  (Rheinland/Westfalen/Lippe) Vom Himmel hoch, o Engel, kommt|Vom Himmel hoch, o Engel, kommt!
 * 658  (Österreich) Vom hohen Baum der Jahre fällt ein Blatt zu Boden
 * 653  (reformierte) Von allen Seiten umgibst du mich
 * 653  (Rheinland/Westfalen/Lippe) Von allen Seiten umgibst du mich
 * 577  (Bayern/Thüringen) Von des Himmels Thron sende, Gottes Sohn (eG-NA #231)
 * 622  (Württemberg) Von dir, o Vater, nimmt mein Herz
 * 612  (Österreich) Von Gottes Gnade will ich singen
 * 614  (Bayern/Thüringen) Von Gott kommt diese Kunde
 * 541  (Württemberg) Von guten Mächten treu und still umgeben
 * 637  (Bayern/Thüringen) Von guten Mächten treu und still umgeben
 * 652  (reformierte) Von guten Mächten treu und still umgeben
 * 652  (Rheinland/Westfalen/Lippe) Von guten Mächten treu und still umgeben
 * 595  (Nordelbien) Vor deinen Thron tret ich hiermit
 * 629  (Niedersachsen/Bremen) Vor deinen Thron tret ich hiermit
 * 629  (Oldenburg) Vor deinen Thron tret ich hiermit
 * 666  (Österreich) Vor deinen Thron tret ich hiermit
 * 662  (Bayern/Thüringen) Vor Deinen Thron tret ich hiermit
 * 560  (Nordelbien) Wach auf, meins Herzens Schöne
 * 646  (Baden/Elsass/Lothringen) Wag’s und sei doch, was du in Christus bist
 * 646  (Pfalz) Wag’s und sei doch, was du in Christus bist
 * 561  (Nordelbien) Walte, walte nah und fern
 * 578  (Württemberg) Walte, walte nah und fern
 * 584  (Baden/Elsass/Lothringen) Walte, walte nah und fern
 * 584  (Pfalz) Walte, walte nah und fern
 * 634  (Nordelbien) Walts Gott, mein Werk ich lasse
 * 555  (Österreich) Wär Christus tausendmal geboren
 * 543  (Baden/Elsass/Lothringen) Warum willst du draußen stehen (eG-NA #46)
 * 543  (Pfalz) Warum willst du draußen stehen (eG-NA #46)
 * 547  (Nordelbien) Was fürchtst du, Feind Herodes, sehr
 * 568  (Nordelbien) Wasser vom Himmel
 * 539  (Hessen-Nassau) Was soll das bedeuten? Es taget ja schon
 * 539  (Kurhessen-Waldeck) Was soll das bedeuten? Es taget ja schon
 * 615  (Württemberg) Weicht, ihr Berge, fallt, ihr Hügel (eG-NA #21)
 * 634  (Baden/Elsass/Lothringen) Weicht, ihr Berge, fallt, ihr Hügel (eG-NA #21)
 * 634  (Pfalz) Weicht, ihr Berge, fallt, ihr Hügel (eG-NA #21)
 * 646  (reformierte) Weicht, ihr Berge, fallt, ihr Hügel (eG-NA #21)
 * 646  (Rheinland/Westfalen/Lippe) Weicht, ihr Berge, fallt, ihr Hügel (eG-NA #21)
 * 642  (Hessen-Nassau) Weil Gott die Welt geschaffen hat
 * 642  (Kurhessen-Waldeck) Weil Gott die Welt geschaffen hat
 * 593  (Bayern/Thüringen) Weil ich Jesu Schäflein bin (eG-NA #423)
 * 652  (Baden/Elsass/Lothringen) Weil ich Jesu Schäflein bin (eG-NA #423)
 * 652  (Pfalz) Weil ich Jesu Schäflein bin (eG-NA #423)
 * 555  (Baden/Elsass/Lothringen) Weise mir, Herr, deinen Weg
 * 555  (Pfalz) Weise mir, Herr, deinen Weg
 * 591  (Niedersachsen/Bremen) Weiß ich den Weg auch nicht
 * 591  (Oldenburg) Weiß ich den Weg auch nicht
 * 603  (Nordelbien) Weiß ich den Weg auch nicht
 * 618  (Hessen-Nassau) Weiß ich den Weg auch nicht
 * 618  (Kurhessen-Waldeck) Weiß ich den Weg auch nicht
 * 624  (Württemberg) Weiß ich den Weg auch nicht
 * 641  (Baden/Elsass/Lothringen) Weiß ich den Weg auch nicht
 * 641  (Pfalz) Weiß ich den Weg auch nicht
 * 650  (reformierte) Weiß ich den Weg auch nicht
 * 650  (Rheinland/Westfalen/Lippe) Weiß ich den Weg auch nicht
 * 622  (Hessen-Nassau) Weißt du, wo der Himmel ist
 * 622  (Kurhessen-Waldeck) Weißt du, wo der Himmel ist
 * 622  (Nordelbien) Weit wie das Meer ist Gottes große Liebe
 * 642  (Württemberg) Welch ein Freund ist unser Jesus
 * 559  (Hessen-Nassau) Welcher Engel wird uns sagen
 * 559  (Kurhessen-Waldeck) Welcher Engel wird uns sagen
 * 632  (Hessen-Nassau) Wenn das Brot, das wir teilen
 * 632  (Kurhessen-Waldeck) Wenn das Brot, das wir teilen
 * 667  (reformierte) Wenn das Brot, das wir teilen
 * 667  (Rheinland/Westfalen/Lippe) Wenn das Brot, das wir teilen
 * 618  (Württemberg) Wenn die Last der Welt dir zu schaffen macht
 * 645  (Baden/Elsass/Lothringen) Wenn die Last der Welt dir zu schaffen macht
 * 645  (Pfalz) Wenn die Last der Welt dir zu schaffen macht
 * 655  (Baden/Elsass/Lothringen) Wenn einer sagt: Ich mag dich, du
 * 655  (Pfalz) Wenn einer sagt: Ich mag dich, du
 * 692  (Baden/Elsass/Lothringen) Wenn ich, mein Gott, einst sterben soll
 * 692  (Pfalz) Wenn ich, mein Gott, einst sterben soll
 * 608  (Nordelbien) Wenn wir in Wassersnöten sein
 * 553  (Baden/Elsass/Lothringen) Werde licht, du Stadt der Heiden (eG-NA #70)
 * 553  (Pfalz) Werde licht, du Stadt der Heiden (eG-NA #70)
 * 623  (Bayern/Thüringen) Wer Gott vertraut, hat wohl gebaut (eG-NA #347)
 * 660  (reformierte) Wer Gott vertraut, hat wohl gebaut (eG-NA #347)
 * 660  (Rheinland/Westfalen/Lippe) Wer Gott vertraut, hat wohl gebaut (eG-NA #347)
 * 664  (Österreich) Wer hat die Sonne denn gemacht
 * 616  (Nordelbien) Wer kann der Treu vergessen
 * 638  (Österreich) Wer kann der Treu vergessen
 * 649  (reformierte) Wer kann dich, Herr, verstehen
 * 649  (Rheinland/Westfalen/Lippe) Wer kann dich, Herr, verstehen
 * 546  (Hessen-Nassau) Wer leben will wie Gott auf dieser Erde
 * 546  (Kurhessen-Waldeck) Wer leben will wie Gott auf dieser Erde
 * 553  (Bayern/Thüringen) Wer leben will wie Gott auf dieser Erde
 * 634  (Österreich) Wer leben will wie Gott auf dieser Erde
 * 570  (Baden/Elsass/Lothringen) Wer sind die vor Gottes Throne (eG-NA #529)
 * 570  (Pfalz) Wer sind die vor Gottes Throne (eG-NA #529)
 * 669  (Österreich) Wer sind die vor Gottes Throne (eG-NA #529)
 * 674  (Württemberg) Wer wohlauf ist und gesund
 * 616  (Niedersachsen/Bremen) We shall overcome
 * 616  (Oldenburg) We shall overcome
 * 636  (Hessen-Nassau) We shall overcome
 * 636  (Kurhessen-Waldeck) We shall overcome
 * 652  (Württemberg) We shall overcome
 * 603  (Württemberg) When Israel was in Egypt’s land
 * 572  (Nordelbien) Wie der Herr vor Zeiten mit den Jüngern saß
 * 600  (Württemberg) Wie der Hirsch nach frischer Quelle
 * 617  (reformierte) Wie der Hirsch nach frischer Quelle
 * 617  (Rheinland/Westfalen/Lippe) Wie der Hirsch nach frischer Quelle
 * 567  (Württemberg) Wieder kommen wir zusammen
 * 633  (reformierte) Wie die Träumenden werden wir sein
 * 633  (Rheinland/Westfalen/Lippe) Wie die Träumenden werden wir sein
 * 660  (Württemberg) Wie ein Fest nach langer Trauer
 * 666  (Baden/Elsass/Lothringen) Wie ein Fest nach langer Trauer
 * 666  (Pfalz) Wie ein Fest nach langer Trauer
 * 607  (Württemberg) Wie groß ist des Allmächt’gen Güte (eG-NA #22)
 * 609  (Bayern/Thüringen) Wie groß ist des Allmächtgen Güte (eG-NA #22)
 * 610  (Österreich) Wie groß ist des Allmächtgen Güte (eG-NA #22)
 * 625  (Baden/Elsass/Lothringen) Wie groß ist des Allmächtgen Güte (eG-NA #22)
 * 625  (Pfalz) Wie groß ist des Allmächtgen Güte (eG-NA #22)
 * 662  (reformierte) Wie groß ist des Allmächtgen Güte (eG-NA #22)
 * 662  (Rheinland/Westfalen/Lippe) Wie groß ist des Allmächtgen Güte (eG-NA #22)
 * 598  (Württemberg) Wie lange willst du mein vergessen
 * 592  (Niedersachsen/Bremen) Wie mit grimmgem Unverstand
 * 592  (Oldenburg) Wie mit grimmgem Unverstand
 * 609  (Nordelbien) Wie mit grimmgem Unverstand
 * 553  (Nordelbien) Wie morgenrot der Tag erwacht
 * 544  (Württemberg) Wie schön leuchtet der Morgenstern (eG-NA #300)
 * 599  (Nordelbien) Wi hebbt en König, Lüüd höört to
 * 568  (reformierte) Wind kannst du nicht sehen
 * 568  (Rheinland/Westfalen/Lippe) Wind kannst du nicht sehen
 * 566  (Österreich) Wind, Wind, der Samen weht
 * 584  (Österreich) Wir alle essen von einem Brot
 * 678  (reformierte) Wir beten für den Frieden
 * 678  (Rheinland/Westfalen/Lippe) Wir beten für den Frieden
 * 668  (Baden/Elsass/Lothringen) Wir bitten unsern Gott
 * 668  (Pfalz) Wir bitten unsern Gott
 * 564  (Nordelbien) Wir bringen, Gott
 * 566  (Niedersachsen/Bremen) Wir bringen, Gott, dies Kind zu dir
 * 566  (Oldenburg) Wir bringen, Gott, dies Kind zu dir
 * 594  (reformierte) Wir bringen, Herr, dies Kind zu dir
 * 594  (Rheinland/Westfalen/Lippe) Wir bringen, Herr, dies Kind zu dir
 * 681  (Baden/Elsass/Lothringen) Wir danken dir, o Vater, heut
 * 681  (Pfalz) Wir danken dir, o Vater, heut
 * 610  (Nordelbien) Wir dienen, Herr, um keinen Lohn
 * 622  (Österreich) Wir dienen, Herr, um keinen Lohn
 * 561  (Bayern/Thüringen) Wir feiern deine Himmelfahrt
 * 545  (Hessen-Nassau) Wir gehn hinauf nach Jerusalem
 * 545  (Kurhessen-Waldeck) Wir gehn hinauf nach Jerusalem
 * 653  (Niedersachsen/Bremen) Wir glauben an den einen Gott (eG-NA #153)
 * 653  (Oldenburg) Wir glauben an den einen Gott (eG-NA #153)
 * 670.2  (Nordelbien) Wir glauben an den einen Gott (eG-NA #153)
 * 654  (Niedersachsen/Bremen) Wir glauben an Gott Vater
 * 654  (Oldenburg) Wir glauben an Gott Vater
 * 670.3  (Nordelbien) Wir glauben an Gott Vater
 * 591  (Bayern/Thüringen) Wir glauben dich, Gott
 * 594  (Österreich) Wir glauben dich, Gott
 * 648  (reformierte) Wir haben Gottes Spuren festgestellt
 * 648  (Rheinland/Westfalen/Lippe) Wir haben Gottes Spuren festgestellt
 * 656  (Württemberg) Wir haben Gottes Spuren festgestellt
 * 665  (Baden/Elsass/Lothringen) Wir haben Gottes Spuren festgestellt
 * 665  (Pfalz) Wir haben Gottes Spuren festgestellt
 * 575.3  (Österreich) Wir loben dich. Wir preisen dich
 * 634  (Niedersachsen/Bremen) Wir sagen dir Dank
 * 634  (Oldenburg) Wir sagen dir Dank
 * 651  (Hessen-Nassau) Wir sind mitten im Leben
 * 651  (Kurhessen-Waldeck) Wir sind mitten im Leben
 * 682  (Württemberg) Wir sind mitten im Leben zum Sterben bestimmt
 * 672  (Österreich) Wir sind nur Gast auf Erden
 * 681  (Württemberg) Wir sind nur Gast auf Erden
 * 570  (Niedersachsen/Bremen) Wir sind zum Mahl geladen
 * 570  (Oldenburg) Wir sind zum Mahl geladen
 * 598  (reformierte) Wir sind zum Mahl geladen
 * 598  (Rheinland/Westfalen/Lippe) Wir sind zum Mahl geladen
 * 541  (Niedersachsen/Bremen) Wir singen dir, Immanuel (eG-NA #59)
 * 541  (Oldenburg) Wir singen dir, Immanuel (eG-NA #59)
 * 542  (reformierte) Wir singen dir, Immanuel (eG-NA #59)
 * 542  (Rheinland/Westfalen/Lippe) Wir singen dir, Immanuel (eG-NA #59)
 * 543  (Bayern/Thüringen) Wir singen dir, Immanuel (eG-NA #59)
 * 544  (Nordelbien) Wir singen dir, Immanuel (eG-NA #59)
 * 549  (Baden/Elsass/Lothringen) Wir singen dir, Immanuel (eG-NA #59)
 * 549  (Pfalz) Wir singen dir, Immanuel (eG-NA #59)
 * 558  (Baden/Elsass/Lothringen) Wir singen und verkünden
 * 558  (Pfalz) Wir singen und verkünden
 * 631  (Nordelbien) Wir sitzen an gedeckten Tischen
 * 625  (Hessen-Nassau) Wir strecken uns nach dir
 * 625  (Kurhessen-Waldeck) Wir strecken uns nach dir
 * 642  (Bayern/Thüringen) Wir strecken uns nach dir
 * 664  (reformierte) Wir strecken uns nach dir
 * 664  (Rheinland/Westfalen/Lippe) Wir strecken uns nach dir
 * 600  (Nordelbien) Wir strecken unsre Hände aus wie leere Schalen
 * 661  (Österreich) Wir wollen dir dienen auf mancherlei Weise
 * 627  (Nordelbien) Wi seggt di Dank vun Hartensgrunn
 * 551  (Württemberg) Wo einer dem andern neu vertraut
 * 604  (Niedersachsen/Bremen) Wo ein Mensch Vertrauen gibt
 * 604  (Oldenburg) Wo ein Mensch Vertrauen gibt
 * 630  (Hessen-Nassau) Wo ein Mensch Vertrauen gibt
 * 630  (Kurhessen-Waldeck) Wo ein Mensch Vertrauen gibt
 * 638  (Württemberg) Wo ein Mensch Vertrauen gibt
 * 643  (Österreich) Wo ein Mensch Vertrauen gibt
 * 648  (Bayern/Thüringen) Wo ein Mensch Vertrauen gibt
 * 611  (Nordelbien) Wohin denn sollen wir gehen
 * 568  (Niedersachsen/Bremen) Wohlauf, die ihr hungrig seid
 * 568  (Oldenburg) Wohlauf, die ihr hungrig seid
 * 690  (Baden/Elsass/Lothringen) Wohlauf, wohlan zum letzten Gang
 * 690  (Pfalz) Wohlauf, wohlan zum letzten Gang
 * 654  (reformierte) Wo ich gehe, wo ich stehe
 * 654  (Rheinland/Westfalen/Lippe) Wo ich gehe, wo ich stehe
 * 606  (Bayern/Thüringen) Womit hat es angefangen?
 * 606  (Württemberg) Womit soll ich dich wohl loben (eG-NA #387)
 * 592  (reformierte) Wort, das lebt und spricht
 * 592  (Rheinland/Westfalen/Lippe) Wort, das lebt und spricht
 * 587  (Österreich) Wo soll ich fliehen hin (eG-NA #265)
 * 563  (Hessen-Nassau) Wo zwei oder drei
 * 563  (Kurhessen-Waldeck) Wo zwei oder drei
 * 578  (reformierte) Wo zwei oder drei
 * 578  (Rheinland/Westfalen/Lippe) Wo zwei oder drei
 * 564  (Niedersachsen/Bremen) Wo zwei oder drei in meinem Namen versammelt sind
 * 564  (Oldenburg) Wo zwei oder drei in meinem Namen versammelt sind
 * 568  (Bayern/Thüringen) Wo zwei oder drei in meinem Namen versammelt sind
 * 577  (Baden/Elsass/Lothringen) Wo zwei oder drei in meinem Namen versammelt sind
 * 577  (Pfalz) Wo zwei oder drei in meinem Namen versammelt sind
 * 597  (Österreich) Wo zwei oder drei in meinem Namen versammelt sind
 * 568  (Württemberg) Wo zwei oder drei versammelt sind
 * 603  (Hessen-Nassau) Zachäus, böser reicher Mann
 * 603  (Kurhessen-Waldeck) Zachäus, böser reicher Mann
 * 637  (reformierte) Zachäus, böser reicher Mann
 * 637  (Rheinland/Westfalen/Lippe) Zachäus, böser reicher Mann
 * 537  (Niedersachsen/Bremen) Zieh, Ehrenkönig, bei mir ein
 * 537  (Oldenburg) Zieh, Ehrenkönig, bei mir ein
 * 575  (Baden/Elsass/Lothringen) Zions Stille soll sich breiten
 * 575  (Pfalz) Zions Stille soll sich breiten
 * 578  (Bayern/Thüringen) Zum Tisch des Herren lasst uns gehen
 * 586  (Österreich) Zum Tisch des Herren lasst uns gehen
 * 569  (reformierte) Zu Ostern in Jerusalem
 * 569  (Rheinland/Westfalen/Lippe) Zu Ostern in Jerusalem
 * 555  (Niedersachsen/Bremen) Zu Ostern in Jerusalem, da ist etwas geschehn
 * 555  (Oldenburg) Zu Ostern in Jerusalem, da ist etwas geschehn
 * 556  (Hessen-Nassau) Zu Ostern in Jerusalem, da ist etwas geschehn
 * 556  (Kurhessen-Waldeck) Zu Ostern in Jerusalem, da ist etwas geschehn
 * 613  (Baden/Elsass/Lothringen) Zwei Ufer, eine Quelle
 * 613  (Pfalz) Zwei Ufer, eine Quelle
 * 582  (Niedersachsen/Bremen) Zwischen Jericho und Jerusalem
 * 582  (Oldenburg) Zwischen Jericho und Jerusalem
 * 658  (Baden/Elsass/Lothringen) Zwischen Jericho und Jerusalem
 * 658  (Pfalz) Zwischen Jericho und Jerusalem
