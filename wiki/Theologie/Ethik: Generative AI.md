# Ethik: Generative AI

## Grundlegendes

- Funktionsweise: https://www.heise.de/hintergrund/Kuenstliche-Intelligenz-Die-Mathematik-hinter-neuronalen-Netzen-9687323.html
    - https://www.heise.de/hintergrund/Physik-Nobelpreis-Wie-viel-Physik-in-neuronalen-Netzen-steckt-10001801.html
    - Visualisierung von der Funktionsweise: https://tensorspace.org/
- Soziale Implikationen: https://netzpolitik.org/2023/kuenstliche-intelligenz-eines-der-groessten-sozialen-experimente-aller-zeiten/

## Kreativität

- https://www.newyorker.com/culture/the-weekend-essay/why-ai-isnt-going-to-make-art

## Gefahren

- Geoffrey Hinton: Fakes, Code ausführen: https://www.heise.de/news/KI-Pionier-Geoffrey-Hinton-verlaesst-Google-8984065.html
- "Warnung vor KI" ist Marketing, KI ist eigentlich Überwachung: https://www.zeit.de/digital/2024-05/kuenstliche-intelligenz-meredith-whittaker-fortschritt-ueberwachung
    - Negativ-Beispiele in der Presse: https://www.tagesschau.de/wirtschaft/digitales/ki-chatgpt-100.html, https://www.zdf.de/nachrichten/heute-sendungen/forschende-warnen-ki-video-100.html

## Urheberrecht und Gesetzgebung

- Training benötigt Vervielfältigung: https://www.kulturrat.de/positionen/kuenstliche-intelligenz-und-urheberrecht/
    - "Zulässig sind nach § 44b Abs. 2 UrhG aber nur Vervielfältigungen von rechtmäßig zugänglichen Werken."
- Gesetzgebung hinkt hinterher: https://netzpolitik.org/2024/europaeische-ki-verordnung-die-eigentlichen-regeln-fuer-chatgpt-kommen-noch/

## Systemische Probleme

- https://www.heise.de/news/KI-Kollaps-droht-wegen-KI-generierter-Trainingsdaten-9823352.html
- https://www.heise.de/news/Reasoning-Fail-Gaengige-LLMs-scheitern-an-kinderleichter-Aufgabe-9755034.html (kein logisches Denken)
- https://netzpolitik.org/2024/generative-ki-bei-wikipedia-die-freie-und-ki-generierte-enzyklopaedie/ (KI vs echtes Wissen)
- Halluzinationen: https://www.heise.de/hintergrund/Kuenstliche-Intelligenz-Chatbots-bleiben-erfinderisch-9633784.html
- "You know what the biggest problem with pushing all-things-AI is? Wrong direction.
I want AI to do my laundry and dishes so that I can do art and writing, not for AI to do my art and writing so that I can do my laundry and dishes." Joanna Maciejewska, @AuthorJMac, https://x.com/AuthorJMac/status/1773679197631701238

## Klimakatastrophe

- Kipppunkt "natürliche CO2-Senken" erreicht: https://www.focus.de/earth/analyse/natuerliche-co2-speicher-ploetzlich-kommt-etwas-ins-wanken-womit-kein-klimaforscher-gerechnet-hat_id_260399917.html
- Lösung Emissionshandel: https://www.focus.de/earth/experten/allianz-chefvolkswirt-ludovic-subran-wie-retten-wir-die-natur-bekannter-oekonom-macht-drastischen-vorschlag_id_260424300.html
- KI wäre/ist damit unwirtschaftlich teuer??
- KI bringt Atomreaktor wieder in Betrieb (Deal MS): https://www.borncity.com/blog/2024/09/26/ai-microsoft-explodierende-energieverbraeuche-und-kosten/
    - https://www.heise.de/hintergrund/Warum-Google-Microsoft-und-Amazon-auf-Atomenergie-setzen-9999297.html
- https://www.tagesschau.de/wirtschaft/energie/kuenstliche-intelligenz-energieverbrauch-100.html
- Mit der Menge der verarbeiteten Daten steigt der Energieverbrauch: https://oecd.ai/en/wonk/understand-environmental-costs
- https://netzpolitik.org/2020/hoffnung-fuer-den-umweltschutz-oder-oekoproblem/

## Menschenrechte

- Datenarbeiter:innen: https://netzpolitik.org/2023/datenarbeit-wie-millionen-menschen-fuer-die-ki-schuften/
    - https://netzpolitik.org/2024/data-workers-inquiry-die-versteckten-arbeitskraefte-hinter-der-ki-erzaehlen-ihre-geschichten/
- Rassismus / Sexismus: https://netzpolitik.org/2023/vereinte-nationen-warnung-vor-rassistischer-und-sexistischer-ki/

## Ökonomie / Theologie

- Sam Altman weiß nicht, wie er mit einer AGI (allgemeinen künstlichen Intelligenz) Geld verdienen will, aber er wird sie bauen und sie dann fragen: https://www.reddit.com/r/MachineLearning/comments/bsij7c/news_sam_altman_on_openais_business_model/ - https://www.threads.net/@nixcraft/post/C5vj0naNlEq?hl=de
- Abwägungen des Ethikrates: https://www.ethikrat.org/fileadmin/Publikationen/Stellungnahmen/deutsch/stellungnahme-mensch-und-maschine.pdf

## Gute Anwendungen von KI (keine LLM/GenAI)

- Gesichtserkennung auf dem Mac mit iPhoto seit 2009: https://www.macworld.com/article/194779/iphoto_faces.html
- https://www.heise.de/news/KI-basiertes-Rettungssystem-kann-Menschen-vor-dem-Ertrinken-retten-10005783.html
- https://www.heise.de/news/Erste-Erkenntnisse-Was-KI-ueber-die-Sprache-der-Wale-verraet-9578936.html
