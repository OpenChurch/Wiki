# Theologie der Offenen Quellen

## Literatur

### Allgemein

- Johannes Brakensiek: [Folien des Pfarrkonvents "Digitalisierung" des Kirchenkreises Dinslaken im August 2022](https://codeberg.org/OpenChurch/Wiki/src/branch/main/wiki/Theologie/Ethik)

### Digitale Nachhaltigkeit

(Literaturhinweise entnommen aus dem internen LUKi-Wiki.)

- Ralf Peter Reimann: [Digitale Nachhaltigkeit: Was ist das? Wie geht das?](https://theonet.de/2015/10/19/digitale-nachhaltigkeit-was-ist-das-wie-geht-das/) Blogbeitrag vom 19.20.2015.
- Meike Richter: [Fair Code - Free/Open Source Software and the Digital Divide. 2005.](http://www.commonspage.net/fair-code/)
    - Sie hat auch weitere Artikel im Open Source Jahrbuch veröffentlicht und auf dem [23C3](http://www.commonspage.net/fair-code/) des CCC darüber gesprochen.
- Thorsten Busch: [Open Source und Nachhaltigkeit](http://www.golem.de/0803/58082.html). 2008.
- Georg Greve (FSFE): [So what might Digital Sustainability be?](https://blogs.fsfe.org/greve/?p=462)
- Marcus M. Dapp: Mehrere Lehrveranstaltungen und Dokumente -- u.a.: <http://wiki.project21.ch/images/5/51/Digitalenachhaltigkeit_2010.pdf>
- Melanie Grießer: [Digitale Nachhaltigkeit.](http://semelina.de/wp-content/uploads/2013/10/Digitale-Nachhaltigkeit.pdf) Interdisziplinäre Transformation eines ökologischen Begriffs. 2013.
- Matthias Stürmer: [Nachhaltig in die digitale Zukunft](http://www.digitale-nachhaltigkeit.unibe.ch/unibe/portal/fak_wiso/a_bwl/inst_wi/abt_digital/content/e273593/e304567/e359666/UniPress2015-NachhaltigindiedigitaleZukunft_ger.pdf), UniPress 2015.
    - Matthias Stürmer: [Characteristics of Digital Sustainability](http://www.stuermer.ch/maemst/wp-content/uploads/2014/10/2014_Stuermer_CharacteristicsOfDigitalSustainability.pdf) (ähnlicher Inhalt wie oben)
- Matthias Stürmer: [Digitale Nachhaltigkeit: Digitale Gemeingüter für die Wissensgesellschaft der Zukunft](https://www.parldigi.ch/de/2017/07/digitale-nachhaltigkeit/). 10 Voraussetzungen für digital nachhaltige Güter. Blogbeitrag vom 7.7.2017 mit Hinweis auf die entsprechende wissenschaftliche Publikation.
- [project21: Studio!Sus 14 - Digitale Nachhaltigkeit](https://www.project21.ch/series/ss-014-digitalenachhaltigkeit/) - [hier die PDF des Magazins](https://www.project21.ch/images/stories/studiosus/PDFs/Studiosus-14-Digitale_Nachhaltigkeit.pdf)

### Ökologische Nachhaltigkeit

- Jens Gröger, Marina Köhn: [Nachhaltige Software](https://www.umweltbundesamt.de/publikationen/nachhaltige-software). Dokumentation des Fachgesprächs „Nachhaltige Software“ am 28.11.2014. Juni 2015.
- Detlef Thoms: [Sustainable Programming – Softwarecode ohne Stromfresser.](https://www.heise.de/developer/artikel/Sustainable-Programming-Softwarecode-ohne-Stromfresser-4197828.html) Wie man Anwendungssysteme dazu bringt, den Energieverbrauch einer ganzen Stadt zu sparen. heise Developer 30.10.2018.
- Nikita Prokopov: [Software disenchantment](https://tonsky.me/blog/disenchantment/). September 2018. Grundsatzschrift über unnachhaltige Softwareentwicklung. Übersetzt in mehrere Sprachen.
-  Anja Höfner (Hrsg.), Vivian Frick (Hrsg.): [Was Bits und Bäume verbindet](https://www.oekom.de/buch/was-bits-und-baeume-verbindet-9783962381493?p=1). Digitalisierung nachhaltig gestalten. oekom 04.07.2019.  Ergebnisse der Konferenz „bits&baeume“.


### Reformation

- Matthias Gockel: Ad fontes - Offene Quellen als Thema der Theologie. Fundamentaltheologische Überlegungen. Cursor_ Zeitschrift Für Explorative Theologie 2018. https://doi.org/10.21428/d1d24432
- Benedikt Friedrich: Exploring Freedom. A Conversation between FLOSS-Culture and Theological Practices of Freedom. Cursor_ Zeitschrift Für Explorative Theologie 2019. https://doi.org/10.21428/fb61f6aa.9cf71305
- die „netztheologen“: [Open Source: Offene Quellen, Copyleft und Offenbarung](https://netztheologen.podigee.io/12-open-source-offene-quellen-copyleft-und-offenbarung). Podcastfolge vom 15. Mai 2020.


### Allmende / Gemeingüter / Commons / Bildungs- und Wettbewerbsgerechtigkeit

- GEMEINGÜTER-Initiative von Pauluskirche und Kultur der Ev. Lydia-Gemeinde Dortmund: [37. Deutscher Evangelischer Kirchentag. Resolution: „Gottes Güter umsonst – Einfach frei](https://static.kirchentag.de/production/htdocs/fileadmin/dateien/Resolutionen/DEKT37_Resolution_Gottes_Gueter_umsonst.pdf)
- Christoph Fleischmann: Gottes Güter für alle. Die verdrängte Lehre der Reformation.  Blätter für deutsche und internationale Politik 5/2017, S. 41-49.
- Reformation und Ökonomie. [Gottes Güter umsonst](https://www.deutschlandfunkkultur.de/gottes-guter-umsonst-reformation-und-okonomie-feature-von.media.c5129c4eadd9fb0a0ecd25e8f4ef5c70.pdf). Ein Radiofeature von Christoph Fleischmann. Deutschlandradio Kultur vom 16. April 2017.
- Friedrich Laker: [Commons als jüdisch- (ur)christliche Lebensform.](https://theologiedeslebens.blog/2019/10/23/commons-als-urchristliche-lebensform/) Blogbeitrag vom 23.10.2019.
- [Public Money, Public Code](https://publiccode.eu/de/) - [Argumentation des LUKi e.V.](https://luki.org/mitmachen/public-money-public-code/)
- Ulrich Berens: [Fair-gehandelte Bananen und Freie Software: Ein Imperativ für Christen](https://theonet.de/2012/03/15/fair-gehandelte-bananen-und-freie-software-ein-imperativ-fur-christen/). Blogbeitrag vom 15.3.2012.
- Ralf Peter Reimann: [Open Source, Open Content, Open Educational Resources – was bewegt die Kirche?](https://theonet.de/2013/10/16/open-source-open-content-open-educational-resources/). Blogbeitrag vom 16.10.2013.
- Sven Hilbig: [Patente auf Software: Schutz auf Kosten der Armen?](https://www.brot-fuer-die-welt.de/blog/2019-patente-auf-software-schutz-auf-kosten-der-armen/). Blogbeitrag für Brot für die Welt vom 04.11.2019.
- Adrian Lobe: [Programmierer - die „leisesten einflussreichsten Leute auf dem Planeten“](https://www.sueddeutsche.de/digital/clive-thompson-programmierer-the-making-of-a-new-tribe-and-the-remaking-of-the-world-1.4756095). Rezension von „Clive Thompson: Coders: The Making of a New Tribe and the Remaking of the World, Penguin Press 2019“ bei der Süddeutschen vom 19. Januar 2020.
