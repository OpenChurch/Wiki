# Church Management Software

## Übersichten

- [List of Open Source Church Software ](https://gist.github.com/seven1m/aa2b43208293c01ce3698a89260712b7)

## Hilfe vermitteln

- https://easyappointments.org/
- Corona Hilfe CiviCRM Erweiterung - https://github.com/systopia/de.systopia.mutualaid/blob/master/README_DE.md
- https://nachbarschaft.care/, Quellcode: https://github.com/timosystems/nachbarschaft.care
