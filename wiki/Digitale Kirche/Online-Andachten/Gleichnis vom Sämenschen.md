Gleichnis vom Sämenschen
========================

In einem [separaten Repository](https://codeberg.org/OpenChurch/OnlineAndacht-Mk4) finden sich die Quelldateien für eine interaktive Online-Andacht zum Sämenschen aus [Markus 4,3-9](https://www.bibleserver.com/LUT/Markus4%2C3-9). 

Die Andacht ist derzeit (27. Oktober 2024) noch hier erlebbar: https://andacht.walsum-vierlinden.de/