# Datenschutzinformationen zum Gottesdienst-Livestream
Liebe Gottesdienstbesucher,  
wir übertragen den heutigen Gottesdienst per Livestream ins Internet. Wir beachten dabei das Datenschutzgesetz der Evangelischen Kirche in Deutschland (DSG-EKD) und informieren Sie im Folgenden über die Datenverarbeitung und Ihre Rechte.

## Verantwortliche Stelle
*Bitte Kontaktdaten der Kirchengemeinde eintragen*

## Örtlich Beauftragte(r) für Datenschutz
*Bitte Kontaktdaten eintragen oder Hinweis auf andere Ansprechpartner, z.B. „Unsere Gemeinde hat keinen örtlich Beauftragten für Datenschutz bestellt. Sie können sich mit Anliegen zum Datenschutz an das Pfarramt wenden.“*

## Zweck und Rechtsgrundlage der Datenverarbeitung
Der Gottesdienst wird live ins Internet übertragen, damit Menschen zuhause mitfeiern können. Dabei können Bild- und Tonaufnahmen von Personen, die in der Kirche am Gottesdienst beteiligt sind, auch von Gemeindegliedern, übertragen werden. Rechtsgrundlage dafür ist § 53 DSG-EKD.

## Empfänger der Daten und Übermittlung in Drittländer
Der Livestream ist im Internet für die weltweite Öffentlichkeit zugänglich. Er kann auch in Ländern abgerufen werden, in denen andere Datenschutzgesetze gelten als in der EU und im EWR oder in denen überhaupt keine Datenschutzgesetze gelten.

Wir nutzen zur Übertragung Dienste der Firma *bitte einfügen*. Der Zugriff dieser Firma auf die Daten wird durch einen Auftragsverarbeitungsvertrag mit unserer Gemeinde kontrolliert.

*Falls die Zusammenarbeit mit einem Dienstleister nicht als Auftragsverarbeitung, sondern in Gemeinsamer Verantwortung stattfindet, z.B. bei einem Sozialen Netzwerk, das mit der Übertragung von Livestreams auch eigene Zwecke verfolgt oder in Zusammenarbeit mit der Landeskirche, wäre hier anstelle des Hinweis auf Auftragsverarbeitung ein Hinweis auf die gemeinsam verantwortlichen Stellen und Regelungen zur Erfüllung datenschutzrechtlicher Pflichten im Rahmen der gemeinsamen Verantwortung erforderlich.*

## Speicherdauer der Daten
Die Daten werden live übertragen und dazu nur kurzfristig im Aufzeichnungs- und Übertragungssystem gespeichert, soweit dies technisch erforderlich ist. Wir können aber nicht ausschließen, dass Personen oder Organisationen, die den Livestream über das Internet empfangen, Kopien erstellen und speichern. Wir haben darauf, wie bei allen Inhalten im Internet, keinen Einfluss.

## Erforderlichkeit der Aufnahmen
Damit wir für Menschen, die unsere Kirche nicht besuchen können, einen Gottesdienst-Livestream bereitstellen können, ist die Aufnahme des Altarraums und der unteren Bankreihen erforderlich. Sie können unseren Gottesdienst auch auf der Empore mitfeiern. Wer dort sitzt, wird nicht gefilmt. Aufnahmen von Ihrer Person sind deshalb zur Teilnahme am Gottesdienst nur erforderlich, wenn Sie unten sitzen wollen. Bitte sprechen Sie uns an, wenn Sie aufgrund körperlicher Einschränkungen nicht auf die Empore gehen können, aber trotzdem nicht gefilmt werden möchten. Wir finden dann eine individuelle Lösung.

*Bitte an örtliche Verhältnisse anpassen.*

## Ihre Rechte
*Die folgenden Sätze wurden aus der Arbeitshilfe des BfD EKD zu Informationspflichten übernommen. Der erste Absatz wurde für die Datenverarbeitung bei Livestreams angepasst. Vgl. <https://datenschutz.ekd.de/infothek-items/arbeitshilfe-zur-umsetzung-von-informationspflichten/>.*

Sie können Auskunft darüber verlangen, ob Bild- oder Tonaufnahmen von Ihrer Person im Livestream enthalten sind. Ist dies der Fall, so haben Sie ein Recht auf Auskunft über diese personenbezogenen Daten sowie auf weitere mit der Verarbeitung zusammenhängende Informationen (§ 19 DSG‑EKD). Bitte beachten Sie, dass eine Auskunft nicht mehr möglich ist, wenn die Übertragung und kurzfristige Datenspeicherung beendet ist.

Für den Fall, dass personenbezogene Daten über Sie nicht (mehr) zutreffend oder unvollständig sind, können Sie eine Berichtigung und gegebenenfalls Vervollständigung dieser Daten verlangen (§ 20 DSG‑EKD).

Liegen die gesetzlichen Voraussetzungen vor, so können Sie die Löschung oder Einschränkung der Verarbeitung verlangen, vom Recht auf Datenübertragbarkeit Gebrauch machen sowie Widerspruch gegen die Verarbeitung einlegen (§§ 21, 22, 24, 25 DSG‑EKD).

Bitte setzen Sie sich mit uns in Verbindung, wenn Sie ein Betroffenenrecht geltend machen möchten.

## Beschwerderecht
*Auch der folgende Text ist aus der Arbeitshilfe des BfD EKD übernommen.*

Jede betroffene Person kann sich gemäß § 46 Abs. 1 DSG‑EKD unbeschadet weiterer Rechtsbehelfe mit einer Beschwerde an die zuständige Aufsichtsbehörde wenden, wenn sie der Ansicht ist, bei der Erhebung, Verarbeitung oder Nutzung ihrer personenbezogenen Daten durch kirchliche Stellen in ihren Rechten verletzt worden zu sein.

Gemäß § 46 Abs. 3 DSG‑EKD darf niemand wegen der Mitteilung von Tatsachen, die geeignet sind, den Verdacht aufkommen zu lassen, das kirchliche Datenschutzgesetz oder eine andere Rechtsvorschrift über den Datenschutz sei verletzt worden, gemaßregelt oder benachteiligt werden. Mitarbeitende der kirchlichen Stellen müssen für Mitteilungen an die Beauftragten für den Datenschutz nicht den Dienstweg einhalten.
Die zuständige Aufsichtsbehörde erreichen Sie unter:

*Bitte Kontaktdaten der passenden BfD-EKD-Außenstelle einfügen  
siehe <https://datenschutz.ekd.de/ueber-uns/unsere-struktur/>*