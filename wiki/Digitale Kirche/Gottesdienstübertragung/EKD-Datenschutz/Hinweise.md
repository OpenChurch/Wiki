# Hinweise zum Datenschutz bei Gottesdienstübertragungen (DSG-EKD)
*Die folgenden Hinweistexte sollten für alle Besucher eines Gottesdienstes, der live übertragen wird, sichtbar sein. Sie können z.B. auf einem Aushang am Eingang stehen oder in einem Liedblatt. Gottesdienstbesucher sollten die Möglichkeit haben, den Gottesdienst im Kirchenraum mitzufeiern ohne von der Kamera erfasst zu werden. Der Hinweis darauf, wo nicht gefilmt wird, muss natürlich an die örtlichen Gegebenheiten angepast werden.*

*Gottesdienstbesucher können Informationen zur Datenerhebung nach § 17 DSG-EKD verlangen. Dazu sollte ein [Infoblatt](Infoblatt_Livestream-ohne-Speicherung.md) vorbereitet sein. Der Hinweis darauf, wo dieses Infoblatt erhältlich ist, muss natürlich auch an die örtlichen Gegebenheiten angepasst werden.*

## für Internet-Livestream ohne Speicherung
Bitte beachten Sie: Dieser Gottesdienst wird per Livestream ins Internet übertragen. Die Empore wird nicht gefilmt. Dort können Sie sitzen, wenn Sie im Livestream nicht sichtbar sein möchten. Weitere Informationen zum Datenschutz erhalten Sie bei unserer Mesnerin.