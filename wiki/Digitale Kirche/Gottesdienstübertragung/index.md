# Gottesdienstübertragung

## 1. Erfahrungsberichte und Anleitungen

- von Wolfgang Loest:
    - https://erloest.wordpress.com/2016/06/10/multi-cam-livestreaming-fuer-kleines-geld/
    - https://erloest.wordpress.com/2017/04/11/mein-livestream-setup-mit-obs/
    - https://erloest.wordpress.com/2020/01/23/technikliste-januar-2020/
- [Christoph Breit, ELKB: Gottesdienste und Veranstaltungen live streamen](https://kirchedigital.blog/2020/03/11/gottesdienste-und-veranstaltungen-live-streamen/)
- [Tipps der Evangelischen Landeskirche in Baden](https://www.ekiba.de/html/content/tipps_fuer_gemeinden_zu_digitalen_angeboten.html)
- [Tipps für eine Online-Andacht auf Ihrer Homepage](https://www.netzmarginalien.de/content/tipps-fuer-online-andacht-auf-musterwebsite)


## 2. Lösungen

### 2.1 Software

- https://obsproject.com/
- https://softvelum.com/larix/ (verschiedene Lösungen, unter anderem Apps für Endanwender)
- https://mistserver.org/ (Streaming-Server)

#### 2.1.2 Anleitungen für Software

- QuickStart für [OBS](https://obsproject.com/): https://www.streamhow.de/tutorials/obs/studio-quickstart/
- QuickStart für [OBS](https://obsproject.com/) - YouTube-Tutorial: https://www.youtube.com/watch?v=KF7MyEfaG1I
- [MistServer](https://mistserver.org/) mit OBS aufsetzen: https://news.mistserver.org/news/59/Live+streaming+with+MistServer+and+OBS+Studio
- eigenen Live-Streaming-Server mit nginx und RTMP-Modul aufsetzen: https://gerpei.de/eigener-live-streaming-server-setup/
- [Bauchbinden in OBS mit OpenLP](Bauchbinden_in_OBS_mit_OpenLP.md)


### 2.2 Hardware

- https://www.blackmagicdesign.com/products/atemmini


### 2.3 Komplett im kirchlichen Einsatz

- Hervorragende Dokumentation der Komplettlösung (Hardware, Streaming mit OBS und MistServer, Integration von Jitsi Meet für VideoChat) durch die EFG Bad Lausick (Bernd Reuther): https://doku.jgz-energie.net/video-livestream/
- [Hai-End Streaming - Opensource Raspberry Pi based Streaming Solution](https://hai-end-streaming.de/)

## 3. Rechtliches (ohne Gewähr)
Brauchen wir eine **Rundfunklizenz?**  [Checkliste zur Einordnung von Streaming-Angeboten im Internet](https://www.die-medienanstalten.de/fileadmin/user_upload/Rechtsgrundlagen/Richtlinien_Leitfaeden/Checkliste_-_Streaming-Angebote_im_Internet.pdf)

### 3.1 Datenschutz
- **für Teilnehmende in der Kirche**
    - *in der EKD:* Livestreams sind zulässig, „wenn die Teilnehmenden durch geeignete Maßnahmen über Art und Umfang der Aufzeichnung oder Übertragung informiert werden.“ (§ 53 DSG-EKD) Zur Information sind kurze [Hinweise](EKD-Datenschutz/Hinweise.md) für alle sinnvoll, ein ausführliches [Infoblatt](EKD-Datenschutz/Infoblatt_Livestream-ohne-Speicherung.md) für Personen, die genaue Infos verlangen, ist aber auch erforderlich.
    - *während der Corona-Krise* dürfen Gemeindeglieder sowieso nicht in der Kirche am Gottesdienst teilnehmen. Die Mitarbeitenden, die noch in der Kirche Gottesdienst feiern, dürften wissen, dass der Gottesdienst gestreamt wird. Ein besonderer Hinweis ist deshalb nicht nötig. Die Mitarbeitenden könnten das [Infoblatt](EKD-Datenschutz/Infoblatt_Livestream-ohne-Speicherung.md) aber auch verlangen.
- **für Stream-Nutzer_innen** (hier gelten die üblichen Anforderungen an Online-Angebote)
    - beim *Stream auf der Gemeindewebsite oder einem Online-Portal, das im Auftrag der Gemeinde betrieben wird* muss die Gemeinde eine Datenschutzerklärung bereitstellen (und ggf. einen Auftragsverarbeitungsvertrag mit einem Serviceprovider abschließen)
    - beim *Stream in einem Sozialen Netzwerk* (z.B. YouTube oder Facebook) oder bei einem ähnlichen Provider, der mit dem Streaming auch eigene Zwecke verfolgt und bei seiner Tätigkeit nicht an Weisungen der Gemeinde gebunden ist, sind Gemeinde und Soziales Netzwerk gemeinsam für die Datenverarbeitung verantwortlich. In einer Vereinbarung („Joint Controller Agreement“) müsste festgelegt werden, welche Stelle welche datenschutzrechtlichen Verpflichtungen erfüllt. Stream-Nutzer_innen müssen darüber informiert werden.

### 3.2 Urheberrecht, Leistungsschutzrechte u.ä.

- **GEMA**-pflichtige Musikwerke
    - *während der Corona-Krise:* was im Kirchenraum erlaubt ist, darf auch im Livestream genutzt werden, weil die GEMA pauschale Regelungen für Veranstaltungen der katholischen und evangelischen Kirche sowie anderer Veranstalter mit Rahmenverträgen auch auf Livestreams und Downloads anwendet  (Quelle: <https://www.katholisch.de/artikel/24882-gema-zeigt-sich-kulant-bei-gottesdienst-streaming>)
    - *auf YouTube:* GEMA rechnet Vergütung direkt mit YouTube ab (vgl. [Zitat aus GEMA-Schreiben im Artikel von Christoph Breit](https://kirchedigital.blog/2020/03/11/gottesdienste-und-veranstaltungen-live-streamen/))
    - *in der EKD:* ein Rahmenvertrag erlaubt die Verwendung von GEMA-pflichtigem Material ohne Einzelvergütung für den gottesdienstlichen Gebrauch auf Internetseiten, die von EKD-Mitgliedern betrieben werden; Streams müssen aber klar als Gottesdienst erkennbar sein (z.B. durch liturgische Begrüßung am Anfang und Segen am Ende).  
      Achtung: Für Musikstücke, die von Tonträgern eingespeist werden, gilt der Rahmenvertrag nicht!
    - *in Freikirchen:* für Kirchen und Gemeindebünde in der Vereinigung Evangelischer Freikirchen (VEF) hat die EKD mit der GEMA eine Zusatzvereinbarung zu ihrem Rahmenvertrag abgeschlossen, durch diese Vereinbarung gilt der EKD-Rahmenvertrag auch für VEF-Mitglieder
- VG Musikedition: **Lieder und Liedtexte** dürfen bis 15.9.20 zeitgleich mit einem Livestream und zeitversetzt bis zu 72 Stunden nach der Übertragung in Gottesdienst-Streams eingeblendet werden (vgl. <https://www.evangelisch.de/inhalte/167458/19-03-2020/einblenden-von-liedern-gottesdienst-livestreams-erlaubt>)
