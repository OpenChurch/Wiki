# Bauchbinden in OBS mit OpenLP

OpenLP ist ein gutes Tool, um die Daten für die Anzeige von (Lied-)Texten in Gottesdienstübertragungen zu liefern. Hier wird beschreiben, wie man OpenLP unter Linux mit OBS verbindet.

## 1. Stage-View in OpenLP bereit stellen

In OpenLP richtet man eine eigene Stage-View ein, die den Text der Bauchbinden in einer transparenten Webseite vor einem halbtransparenten Hintergrund ausgibt. Dafür ist dieses Stage-View gut: https://codeberg.org/Kirchenkreis_Dinslaken/openlp-stage-obs-bauchbinde


## 2. Stage-View in OBS als Bauchbinde einbinden

Bernd Reuther beschreibt den Vorgang hier sehr ausführlich: https://git.jgz-energie.net/ENERGIE/openlp-liedtexte-in-obs

Einziger Unterschied: Er verwendet die Standard-Stage-View von OpenLP und modifiziert diese von OBS aus. Wer die Stage-View von 1. verwendet, geht den umgekehrten Weg (OpenLP ergänzen, in OBS einfach übernehmen).

## 3. Tipps und Tricks

Wenn man keinen Beamer, aber zwei Monitore angeschlossen hat (OpenLP also nur für das Live-Streaming braucht), empfiehlt es sich, über die Einstellungen das Feld für die OpenLP-Anzeige manuell an einen Ort zu verschieben, an dem es nicht stört, an dem man aber trotzdem die Kontrolle über die Ausgabe der Live-View hat.

Über die Größe der Anzeige bzw. die Einstellungen des Templates kann man dann auch einstellen, wie viel Text in der Stage-View für OBS gleichzeitig angezeigt werden soll.
