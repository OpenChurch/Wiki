# Online-Konferenzen

## Übersichten

- Öffentlich zugängliche BigBlueButton-Installationen: https://pads.ccc.de/bbbliste


## Tools für Bildungszwecke

- s. [Didaktik](../Gemeindepädagogik/Didaktik/Digitale Gemeindepädagogik.md)


## Übersicht technische Architektur (WebRTC - Browsertechnik)

- https://webrtcglossary.com/mesh/


## Open Source Software für Videokonferenzen

### [Jitsi Meet](https://jitsi.org/meet/)

- Vom LUKi unter https://jitsi.luki.org angebotenen, mit Telefoneinwahl
- in der EKiR Teil des von der Landeskirche angebotenen Software-Stacks
- Jitsi-Desktop-Client für Linux (als AppImage) und Mac hier: http://github.com/jitsi/jitsi-meet-electron
- Jitsi funktioniert mit vielen Teilnehmerinnen nur dann effektiv, wenn keine Teilnehmerin Firefox verwendet. Quelle: https://ruhr.social/@fruechtchen/103817820621669438,  https://community.jitsi.org/t/jitsi-meet-room-for-30-participants/20375
    - Korrektur: Jitsi Meet funktioniert ab Version >= 2.1.183 mit Firefox >= 68, s. https://www.youtube.com/watch?v=y4CW3c_Es4I&t=436
- Version der Installation ermitteln: Ein laufendes Meeting (also mit mindestens 2 Teilnehmern) öffnen, dann die Firefox-Web-Konsole öffnen, dort in den Log-Meldungen nach "version" suchen.
- Chome funktioniert gut, die sichere Variante davon ist [Iridium](https://iridiumbrowser.de/)
- Offizielle Dokumentation: https://jitsi.github.io/handbook/docs/intro
- Jitsi Meet skalieren:
    - https://github.com/jitsi/jitsi-meet/blob/master/doc/scalable-installation.md
    - https://www.slideshare.net/AnnikaWickert/freifunk-munich-how-to-scale-jitsi-231999167
- Hervorragende deutschsprachige Anleitung inkl. Anbindung von Asterisk zur Telefoneinwahl durch die EFG Bad Lausick (Bernd Reuther): https://doku.jgz-energie.net/jitsi-meet/


### [Nextcloud Talk](https://nextcloud.com/de/talk/)

- in der ELKB Teil der elkbCloud
- in die Nextcloud integriert
- Vorteil: Dateien, Projekten, Benutzergruppen u.ä., die in der Cloud schon bestehen, können einfach mit Chats und Videokonferenzen verknüpft werden
- Nachteil: wenn die Cloud überlastet ist (weil viele Nutzer_innen gleichzeitig ins Home-Office gehen), ist auch Talk nicht nutzbar
- Hinweis: Videokonferenzen funktionieren nur mit wenigen Nutzer_innen zuverlässig. Ab 5-6 Video-Nutzer_innen in einer Unterhaltung treten Übertragungsprobleme auf.

### [BigBlueButton](https://bigbluebutton.org/) - [Test](https://test.bigbluebutton.org/)

- Mehr didaktische Möglichkeiten als Jitsi, besserer Ton (SIP)
- Infos und Hinweise zur Selbst-Installieren (aufwändig, Betrieb Profis überlassen): https://andersgood.de/blog/bigbluebutton-eine-selbstgehostete-gotowebinar-alternative
- Load Balancer für große Installationen: https://github.com/blindsidenetworks/scalelite
- Hoster:
    - Hostsharing - https://www.hostsharing.net/blog/2020/03/30/bigbluebutton-im-lasttest/
    - Open Source Company - https://www.open-source-company.de/bigbluebutton-hosting/

### [Apache OpenMeetings](https://openmeetings.apache.org/)


### Matrix/Element

- die [Matrix](https://matrix.org/)-Apps [Element](https://element.io/) unterstützen Sprach-Anrufe für die mobilen Endgeräte. Im Browser und in den Desktop-Anwendungen sind darüber hinaus auch Video-Konferenzen via Jitsi (s.o.) und Element Call möglich.
- Support für diese Anwendungen in den Matrix-Chat-Räumen:
    - ```#matrixtest:synod.im```(für Anwender_innen)
    - ```#librechurch:synod.im``` (für Betreiber_innen)
    - ```#synapse-admins:bau-ha.us``` (für Betreiber_innen)
    - Anmeldung bei Bedarf: https://web.synod.im/#/welcome

### Palava.tv
- https://palava.tv/ - https://github.com/palavatv/palava
- Open Source Software von einem deutschen Verein betrieben, wirkt so als käme sie aus dem CCC-Umfeld ;-)


## Proprietäre Lösungen für Videokonferenzen

- https://whereby.com/


## Open Source Software für Audiokonferenzen

- Mumble: https://www.mumble.info/downloads/
    - Server: meet.teckids.org; mumble.luki.org; mumble.freifunk-rheinland.net
        - bei Nutzung bitte eine Spende für die Vereine in Erwägung ziehen


## AdHoc-Telefonkonferenzen

- bis 5 Teilnehmer_innen kostenlos unter https://konferenzen.eu


## Lösungen für Cloud-Telefonanlagen (Telefonkonferenzen und Co.)

- https://www.sipgate.de/
- https://www.sipgateteam.de/
- https://www.placetel.de/
- https://www.pascom.net/de/ (2 Leitungen kostenlos)
