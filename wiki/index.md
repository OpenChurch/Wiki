# Über dieses Projekt

 <img align="right" src="https://codeberg.org/OpenChurch/Wiki/media/branch/main/assets/OpenChurch_Wiki_Logo.jpg" alt="Ein großes rundes, buntes Kirchenfenster, eine runde Struktur mit konzentrischen Kreisen, in der Mitte eine Kreuzform." width="38%" /> 

OpenChurch.Wiki ist eine Plattform, mit der Menschen im kirchlichen Bereich ihre tägliche (theologische und musikalische) Arbeit zugänglich machen. Die Inhalte können geteilt, bearbeitet und verändert werden. Sie können weiterentwickelt und mit anderen Inhalten vermischt werden.

Die Inhalte hier werden nach dem Vorbild des reformatorischen und humanistischen [„ad fontes“](https://de.wikipedia.org/wiki/Ad_fontes) geteilt. Die Zugänglichkeit auch kirchlicher Arbeit ermöglicht Transparenz sowie Bildung und Beteiligung für alle Interessierten.

Dieses Vorbild passt zu den Grundsätzen der [Open Educational Resources](https://de.wikipedia.org/wiki/Open_Educational_Resources) und der [Freie-Software-Bewegung](https://de.wikipedia.org/wiki/Freie_Software). Es entspricht für den Bereich der öffentlich-rechtlichen Körperschaften der Forderung [„Public Money? Public code!“](https://luki.org/mitmachen/public-money-public-code/)

Hintergründe über die Freie-Software-Bewegung, ihre Arbeit mit Versions-Verwaltungssystemen und Zusammenhänge und Unterschiede zu (in diesem Fall) protestantischer Theologie finden sich [in diesem englischsprachigen Aufsatz von Benedikt Friedrich](https://cursor.pubpub.org/pub/friedrichflosstheology).

Schwesterprojekte von OpenChurch.Wiki sind die [Offene Bibel](https://offene-bibel.de/) und der [LUKi e.V.](https://luki.org/).


## Was ist das hier?

Das hier ist ein Wiki. Ok, es ist die Nerdvariante eines Wikis, nämlich eine Webseite, die auf einer Dokumenten-[Versionsverwaltung](https://de.wikipedia.org/wiki/Versionsverwaltung) mit der Software [Git](https://de.wikipedia.org/wiki/Git) basiert. Über [Markdown](https://de.wikipedia.org/wiki/Markdown)-Dateien können leicht formatierte Texte erstellt werden, die auch untereinander verlinkt werden können.

Diesen Zugang haben wir gewählt, weil er zu unserer aktuellen Arbeitsweise passt und sehr leicht umzusetzen war (die Plattform hierfür wird freundlicherweise vom [Codeberg e.V.](https://codeberg.org/Codeberg/org/src/branch/main/Imprint.md) zur Verfügung gestellt). Gerne helfen wir dabei, mit der entsprechenden Arbeitsweise zurecht zu kommen.


## Wie funktioniert das?

In einem [Repository](https://de.wikipedia.org/wiki/Repository) (Dateiverwaltung) werden Inhalte in Form von Textdateien verwaltet (und ggf. durch andere Datenformate ergänzt).

Diese Texte sind am zugänglichsten, wenn sie in textbasierten Dateiformaten gespeichert werden. Dann lässt sich auch die Entwicklung (in der Versionsverwaltung) am besten nachverfolgen. Vorzugsweise erfolgt die Speicherung als [Markdown](https://de.wikipedia.org/wiki/Markdown)-Datei (.md), weil diese Dateien für die Webseite direkt in HTML-Seiten umsetzbar sind. Auch lassen sie sich auf Codeberg auch im Webbrowser schnell und einfach bearbeiten (s. den "Bearbeiten"-Link immer oben auf der Seite). Aber auch andere Textformate wie .tex, .txt und .opml sind möglich.

Wenn du Zugriff auf das Repository hast (bitte bei [Letterus](https://codeberg.org/Letterus) anfragen), kannst du im Browser neue Textdateien erstellen (Klick auf "Neue Datei"). Sobald du als Dateiendung ".md" angibst, bekommst du einen einfachen und funktionellen Editor, um die Markdown-Datei zu formatieren. Noch nie gemacht? Probier es aus. Es ist einfach, du lernst es schneller als du denkst. Erste Hilfestellungen zur Formatierung gibt auf [dieser Webseite](https://markdown.de/).

Wenn du bereits Kenntnisse im Umgang mit git hast, kannst du das Repository natürlich auch clonen und die Dateien auf deiner Festplatte bearbeiten. Eine weiterführende Anleitung zum Bearbeiten dieser Webseite findest du auf [Codeberg](https://codeberg.org/OpenChurch/Wiki/src/branch/main/README.md).

## Unter welchen Bedingungen kann ich die Inhalte nutzen?

Die Autor:innen stellen ihre Inhalte unter den Bedingungen der [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) zur Verfügung.

Bitte habe Verständnis, dass wir aus Gründen der [Digitalen Nachhaltigkeit](https://digitale-nachhaltigkeit.net/) keine grundsätzlich andere Lizenz (sondern höchstens kompatible Lizenzen) verwenden möchten.

Die Autor:innen sind aber damit einverstanden, wenn die Inhalte im Rahmen der kirchlichen Arbeit, insbesondere unter den Bedingungen der mündlichen Rede und auch in Aufnahmen von mündlicher Rede (Predigt-Podcast, Gottesdienstübertragung etc.) ohne Angabe der Quelle und zur Not auch unter anderen Bedingungen als denen der Ausgangslizenz verwendet werden.

Bei schriftlichen Veröffentlichungen gelten die Lizenz-Bedingungen aber in jedem Fall ohne Ausnahme.

## Unter welchen Bedingungen kann ich etwas beitragen?

- Du kannst alles veröffentlichen, was du für geeignet hältst. Verstehe „veröffentlichen“ bitte nicht im Sinne einer klassischen Redaktionstätigkeit. Auch „work in progress“, also Werke, die unvollständig und unfertig sind, sollten gesichert werden und können für andere interessant sein. Möglicherweise werden sie gemeinsam mit anderen korrigiert, weiterentwickelt und fertig gestellt.
- Du kannst also auch Werke und Texte von anderen korrigieren, ergänzen usw. usf. Wenn du einen Text oder ein Werk konzeptionell grundsätzlich ändern willst, benutze einen Fork und erstelle einen Pull Request ([de](https://www.atlassian.com/de/git/tutorials/making-a-pull-request)|[en](https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/)), damit die Änderungen mit dem Autor/der Autorin diskutiert werden können.
- Grundsätzlich veröffentlichst du alle Inhalte unter der [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) (bezüglich der Ausnahmen s. den Abschnitt zuvor).
- Du darfst nur Inhalte veröffentlichen, die du selbst erstellt hast oder bei denen du sicher bist, dass sie im Einklang mit geltenden Urheberrechten und der [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) hier veröffentlicht werden können.
- Bei fremden Quellen ohne erkennbare Quellenangabe oder Verwendungserlaubnis sicherst du zu, den Urheber/die Urheberin und die damit verbundenen Rechte nach bestem Wissen und Gewissen ausfindig gemacht zu haben und die Erlaubnis zur Nutzung der Inhalte soweit wie möglich abgeklärt zu haben.
- Fremde Quellen, die hier nicht veröffentlicht werden können, dürfen gerne in Form von [Bibliographien](https://de.wikipedia.org/wiki/Bibliografie) oder Linksammlungen (also als Verweise auf die geschützte Werke anderer) zusammengestellt werden. Diese Sammlungen können von großem Wert für andere sein.
- Du übernimmst die Verantwortung für die Inhalte, die du beiträgst. Unter keinen Umständen veröffentlichst du Inhalte, die im Widerspruch zu geltendem Recht der Bundesrepublik Deutschland stehen.

## Wie arbeite ich mit den Inhalten auf meinem Computer?

Schau in die [Anleitung](https://codeberg.org/OpenChurch/Wiki/src/branch/main/README.md).

Im Repository [„Toolchain“](https://codeberg.org/OpenChurch/Toolchain) sammeln und dokumentieren wir außerdem Möglichkeiten, wie du die Inhalte und deren Bearbeitung in den Workflow an deinem Arbeitsplatz integrieren kannst.
