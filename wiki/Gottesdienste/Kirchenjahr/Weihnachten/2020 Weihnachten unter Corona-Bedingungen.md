2020 Weihnachten unter Corona-Bedingungen
========================================

## Krippenspiele

- https://www.gerhardbeck.de/krippenspiel-2020-geweckt-werden/


## Ideen und Materialien

### Pad zum Mitschreiben

https://pad.luki.org/p/Weihnachten_2020_1


### Links

- https://www.mi-di.de/materialien/anders-weihnachten
- https://www.pastorale-innovationen.de/weihnachten-2020/
- https://www.michaeliskloster.de/in-zeiten-von-corona/Advent-und-Weihnachten/03-19-texte-fuer-karten-zum-verteilen
- https://gottesdienstkultur-nordkirche.de/liturgien-kategorie/advent-und-weihnachten-2020/
- https://www.elk-wue.de/service/weihnachten-in-corona-zeiten
- https://corona.bayern-evangelisch.de/downloads/Anlage%202c%20Ideen%20f%C3%BCr%20Weihnachten.pdf


## Gemeindebrief / Druckerzeugnisse

- [Sketch Story Heilig Abend 2020](2020%20Sketch%20Story%20Heilig%20Abend/README.md)
- https://www.michaeliskloster.de/in-zeiten-von-corona/Advent-und-Weihnachten/03-19-texte-fuer-karten-zum-verteilen

