README
=========================

## Download der Druckgraphiken

Konzept steht unten. Direktlinks zu den druckbaren Dateien (idealerweise gedruckt auf zwei A5-Seiten:

- [Seite 1](Weihnachten_at_Home_1_mit_QR.jpg)
- [Seite 2](Weihnachten_at_Home_2_mit_QR.jpg)


## Konzept Sketch-Story "Weihnachten @ home" für den Gemeindebrief

Darstellung als Sketch-Story: Weg durch die Liturgie mit Pfeilen und QR-Codes auf einer A4-Seite oder DIN-A5-Doppelseite.

### Weihnachten @ home
#### mit Kindern und der ganzen Familie

- Eine Kerze anzünden (oder vier) [Kerze]
- Gebet [in Tannenbaumform]:  
```
                              Gott, 
                     wir sind hier. Du auch.
                  Dieses Jahr ist alles anders.
          Wir feiern die Christvesper in unserem Zuhause.
       Jesus Christus, du bist Mensch geworden in einem Stall.
    Dafür warst du dir nicht zu schade. Du kommst heute auch zu uns
                  und machst unsere Nacht hell.
                       Dafür danken wir dir.
                              Amen.
```
- Lied Stern über Bethlehem - QR-Code zu: https://www.youtube.com/watch?v=M8fodlFWpo0#t=14s [Gesangbuch mit Noten]
- Die Weihnachtsgeschichte nach Lukas 2 - https://www.youtube.com/watch?v=UGWxoImKzjs [Bibel/Wort Gottes in der Krippe]
- Lied Ihr Kinderlein kommet - QR-Code zu: https://www.youtube.com/watch?v=OmbppQcrfyk [Gesangbuch mit Noten]
- Fürbitten [Engel drumherum]:
```
    Jesus Christus, du kamst in diese Welt - schenke ihr Frieden,
    du kommst in unsere Gemeinschaft, in unserer Stadt, unserem Land - gib uns Zusammenhalt,
    du kommst in unsere Finsternis - mache sie hell,
    du kommst in unser Herz - halte uns bei dir.
```
- Vaterunser - QR-Code zu https://www.youtube.com/watch?v=e2JmjswE--c [Betende Hände]
- Lied Oh du fröhliche - QR-Code zu: https://www.youtube.com/watch?v=1dfhNZpSt-0 [Engelchor außenrum]
- Segen [Segenshände]:
```
   Gott, die Weihnachtsfreude begleitet uns,
   wenn wir zusammen feiern.
   Der Friede Jesu Christi ist unter uns,
   der Heilige Geist lässt uns deine Nähe spüren.
   So segne uns der dreieinige Gott,
   der Vater, der Sohn und der Heilige Geist.
   Amen.
```

(c) Janna und Johannes Brakensiek – CC-BY-SA 4.0