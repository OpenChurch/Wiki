README
=========================

## Bilder

### Für Kinder

![Sketch Gottesdienst Weihnachten für Kinder](2021_Weihnachten_fuer_Kinder_QR.png "Weihnachten für Kinder")

### Für Erwachsene

![Sketch Gottesdienst Weihnachten für Erwachsene Seite 1](2021_Weihnachten_fuer_Erwachsene_1_QR.png "Weihnachten für Erwachsene")

![Sketch Gottesdienst Weihnachten für Erwachsene Seite 2](2021_Weihnachten_fuer_Erwachsene_2_QR.png "Weihnachten für Erwachsene")

## Download der Druckgraphiken

### PDF Druckvorlagen A5 mit jeweils 3mm Beschnittzugabe:

- [Weihnachten für Kinder (A5 Hochformat, einseitig)](Weihnachten_fuer_Kinder_Druckvorlage_mit_Beschnittzugabe.pdf)
- [Weihnachten für Erwachsene (A5 Querformat, zweiseitig)](Weihnachten_fuer_Erwachsene_Druckvorlage_mit_Beschnittzugabe.pdf)


### Scribus Layout-Dateien

- [Weihnachten für Kinder (A5 Hochformat, einseitig)](Scribus_Weihnachten_fuer_Kinder_Druckvorlage.sla)
- [Weihnachten für Erwachsene (A5 Querformat, zweiseitig](Scribus_Weihnachten_fuer_Erwachsene_Druckvorlage.sla)


### Graphiken

siehe die Dateiliste im Verzeichnis dieser Datei


## Konzept: Für Erwachsene

   * Lied zum Anfang:
       * Tochter Zion [https://www.youtube.com/watch?v=8gkKl9zPDZA](https://www.youtube.com/watch?v=8gkKl9zPDZA)


   * Gebet
       * Guter Gott,
       * du kommst.
       * Uns darüber einfach nur zu freuen
       * fällt uns auch dieses Weihnachten wieder schwer:
       * Schon wieder sorgen wir uns um unsere Lieben.
       * Wir sehnen uns nach Normalität und Gemeinschaft.
       * Die Freude über deinen Einzug soll in unser Leben leuchten.
       * Schenke uns das.
       * Amen.


   * als Evangelium
       * Kommet ihr Hirten: [https://www.youtube.com/watch?v=LjpVUBgk9O4](https://www.youtube.com/watch?v=LjpVUBgk9O4) (Maite Kelly)


   * Impuls: Bei mir einziehen?! - zu Sach 2,14 als Podcast/Vlog eingesprochen (TODO: muss noch produziert werden - vorläufig ist der Text [unter der URL abgelegt](https://podcast.walsum-vierlinden.de/2021/12/03/bei-mir-wohnen/), damit der QR-Code funktioniert)


   * Lied: Ich steh an deiner Krippe hier: [https://www.youtube.com/watch?v=WEJB58zvHqs](https://www.youtube.com/watch?v=WEJB58zvHqs) (Thomanerchor in Corona-Version)


   * Abschlussgebet
       * Gott,
       * wenn du bei uns einziehst,
       * baue unsere Herzen um.
       * Mach sie zu Wohnungen/Palästen deiner Liebe.
       * Lass sie deine Krippe sein.
       * Schenke uns deinen Weihnachtsfrieden.
       * Amen.


   * Vaterunser: [https://www.youtube.com/watch?v=e2JmjswE--c](https://www.youtube.com/watch?v=e2JmjswE--c)


   * Lied zum Schluss: Mary did you know?  [https://www.youtube.com/watch?v=ifCWN5pJGIE](https://www.youtube.com/watch?v=ifCWN5pJGIE)


   * Segen
       * Wir wissen: Der Friede Gottes zieht bei uns ein. Er bleibt bei uns.
       * So segnet uns Gott, der mächtig ist und gut. Amen.


## Impuls zum Monatsspruch für den Monat Dezember

Freue dich und sei fröhlich, du Tochter Zion! Denn siehe, ich komme und will bei dir wohnen, spricht der HERR. *Sach 2,14 (L)*

## Gedanken

Das soll eine Freudenbotschaft sein?

„Ich komme und will bei dir wohnen.“

Das muss man sich mal auf der Zunge zergehen lassen. Selbst wenn meine besten Freunde mir das sagen würden: „Ich komme und will bei dir wohnen“ – es wäre keine Freudenbotschaft. Auf einen Besuch und einen Kaffee, klar, gerne. Aber bei mir wohnen? Da sind die meisten ja selbst mit der Partnerin und dem Partner ein wenig zurückhaltend bis man sich auf solche Späße einlässt.

Warum nun soll es eine Freudenbotschaft sein, wenn Gott bei uns – und im Kontext des Propheten Sacharja natürlich erst einmal im Tempel in Jerusalem – wohnen möchte?

Nun, zum einen scheint er ein wirklich angenehmer WG-Gefährte zu sein. Jesus hat das im Neuen Testament ganz ähnlich vorgemacht. Er ist bei den Menschen nicht direkt eingezogen, aber er hat sich bei ihnen zum Essen eingeladen. Zum Beispiel bei dem verbiesterten Zöllner Zachäus (Lk. 19,5). Der freut sich und ändert daraufhin seine Gewohnheiten und sein Leben und wird zum großherzigen Gönner.

Ich schließe daraus, dass Gott, wenn er bei uns einzieht, natürlich in unsere Herzen einzieht. Und dass er diese Wohnungen einfach ordentlich umbaut, wenn er denn kommt. Vorher war unser Herz ein 1-Zimmer-Appartment, wo gerade so Platz war für unser Leben, Essen und die Sorgen des Alltags. Daraus macht Gott eine Luxus-Villa mit mehreren Schlafgemächern für ausreichend Gäste und Wohnzimmern zum Essen, Feiern, Spielen und Musizieren.

Ein Palast des Friedens und der Freude. Unsere kleinen Herzen werden groß und geräumig.

Dass Gott in der Person von Jesus zu Weihnachten in so einer kleinen Krippe auftaucht, macht diesen Zusammenhang besonders deutlich: Er ist eigentlich ein kleine-Herzen-zu-goße-Villen-Umbauer. Ein Futtertrog war ihm nicht zu klein. Unsere Herzen sind es auch nicht.

Na dann, lassen wir ihn einziehen!


## Konzept: Für Kinder

   * Lied zum Anfang:
       * [https://www.youtube.com/watch?v=hQE7M3CR3LU](https://www.youtube.com/watch?v=hQE7M3CR3LU) Ihr Kinderlein kommet


   * Gebet:
       * Lieber Gott,
       * endlich hat das Warten ein Ende: Du bist geboren - es ist Weihnachten!
       * Danke, dass wir uns darüber so sehr freuen dürfen!
       * Amen


   * Weihnachtsgeschichte für Kinder: [https://www.youtube.com/watch?v=zvhPNsizb4c](https://www.youtube.com/watch?v=zvhPNsizb4c)


   * Lied: Hört der Engel helle Lieder [https://www.youtube.com/watch?v=HbK1TipyTZI](https://www.youtube.com/watch?v=HbK1TipyTZI)


   * Segen
       * Gott segne dich mit Weihnachtsfreude, die über das Weihnachtsfest hinaus anhält.
       * Gott segne dich mit Freude, die ansteckt.
       * Gott lasse dich ein Segen sein.
       * Amen


(c) Janna und Johannes Brakensiek – CC-BY 4.0
