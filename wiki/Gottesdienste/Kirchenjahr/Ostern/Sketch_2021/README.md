README
=======

In diesem Verzeichnis befinden sich Druckgraphiken für Gottesdienste "@ home" zu Karfreitag und zu Ostern.

Die Dateien werden idealerweise jeweils auf zwei A5-Seiten gedruckt.


## Druckgraphiken

### Karfreitag:

<img src="Karfreitag_at_home_I_QR.png" alt="Graphik für Karfreitag I" width="50%" /> 

Download: [Seite 1](Karfreitag_at_home_I_QR.png)

<img src="Karfreitag_at_home_II_QR.png" alt="Graphik für Karfreitag I" width="50%" /> 

Download: [Seite 2](Karfreitag_at_home_II_QR.png)


### Ostern:

<img src="Ostern_at_home_I_QR.png" alt="Graphik für Ostern I" width="50%" /> 

Download: [Seite 1](Ostern_at_home_I_QR.png)

<img src="Ostern_at_home_II_QR.png" alt="Graphik für Ostern II" width="50%" /> 

Download: [Seite 2](Ostern_at_home_II_QR.png)


(c) Janna und Johannes Brakensiek –  [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/3.0/de/)
