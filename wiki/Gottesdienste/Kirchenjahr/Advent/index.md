# Sketch Adventskalender zum Selberbefüllen

In diesem Verzeichnis findet sich ein druckbarer Adventskalender zum Selberausfüllen/Selberbefüllen.

Gedacht ist er für die vier Sonntage/Wochen des Advents. Für jede Woche kann eine A5-Karte mit Vorder- und Rückseite gedruckt werden.

- für den 1. Advent:
    - [Vorderseite](01_Adventskalender_01.png)
    - [Rückseite](01_Adventskalender_02.png)
- für den 2. Advent:
    - [Vorderseite](02_Adventskalender_01.png)
    - [Rückseite](02_Adventskalender_02.png)
- für den 3. Advent:
    - [Vorderseite](03_Adventskalender_01.png)
    - [Rückseite](03_Adventskalender_02.png)
- für den 4. Advent:
    - [Vorderseite](04_Adventskalender_01.png)
    - [Rückseite](04_Adventskalender_02.png)

Copyright Janna Brakensiek, freigegeben unter [Creative Commons Lizenz CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de).