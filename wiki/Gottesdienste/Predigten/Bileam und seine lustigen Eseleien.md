---
title: Bileam und seine lustigen Eseleien
summary: Die Erzählung über den heidnischen Priester Bileam ist wie ein antikes Theaterstück, das uns einen Spiegel vorhält
authors:
    - Johannes Brakensiek
tags:
    - Bileam
    -  4 Mose 22
    -  Engel
    -  Tolkien
    -  Landnahme
    -  Israel
    -  Reichtum
    -  Esel
    -  Eselin
date: 2024-09-29
---

# Bileam und seine lustigen Eseleien

Liebe Gemeinde,

ich muss es Ihnen sagen: Ich  hasse Engel! Diese kitschigen, blondgelockten Gefährten, die besonders zur Weihnachtszeit viele Räume in unserem Kulturkreis verzieren. Sie sind dabei so etwas wie die bessere Version des Gartenzwerg: Praktische Wesen, lustig anzusehen, aber nicht störend. Sie sind das Popsymbol des Christentums, so hold und rein, dass niemand was dagegen haben kann, schließlich beschützen und behüten sie einen, auch wenn einem Gott in seinem Leben sonst eigentlich fürchterlich egal ist.

Sprich: Ich finde Engel ganz fürchterlich. Zumindest die mit diesem popkulturellen Image.

Mein Bild hat sich etwas gewandelt, als ich die Bücher und die Filme zu J.R.R. Tolkiens "Herr der Ringe" las und sah. Da gibt es das Volk der Elben: Geheimnisvolle Völker, unsterblich, tapfere Krieger, die sich mit geschickter Kriegskunst für das Gute in der Welt einsetzen, dafür kämpfen und sterben – und die die Ich-Süchtigen Wesen verächtlich links liegen lassen und ihnen den Rücken kehren. Unzweifelhaft war Tolkien bei diesen Figuren von biblischen Engeln inspiriert und ich finde, das passt doch schon deutlich besser.

Auch in unserem Predigttext kommt so ein Engel vor, wir haben es gerade gehört. Die Geschichte selbst ist eigentlich noch viel länger, aber noch mehr wollte ich Michael nun wirklich nicht vorlesen lassen.

Es ist eine Erzählung, die sich eigentlich hervorragend für ein Theaterstück eignet. Ein Theaterstück, bei dem es nicht so sehr um Schutz und Kriegskunst, sondern um ganz menschliche, alltägliche Themen geht: Darum, wer mächtig ist und etwas zu sagen hat, auch im religiösen Bereich – und wie diese Menschen  über Eigennutz und eigene Interessen stolpern. Ein Theaterstück, dass sie entlarvt mit ihren niederen Beweggründen, ihren Tricks, aber auch ihrer Ohnmacht gegenüber Gott und seiner Gerechtigkeit. Es ist ein Stück voller jüdischem Humor, so scheint mir.

Hintergrund ist die sogenannte Landnahmeerzählung, also die Erzählung, die vier der Bücher Mose und das Buch Josua prägt: Wie das Volk Israel aus Ägypten auszieht und 40 Jahre lang Wohnung sucht im verheißenen Land. In unserem Abschnitt in 4. Mose 22 (es sind eigentlich mehrere Kapitel) sind sie gerade im Jordantal unterwegs, irgendwo in der Gegend um Jericho, im Land der Moabiter. Und natürlich, wenn da so ein großes Volk durch die Landen zieht, haben die Moabieter Angst vor dem Volk und dafür, dass diese Menschen wie Heuschrecken einfallen und ihnen buchstäblich die Harre vom Kopf fressen.

Balak war damals König der Moabiter. Einer seiner engsten Berater hieß Bileam, er war zugleich Priester und hatte offenbar auch einen direkten Draht zu Gott, zu dem Gott der Israeliten.

Ja, und damit kommen wir zu der ersten humoristischen Spitze unseres Stückes: Bileam kann Gott in der Nacht (vermutlich im Traum) nicht nur hören, er kann mit ihm sprechen wie mit einem himmlischen Telefon. Aber er schafft es trotzdem nicht, auf ihn zu hören.

Denn als nun Balak eine Gesandtschaft schickt, Bileam reichen Lohn verspricht, dafür, diese Eindringlinge zu verfluchen, da hört Bileam in der Nacht von Gott, der sagt: "Du wirst nicht mit den Männern gehen! Und du darfst das Volk nicht verfluchen, denn ich habe es gesegnet!"

Und beim ersten Mal gehorcht Bileam Gott noch und gibt das an die Gesandtschaft des Königs weiter. Aber als Balak dann noch mehr Boten losschickt, es heißt "Diesmal waren es noch mehr Männer, und sie hatten noch größeres Ansehen.", da hält Bileam ihnen entgegen, dass der König ihm zwar alles Silber und Gold geben könnte, aber er doch auf Gottes Wort hören muss.

Und dann, oh Wunder, redet er in der Nacht noch einmal mit Gott und dieses Mal sagt Gott, dass er doch bitte gehen soll, er aber bitte nur das sagen soll, was Gott von ihm möchte. – Ja, das ist der Punkt wo ich mir vorstelle, wie die antiken Zuschauer unseres Theaterstücks schon deutlich schmunzeln und einer dem anderen den Ellenbogen in die Seite stößt in freudiger Erwartung dessen, was da jetzt gleich kommt. Denn es ist ja erstaunlich, dass Gott seine Meinung ändert, angesichts von Bileams Erwartung von Silber und Gold.

Es kommt also wie die grinsenden Zuschauerinnen es erwarten, Bileam macht sich mit seiner Eselin und zwei Knechten auf den Weg und etwas geht schief.

Gott ist natürlich zornig über Bileams Verhalten und sendet seinen Engel, der sich mit gezogenem Schwert in seinen Weg stellt. Ein Krieger Gottes, der den Priester Gottes, Bileam, wieder auf den rechten Weg weisen soll. Aber, der Mensch, der nachts mit Gott reden konnte, sieht diese mächtige Erscheinung Gottes jetzt nicht mehr. Silber und Gold scheinen ihm so den Blick vernebelt zu haben, dass er nichts mehr erkennt.

Ganz anders seine Eselin, die dem Engel ausweicht, sich von Bileam schlagen lässt, ihn am Ende auf den Boden wirft und gar nicht mehr macht und Bileam schlägt sie weiter.

Ich stelle mir vor, unsere antiken Zuschauerinnen schlagen sich bei diesen Szenen jetzt schon vor Lachen auf die Schenkel, denn das Bild wird natürlich vollends klar als die Eselin jetzt auch noch anfängt zu sprechen und Worte der Gerechtigkeit spricht: "Was habe ich dir getan, dass du mich jetzt zum dritten Mal schlägst? Bin ich nicht deine Eselin, auf der du schon dein Leben lang reitest? Habe ich mich dir gegenüber jemals so verhalten?"

Damit wird nun vollends deutlich, wer in unserem Stück eigentlich der Esel ist und wer die Klugheit hat, Gottes Boten zu erkennen und gerecht zu handeln.

Und nachdem das Lachen der Zuschauerinnen verklungen ist, wendet sich natürlich das Blatt. Bileam wird, bekehrt durch seine Eselin, wieder zum gottesfürchtigen Priester. Und als er bei König Balak ankommt und er sich über die Verzögerung beschwert, sagt er deutlich, dass er nur tun kann, was Gott von ihm erwartet. Die Geschichte ist nicht zu Ende, denn nun ist Bileam dran, Balak mit netten Tricks an der Nase herumzuführen. Ihn glauben zu machen, dass das mit der Verfluchung Israels schon klappen könnte, wenn er nur an der richtigen Stelle Altäre baut und genügend Stiere und Widder opfert. Doch das ist eine andere Geschichte. Sozusagen die Fortsetzung nach der Pause. Nur so viel sei verraten: Es klappt nicht. Statt Israel zu verfluchen, segnet Bileam Israel am Ende vier Mal. Balak ist sauer, Balak und Bileam verlassen die Bühne.

Was ist nun mit uns, die wir dieses Theaterstück vor unserem inneren Auge ein wenig miterlebt haben?

Wir sollen uns natürlich mit Bileam identifizieren. Mit dem mächtigen Mann Gottes, von ihm gesandt. Der dann aber doch so ist wie wir: Wir wissen vielleicht ganz gut, was richtig ist, was Gott von uns erwartet. Aber dann gibt es doch sehr menschliche Gründe, warum wir den Willen Gottes ein wenig zurechtbiegen und meinen, es könnte doch auch so oder so gehen. Ich denke, da fallen uns aus unserem Leben, aber auch aus den Geschehnissen der aktuellen Tagespolitik viele Beispiel ein.

Aber wir brauchen darüber nicht zu verzweifeln. Wenn wir darauf schauen, können wir die Zuschauerinnen unseres Theaterstückes über uns selbst lachen und feststellen: Am Ende geschieht das, was Gott möchte. Und es steht uns frei, so zu sein wie die kluge Eselin Bileams, und das Richtige zu sehen und mit zu tun. Auch wenn es sicher nicht immer einfach ist.

Nach den vielen Worten noch zwei letzte: Hat diese alte Landnahmeerzählung der 5 Bücher Mose und Josua etwas mit dem zu tun, was heute in Israel geschieht? Würde Bileam heute Israel segnen? Ich glaube nicht. Es sind ganz unterschiedliche Verhältnisse. Die Landnahmeerzählung ist eine Geschichte darüber, wie ein kleines und unbedeutendes Volk Gott, Heimat und Identität findet. Wie es gegen übermächtige Gegner kämpft und sich auch mit dem eigenen Unwillen Gottes Willen folgen, auseinandersetzt. Und das vor allem literarisch. Wie viel davon historisch passiert ist und ob es jemals überhaupt so geschehen ist, wie es da steht - das kann heute niemand mehr wissen.

In jedem Fall ist die heutige Situation eine völlig andere: Wir haben eine hochentwickelte Demokratie, einen Nationalstaat mit den modernsten Technologien der Welt, gestärkt von der Supermacht der Welt, der sich gegen zT. sehr schwache und kleine Nachbarn wendet. Was die Weltpolitik angeht: Da kann ich von dem Volk Israel damals Nichts auf die Nation von heute übertragen. Zu diesem Vergleich weiß ich nichts Gutes zu sagen.

Aber, was ich sehr wohl weiß, ist, dass es heute Menschen gibt, denen die Erzählungen der 5 Bücher Mose und Josua wichtig sind. Dass es heute auch in unserem Land Jüdinnen und Juden gibt, die diese Geschichten hören und lesen. Die darüber lachen, sich selbst darin erkennen und sich fragen, was heute richtig und wichtig ist. Auch in Palästina. Und zwar von Gott her. Und die deswegen, wegen ihres Glaubens wieder ein sehr schweres Leben in unserem Land haben. Und denen gehört Gottes Segen und unsere Unterstützung. Dass wir mit ihnen lachen und uns über biblische Weisheiten freuen und wir uns erkennen und unsere Eseleien. Und wenn es dazu dann eben so Engel braucht. Amen.