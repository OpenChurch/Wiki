---
title: Seefahrtromantik - die Sturmstillung
summary: Stürme werden zu einem spannenden Abenteuer, wenn Jesus an Bord ist. Machen wir es uns gemütlich!
authors:
    - Johannes Brakensiek
tags:
    - Mk 4,35-41
    - Sturm
    - See Genezareth
    - Wunder
    - Santiano
    - Seefahrt
date: 2025-02-09
---

# Seefahrtromantik - die Sturmstillung

Liebe Gemeinde,

willkommen in der Geschichtenstunde! Wir sind in diesem Jahr mitten in der Predigtreihe I, die sozusagen das best of aller Geschichten für den Kindergottesdienst enthält. Das Best of der Geschichten der Geschichten auch für Erwachsene, die uns die Evangelien erzählen. Und die nicht ohne Grund Weltliteratur sind. Denn wer liebt sie nicht: Gute, packende Geschichten? Heute ist sogar Seefahrerromantik dabei.

Die Seefahrt und die See sind seit Jahrhunderten Gegenstand phantastischer Geschichten. Wer hier in Vierlinden unterwegs ist, der wird auf der Römerstraße Richtung Kraftwerk, kurz vor der Eisenbahnbrücke schon links versteckt das Haus der Marinejugend entdeckt haben. Es heißt:  Peter-Gording-Haus. Peter Gording war selbst Seemann und ein Schreiber phantastischen Seemanngarns für Kinder und Jugendliche.

Woher kommt diese Faszination, die über den Rhein bis nach Walsum kommt? Nun, ich denke, beide Gründe für die Faszination kommen in der Geschichte von der Sturmstillung vor. Einmal die Möglichkeit auf einem Boot unüberwindbare Grenzen überwinden zu können. Ferne - oder nicht so ferne Länder ansteuern zu können und dabei trocken und sicher zu bleiben. Ja, das ist das, was Jesus vormacht. Er zeigt den Jüngern, dass ein gutes Boot ein hervorragende Ort ist, um es sich gemütlich zu machen, um sich in der Weite von Wind und Willen sicher und geborgen zu fühlen. Er schläft auf einem Kopfkissen. So, als würde er gerade Sommerurlaub machen.

Ein Gefühl, dass sicher viele Seglerinnen und Seefahrer nachvollziehen können: Denn die See ist nicht nur weit und gefährlich, sie ist auch der Schoß allen Lebens. Aus der Biologie wissen wir, dass am Ende alles irdische Leben aus dem Wasser kommt. Ja, wir als Menschen wachsen selbst in Wasser auf. In Fruchtwasser. Deswegen ist es für menschliche Babies auch vollkommen unschädlich in Wasser geboren zu werden, daher kommen wir.

Und auch für uns, die wir uns heute lange nicht mehr an diese Verbindung zum Wasser erinnern können, kommen vermutlich 80% des Sauerstoffs, den wir atmen - nicht aus den Wäldern, nicht aus den Bergen, sondern aus dem Ozean.

Viele Gründe also, Wasser als Heimat, als Zuhause, als Wohlfühlort zu verstehen. Gerade in einem guten Boot und mit einem bequemen Kopfkissen. Jesus macht es vor.

Doch dann hat die See natürlich auch das andere. Die Gefahr, die Tücke. Unberechenbare Wetter, Stürme, gefährliches Eis, unberechenbare Strömungen.

Nun muss man bei aller Seefahrerromantik zugeben, der See Genezareth, auf dem Jesus und seine Jünger unterwegs waren, ist nicht die hohe See. Aber mit einer Länge von heute 21km und einer Breite von 13km ist er alles andere als ein kleiner Teich. Im Arabischen wurde er auch "Meer von Tiberias“ oder im Mittelalter Meer von Minya" nach den naheliegenden Orten benannt.

Und, da er sehr tief liegt, 212m unter dem Meeresspiegel, ist er dafür bekannt, dass die Luft an der Wasseroberfläche deutlich wärmer ist als die kalte, fallende Luft von den umliegenden Bergen. Das kann zu starken Stürmen, angeblich bis zu Windstärke 7 führen.

In solch einen Sturm geraten also die Freunde von Jesus und das ist dann selbst für die erfahrenen Fischer, die ja ihr Leben auf diesem See verbringen, zu viel. In Todesangst wenden sie sich an Jesus.

Er hört sie.  
Er spricht zu Wind und Wellen.  
Es wird still.

Und Jesus fragt sie: »Warum habt ihr solche Angst? Habt ihr immer noch keinen Glauben?«

Das ist eine Frage an uns. Die wir als Jüngerinnen und Jünger Jesu wieder einmal in stürmischen Zeiten leben.

Mich hat da ein Votum des Geschäftsführers unserer Kinderwelt Timon Mecks auf der letzten Synode unseres Kirchenkreises beeindruckt. Er sagte vom Sinn her: "Als Menschen und als Kirche haben wir immer wieder gehofft, dass es doch wieder besser wird und die Zeiten wieder ruhiger werden. Und oft ist das eingetreten. Aber das steht derzeit nicht an. Wir müssen uns darauf vorbereiten, dass die Stürme bleiben, dass die Zeiten herausfordernd bleiben. Wir brauchen eine andere Einstellung."

Ich denke, da ist was dran. Und vielleicht brauchen wir die Einstellung von Seefahrerinnen und Seefahrern, die in stürmischen Zeiten unterwegs sind, aber sich sicher und geborgen fühlen können, weil sie auf ihr Boot und dann vor allem auf den, der mit ihnen im Boot ist, vertrauen können.

Denn das ist die letzte Frage der Jünger in dieser Erzählung, die so kling als wären sie begriffsstutzig, die aber natürlich eine rhetorische Frage an uns als Leserinnen und Leser ist:

Sie fragten sich: »Wer ist er eigentlich?  
Sogar der Wind und die Wellen gehorchen ihm!«

Wer ist der, dem Wind und Wellen gehorchen? Die Antwort erschließt sich vom Ende der Geschichte Jesu her. Gott selbst. In Markus 15 sagt der römische Hauptmann vor dem Kreuz: »Dieser Mensch war wirklich Gottes Sohn!«

Es ist eine vermeintliche einfache Antwort. Aber eine Antwort, die auch für die Jünger schwer zu begreifen ist. Aber wer sie begreift, der oder die segelt sicher im Sturm: Gott selbst ist mit uns im Boot.

Und so ist es in diesen schwierigen Zeiten nicht unsere Aufgabe, uns in sichere Häuser zurückziehen, so verlockend das sein mag. Sondern rauszugehen und rauszufahren, die schweren Zeiten zu meistern. Denn wir haben die Aufgabe, nicht für uns selbst zu bleiben. Sondern Salz und Licht zu sein, in den Worten der Bibel. In unseren Worten: Für Menschlichkeit und Menschenwürde eintreten. Uns dafür einsetzen, dass Gottes Wille auf dieser Welt geschieht. Das Banner "Kreuz gegen Rechts" an unserer Fensterscheiben soll nur ein kleines Zeichen dafür sein. Da kann es unruhig werden.

Aber Gott selbst ist mit uns im Boot. Er hat ein gemütliches Kopfkissen mitgebracht und ein sicheres Boot. Fahren wir weiter.

Und so müssten wir dann vielleicht noch einmal etwas Seemannsgarn spinnen und Geschichten mit Seefahrerromantik erzählen.

Ich habe schon länger geplant einmal einen Santiano-Gottesdienst, mit den besten Schlagern der Seefahrtsromantik der Band Santiano zu machen. Ich hab das damals im Vikariat erprobt, das macht großen Spaß. Zu einem Schlagergottesdienst kommt es jetzt heute nicht.

Aber ein Lied von Band Santiano möchte ich dann doch zitieren:

Schau Dich um diese Welt ist unsere  
Lass die Angst und die Sorgen los  
Hier bist du frei (frei), frei (frei)  
Jede Fahrt ist wie ein neuer Tag  

Halt die Hand in die kühlen Fluten  
Und du spürst eine Macht so groß  
Hier sind wir frei (frei), frei (frei)  
Himmelweit in die Unendlichkeit  

Gott muss ein Seemann sein  
Keiner geht verloren  
Keiner geht verloren  
Er läßt die Mannschaft nie allein

Gott muss ein Seemann sein  
Gott muss ein Seemann sein  
Keiner geht verloren  
Keiner geht verloren  

Der Teufel holt uns niemals ein  
Gott muss ein Seemann sein  

Lass uns ziehn mit den ersten Strahlen  
Lass uns ziehn wenn der Westwind braust  
Dann wir sind frei (frei), frei (frei)  
Geradeaus in alle Welt hinaus  
Wo die Wellen nach dem Himmel greifen  
In der Ferne sind wir zu Haus  
Denn wir sind frei (frei), frei (frei)  
Himmelweit in die Unendlichkeit  

Gott muss ein Seemann sein  
Keiner geht verloren  
Keiner geht verloren  
Er läßt die Mannschaft nie allein  
Gott muss ein Seemann sein  
Gott muss ein Seemann sein  

Amen.
