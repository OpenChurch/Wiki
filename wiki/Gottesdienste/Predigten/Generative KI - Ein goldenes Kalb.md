---
title: Generative KI - Ein goldenes Kalb
summary: Predigt im Before-Tatort-Gottesdienst zum Thema "KI-rche"
authors:
    - Johannes Brakensiek
tags:
    - Ethik
    - KI
    - Gottebenbildlichkeit
    - ErstesGebot
    - Exodus 32
date: 2024-11-10
---
 
# Generative KI - Ein goldenes Kalb

Guten Abend zusammen!

Wer von euch kennt die Geschichte [von Mose und dem goldenen Kalb](https://www.die-bibel.de/bibel/BB/EXO.32.1-EXO.32.6)?

Mose ist zu lange auf dem Berg Sinai, um mit Gott zu reden und von ihm die 10 Gebote zu empfangen. Und als er einfach nicht zurückkehrt, da macht Aaron dem Volk einen Gott aus Gold. Das goldene Kalb. Und das Volk opfert diesem Gott, es feiert und begibt sich in anrüchige Orgien.

Es ist vielleicht der Gipfel einer Reihe von Geschichten der Unzufriedenheit des Volkes mit ihrem Gott. Und aus lauter Frust sammelt Aaron das Wertvollste ein, was sie haben: Ihren goldenen Schmuck, um daraus das Kalb zu gießen.

An diese Geschichte musste ich denken als ich Anfang des Jahres Sam Altman, CEO von OpenAI sah. Das sind die Erfinder von ChatGPT. [Vor einer Gruppen von Risikokapitalgeber:innen spricht Sam Altman darüber, wie er mit dieser Technik Geld verdienen möchte](https://www.reddit.com/r/MachineLearning/comments/bsij7c/news_sam_altman_on_openais_business_model/). Er sagt platt und klar, dass er es nicht weiß. Aber er plant eine allgemeine künstliche Intelligenz, also eine Art Denkapparat, die dem von uns Menschen dann wirklich nahe kommen soll. Und wenn er diese Intelligenz gebaut hat, dann möchte er sie fragen, wie man mit ihr Geld verdienen kann. Gelächter im Raum. Aber er meint es ernst.

Er nimmt all ihr Geld, Milliarden von Dollar mit dem Versprechen einer Supertechnologie. Er nimmt all eure, unsere Daten, alles, was er kriegen kann, um diese Form von Software zu trainieren. Denn anders geht es nicht. Nur mit so vielen Daten wie möglich. [Woher die kommen, von wem die kommen,](https://www.zeit.de/digital/2024-05/kuenstliche-intelligenz-meredith-whittaker-fortschritt-ueberwachung) [wem sie gehören](https://www.kulturrat.de/positionen/kuenstliche-intelligenz-und-urheberrecht/): Das ist gleich. Alles ist ihm recht. Für das goldene Kalb einer künstlichen Hyperintelligenz, die klüger sein soll als alle wir Menschen, ja die fast allwissend sein wird, denn sie basiert auf dem Wissen der Welt. Sie ist wie Gott.

Das stammt nicht aus dem Tatort, sondern das ist real.

Wie konnte es dazu kommen? Beim Volk Israel war es – der Erzählung nach – wohl eine lange Zeit der Unzufriedenheit, die lange Wanderung durch die Wüste.

Bei der KI ist schon noch etwas anders. KI an sich ist weder etwas Neues, noch etwas Schlechtes. In den 40er Jahren hatten Wissenschaftler:innen erste Erkenntnisse darüber, wie unser Gehirn funktionieren könnte. [Anfang der 80er Jahre erkannten Physiker (die haben jetzt gerade Nobelpreise bekommen) Ähnlichkeiten zu physikalischen Modellen und konnten mathematische Modelle darüber entwickeln](https://www.heise.de/hintergrund/Physik-Nobelpreis-Wie-viel-Physik-in-neuronalen-Netzen-steckt-10001801.html), wie auch eine Maschine über ein gestuftes System von Wahrscheinlichkeitsrechnungen und mit wiederholten Trainingsläufen so etwas wie ein selbstständiges Erkennen lernen könnte. Wir wissen heute, das hat mit der menschlichen Funktionsweise des Gehirns nicht besonders viel zu tun. Aber es funktioniert. Seit den 90er Jahren werden solche Systeme in der Wissenschaft verwandt, um in großen Mengen von Daten Muster erkennen zu können.

Das ist das, was diese Software eigentlich macht und kann: Aufgrund von Wahrscheinlichkeitsberechnungen Muster erkennen, die dann in Kategorien, Symbole, Begriffe, neue Daten überführt werden. Es ist keine Intelligenz. Da gibt es kein Nachdenken, kein Fühlen, keine Wahrnehmung, sondern das, was die Amerikaner Intelligence nennen. Etwa in ihrer CIA, der Central Intelligence Agency. Es geht darum Dinge aufzuspüren, Muster wiederzuerkennen.

Das ist sehr wertvoll und sinnvoll, das kann man auf ganz viele Bereiche, in denen es um viele Daten geht, anwenden. Wir haben gerade schon von welchen gehört. Und auch wir als Normalmenschen nutzen das schon lange. Wenn ihr ein iPhone besessen habt, diese Geräte können 2017 Gesichter in euren Fotos erkennen. [Die Foto-Software iPhoto auf dem Mac konnte das seit 2009](https://www.macworld.com/article/194779/iphoto_faces.html). Das ging damals noch seeehr langsam.

Aber wenn es nur um diese Formen von maschinellem Lernen, wie man diese Algorithmen besser nennt, gehen würde, dann würde ich hier nicht stehen. Nein, ich stehe hier, weil es eine neue Generation von Lernalgorithmen gibt, die mit gigantischen Mengen von Daten trainiert wurden, mit einem gigantischen Aufwand an Ressourcen (dazu später mehr) und die dadurch tatsächlich gelernt haben unter anderem zu sprechen. So zu sprechen, dass es klingt wie ein echter Mensch. Und das ist so beängstigend, dass Menschen darüber einen Tatort drehen und Theolog:innen öffentlich beginnen darüber zu reden, ob Maschinen Personen werden können. Ich bin heute hier wegen ChatGPT.

Ich glaube dieser Vorgang sagt zuallererst mehr über uns aus als alles andere. Nämlich, dass wir Sprache für ein menschliches Kennzeichen halten und dass wir sehr geneigt sind, jedem sprechenden Gegenstand menschliche Eigenschaften zu attestieren.

Um das zu veranschaulichen hat ein Dozent in den Staaten während einer Vorlesung einmal seinen Stift vom Pult genommen, ihm einen Schnurrbart angeklebt und angefangen mit ihm zu sprechen. Und während des Gesprächs nimmt er den Stift und knallt ihn mit voller Wucht auf das Pult, sodass er zerbricht. Ein Aufschrei des Publikums! Als hätte er gerade eine Person erschlagen. Es war ein Stift. Eine ganze Menge sehr intelligenter junger Menschen in einer Vorlesung war bereit, einem sprechenden Stift menschliche Eigenschaften zu attestieren. Weil unsere Vorstellungskraft dazu in der Lage ist. Soviel also zur Zuverlässigkeit [des Turing-Testes](https://de.wikipedia.org/wiki/Turing-Test).

ChatGPT ist ein Softwarealgorithmus, der aufgrund von Wahrscheinlichkeitsrechnung Texte erstellt. Das funktioniert nur aufgrund von bereits vorhandenen Texten, mit denen diese Datenstrukturen trainiert wurden und es wird auch nur Informationen hervorbringen, die aufgrund dieser bereits existierenden Texte möglich sind. Algorithmen dieser Art sind ["stochastischen Papageien"](https://www.heise.de/news/Konflikt-um-Fachaufsatz-von-Google-KI-Forscherin-4982590.html). [Echte Kreativität](https://www.newyorker.com/culture/the-weekend-essay/why-ai-isnt-going-to-make-art), Emotionen, Bewusstsein, Intention, all das haben diese Texte nicht. Sie klingen nur so, weil sie menschliche Sprache nachmachen und deswegen sind wir so gerne bereit, das zu glauben.

D.h. unsere Leichtgläubigkeit und die Bereitschaft, mit der wir Software oder uns selbst zu einem goldenen Kalb machen - die ist ein Problem. Wir haben ein Wesen erschaffen: "Schau, es kann sprechen wie wir. Wir sind Gott!" - Nein, sind wir nicht.

Aber wir sind genau wie die Israelit:innen und zahlen dafür einen hohen Preis, der sozusagen das wirtschaftliche Gegengewicht zu dem Venture Capital bildet, das Sam Altman einsammelt.

Auf der Seite der sozialen Kosten gibt es nun so viele schwierige Aspekte, dass ich sie schlicht nicht alle darstellen und ausgewogen diskutieren kann.

Die zwei größten Punkte, sozusagen die zwei größten Goldklunker, die wir zahlen, sind einmal der gigantische Energieumsatz und dann die Sklavenarbeit von Menschen.

So kurz wie es geht: [Die OECD geht davon aus](https://oecd.ai/en/wonk/understand-environmental-costs), dass generative KI im Jahr 2026 soviel elektrische Energie umsetzen werden wie die Staaten Finnland oder Österreich. Microsoft lässt dafür gerade wieder [ein altes Atomkraftwerk in Betrieb](https://www.borncity.com/blog/2024/09/26/ai-microsoft-explodierende-energieverbraeuche-und-kosten/) nehmen. Für 1,6 Milliarden Dollar. Und [Google und Amazon wollen neue Mini-AKW entwickeln](https://www.heise.de/hintergrund/Warum-Google-Microsoft-und-Amazon-auf-Atomenergie-setzen-9999297.html), um ihren Energiebedarf zu decken. Im Angesicht der Klimakatastrophe ist das eine echte Katastrophe. Denn eigentlich waren alle Big-Tech-Firmen entschlossen, ihre Energiebilanz deutlich verbessern zu wollen.

Das andere:[In Kenia und in Argentinien arbeiten Menschen für 2 oder 1,7 US Dollar die Stunde](https://netzpolitik.org/2024/data-workers-inquiry-die-versteckten-arbeitskraefte-hinter-der-ki-erzaehlen-ihre-geschichten/), um die Sprachmodelle zu korrigieren. So zu korrigieren, dass sie für uns überhaupt annehmbar und sinnvoll klingen. Dafür nehmen diese schlimme Arbeitsbedingungen auf sich und psychische Schäden mit davon. Die Weltbank geht davon aus, dass weltweit [zwischen 154 Millionen und 435 Millionen Datenarbeiter:innen](https://openknowledge.worldbank.org/entities/publication/ebc4a7e2-85c6-467b-8713-e2d77e954c6c) gibt, von denen viele in den Ländern des globalen Süden leben, für die Tech-Konzerne schuften müssen. Und das alles für unsere Bequemlichkeit.

Aber was ist das überhaupt für eine Bequemlichkeit? [Joanna Maciejewska brachte es in den Sozialen Medien mit einem vielzitierten Wort auf den Punkt](https://x.com/AuthorJMac/status/1773679197631701238): "Ich will doch, dass KI meine Wäsche und mein Geschirr macht, damit ich Kunst und Schriftstellerei betreiben kann und nicht, dass KI für mich Kunst und Schriftstellerei macht, damit ich weiter Geschirr und Wäsche machen muss."

Ihr merkt: Ich bin überzeugt, dass die Kosten für *diese Form* von generativer KI viel zu hoch sind. Ja, auch ein zwei-jähriges Kind muss wirklich viel futtern und braucht viel Energie bis es sprechen kann. Aber die Kosten für generative KI sind absurd. Wir brauchen dafür [strenge Gesetzgebung](https://netzpolitik.org/2024/europaeische-ki-verordnung-die-eigentlichen-regeln-fuer-chatgpt-kommen-noch/), für viele soziale Aspekte, die bei dieser Technologie eine Rolle spielen. Dabei sein sollte unter anderem ein sehr teures CO2-Budget.

Und wir sollten auch aufhören dem Marketing dieser Big-Tech-Firmen auf den Leim zu gehen. Denn die sind darin natürlich sehr gut, uns zu erzählen wie toll und gefährlich diese generativen Modelle sind usw. Ja, das Spiel mit der Angst ist vermutlich ihre wichtigste Marketingmasche. Gefährlich ist Software, wenn wir sie gefährliche Dinge tun lassen. Das ist unsere Entscheidung. Software selbst wird nicht einfach von alleine selbstständig und wir werden nicht zu Gott.

[Aber wir sind sehr klug, denn so hat Gott uns gemacht.](https://www.die-bibel.de/bibel/BB/GEN.1.27) Und so können wir schöne Software schreiben. Daran hab ich auch Spaß. Gute KI schreiben, die z.B. auf unseren iPhones Gesichter erkennt. Denn der Betrieb eines Smartphones kostet so viel Energie, dass ihr die  problemlos mit einem Fahrrad herbeistrampeln könntet, wenn ihr wolltet.

Oder wir schauen zu der meeresbiologischen Forschung, die mit lernenden Algorithmen z.B. die Rufe von Walen analysiert hat. [Und Hinweise gefunden hat, dass man in den Stimmen von Walen sprachliche Merkmale finden kann.](https://www.heise.de/news/Erste-Erkenntnisse-Was-KI-ueber-die-Sprache-der-Wale-verraet-9578936.html) Was wäre, wenn wir in den Ozeanen intelligente Wesen haben, die tatsächlich miteinander sprechen? Wesen, die zunehmend von einem Lebensraum in den nächsten vertrieben werden, weil wir mit unnötiger CO2-Erzeugung die Meere zum Kochen bringen. Lasst uns lieber diesen Lebewesen intelligente Eigenschaften zutrauen, denn Gott hat sie gemacht wie uns.

So bleibt Gott Gott und wir die, die auf seine Schöpfung aufpassen. Hoffen wir, dass Mose uns dafür nicht nochmal seine Tafeln bringen muss und wir sollten unsere Klunker dafür hergeben, Leben zu erhalten, denn es gibt so viele Herzen, die schlagen. Sie fühlen, leiden und haben einen Willen. Wir sind für sie verantwortlich. Denn dazu hat Gott uns gemacht. Amen.
