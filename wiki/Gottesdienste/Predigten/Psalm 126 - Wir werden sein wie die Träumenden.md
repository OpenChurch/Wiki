---
title: Psalm 126 - Wir werden sein wie die Träumenden
summary: Tränen sind Teil unseres Lebens bis sich Gottes Traum erfüllt
authors:
    - Johannes Brakensiek
tags:
    - Psalm 126
    - Traum
    - Strom
    - Tränen
    - Campino
    - Saat
    - Auferstehung
date: 2024-11-24
---

# Psalm 126 - Wir werden sein wie die Träumenden

"Wir werden sein wie die Träumenden." sagt die Beterin des Psalm 126, den wir zu Anfang gemeinsam gesprochen haben.

Wovon haben Sie geträumt, liebe Gemeinde? In den Tagen vor dem Tod Ihres lieben Menschen. In den vielen Nächten, in denen das Leben schwer und ungewiss war? Waren es traumlose Nächte, mit wenig Schlaf? Waren es Nächte voller Angst und Ungewissheit, Träume voller Schreckensbilder? Vermutlich waren Sie in dieser Zeit Menschen mit Träumen, die Sie sich nicht zurück wünschen.

Und welche Träume waren es nach der Trauerfeier und der Beerdigung Ihres lieben Menschen? Waren es Träume, in denen sich das Vergangene der letzten Tage wiederholte? Waren es Eindrücke von ganz früher? Und dann gab es da Tage, an denen sie wach waren und es die Tagträume sind und das Gefühl: Stand er da nicht gerade in der Tür, so wie immer? Saß sie nicht dort und hat diese Handbewegung gemacht, die sie immer gemacht hat?

Da sind die Gefühle, die bei dem Sortieren der Kleidungsstücke hervorkommen. Gerüche, die uns als Menschen verbinden, so wie eine Mutter den Geruch ihrer Kinder kennt. Der Bademantel, die Jacke, die Ledertasche,… Was haben sie erlebt, was haben wir mit ihnen erlebt.

Da gab und gibt es sicher schwere Träume voller Traurigkeit und dem Fehlen eines Menschen. Aber dann irgendwann vielleicht auch Träume gefüllt mit Erinnerungen von der Fülle des Lebens. Davon, wie Sie zusammen saßen beim Fest oder sie einen Witz machte, so wie immer. Und in Ihrem Herz ist dieser Gedanke, diese Erinnerung ein Keim der Dankbarkeit und der Zufriedenheit.

Auch das Volk Israel träumte damals. Sie träumten von ihrem Gott. Sie träumten davon, dass sie ihre Ketten der Gefangenschaft lösen, dass sie aus dem Exil zurückkehren würden in ihre Heimat und: Dass Gott selbst zurückkehrt nach Zion, d.h. zu seinem Volk, zu seinen Menschen. Dass er so bei ihnen ist, wie sie es in der schweren Arbeit der Gefangenschaft nur hoffen konnten. Sie würden wieder frei sein, ihre Land bewirtschaften und ihr Leben gestalten. Und im Tempel, da wäre Gott gegenwärtig und sie könnten ihn besuchen.

Im Traum ist alles möglich. Das, was eigentlich nicht möglich ist und das, was leider manchmal dann doch eintreten musste.

Im Traum ist aber auch das traumhaft Schöne möglich, was wirklich werden kann und was am Ende kein Traum mehr bleiben muss.

Gottes Gegenwart unter uns ist schon da. Wir, als Christinnen und Christen wissen es seit der Geburt Jesu Christi. Gott ist da. In den Alp- und den Wunschträumen unseres Lebens. Im Alltag. Unser Leben ist der Tempel Gottes. In den Schwierigkeiten jedes Tages. Bei der schweren Aufgabe, die Sachen eines Lieben Menschen zu sortieren. Zu entscheiden, welche Erinnerungsstücke aufzuheben sind, welcher Enkel was bekommt und was dann auch den Weg allen Vergänglichens gehen muss.

Gott ist da, in dem Neu-Sortieren des eigenen Lebens. Vielleicht sind es jetzt weniger Aufgaben, weniger Arbeit, weniger Sorgen, ja. Aber ein Platz, der leer bleibt. Ein Bett, das kalt bleibt. Niemand sitzt auf dem Sofa, niemand steht im Türrahmen. Sie gehen weiter Ihren Weg. Und Gott ist da.

Er war immer schon bei den Menschen und zu ihm kehren wir zurück. So haben wir, so habe ich es bei der Beerdigung benannt.

Im christlichen Glauben und im christlichen Verständnis ist der eigentliche Tod, ohne Gott zu leben. Ohne Gott sind wir vom Leben getrennt. Von der Quelle des Lebens und vom Ziel des Lebens. Ohne Quelle wäre der Rhein, unser mächtiger Strom, ein leeres Flussbett. Und Holland wäre eine trockene oder salzige Einöde.

Im Tod kehren wir zurück zur Quelle unseres Lebens. Gott, bringt die Bäche wieder im Südland, sagt der Psalm. Und was uns heute scheint wie eine Sackgasse, ein Weg voller Tränen, ist am Ende eine Saat in Gottes Acker.

Der Apostel Paulus schreibt davon im 1. Korintherbrief und Martin Luther hat davon eine große Predigt über die Auferstehung gemacht:

Der Tod, nein, unser Leben ist wie die Aussaat in einen Acker. Wir denken, das wäre alles, das Leben wäre dann zu Ende. Aber es ist eine Aussaat. Und im Frühjahr keimen die grünen Halme aberwitzig und zu tausenden aus dem Boden und bringen das Leben hervor, das sie vorher selbst waren. Von Ewigkeit zu Ewigkeit. Am Anfang ein kleines Korn in einer Ähre, voll von Leben. Da kommen wir her. Und unser Leben ist die kurze Dauer, in der wir in der Hand des Bauern oder der Bäuerin sind. Die kurze Zeit, in der mit uns gearbeitet wird, in der wir fliegen und landen. Um dann im Tod zu neuen Leben zu kommen. Ein grüner Halm, dann eine Ähre, voll von Leben. In Ewigkeit.

Wenn wir die Augen zum Himmel heben und uns die Ewigkeit anschauen. Dann ist im Anblick der Ewigkeit unser Leben eine Saat und der Tod ist der Übergang des Korns zu neuem Leben. 

In unserem Leben haben wir manches in der Hand, können selbst gestalten, bauen und säen. Aber das Leben an sich, wie es gemacht ist, wie es endet. Das können wir nur doch nur aus der Hand des Bauern nehmen. Wir werden hinnehmen müssen, das wir manches unter Tränen tun. Aber je stärker wir wissen, woher wir kommen und wohin wir gehen: Umso stärker fühlen und hoffen wir, was der Psalm 126 beschreibt;

Die mit Tränen säen,  
werden mit Freuden ernten.  
Sie gehen hin und weinen  
und tragen guten Samen  
und kommen mit Freuden  
und bringen ihre Garben.  

Der Herr hat Großes an uns getan;  
des sind wir fröhlich.  

Campino, der Sänger der Toten Hosen, hat dazu ein Lied geschrieben. Ja, das ist jene linke Punkrockband, die in ihren frühen Jahren von Religion als Opium für das Volk sang. Und in seinen späten Jahren schreibt und singt Campino über den Tod seiner Mutter dann in dem Lied "Nur zu Besuch". Es ist ein Besuch am Grab:

Und so red' ich mit dir wie immer  
Und ich verspreche dir  
Wir haben irgendwann wieder jede Menge Zeit  
Dann werden wir uns wiedersehen  
Du kannst dich ja kümmern, wenn du willst  
Dass die Sonne an diesem Tag auch auf mein Grab scheint  
Dass die Sonne scheint  
Dass sie wieder scheint  

Amen.