README
======

Dies sind die Quellen für [OpenChurch.Wiki](https://openchurch.wiki).

Wissenswertes über dieses Projekt steht auf der Seite ["Über dieses Projekt"](wiki/index.md).

## Arbeiten und Veröffentlichen mit MkDocs

### Vorbereitung für die Arbeit mit Mediendateien

Optional: Falls mit Mediendateien/Binärdaten (Bilder, Töne, Präsentationen) gearbeitet werden soll, empfiehlt es sich, vor dem Clonen (s.u., Punkt 3) die git-Erweiterung git-lfs zu installieren.

Zum Verwalten von Mediendateien verwendet dieses Repository git-lfs.

Unter Debian/Ubuntu:
```
sudo apt install git-lfs
```

### Software vorbereiten und Repository clonen (herunterladen)

1. mkdocs installieren. Unter Debian und Ubuntu mit `pipx`:
  `pipx install mkdocs`
2. Die benötigte Python-Erweiterungen installieren:
  `pipx inject mkdocs pymdown-extensions 'mkdocs[i18n]'`
3. Das Repository clonen und in das Verzeichnis wechseln:
  `git clone git@codeberg.org:OpenChurch/Wiki.git OpenChurch.Wiki && cd OpenChurch.Wiki`
4. Falls mit Medien/git-lfs gearbeitet werden soll, folgende Einstellung vornehmen:
   `git config lfs.https://codeberg.org/OpenChurch/Wiki.git/info/lfs.locksverify true`
5. Ebenfalls Optional, nur nötig, wenn Änderungen manuell veröffentlicht werden sollen: Submodule (das Verzeichnis mit den generierten HTML-Dateien) laden: 
```
git submodule init
git submodule update
```

### Vor späteren Änderungen

Änderungen von anderen herunterladen/synchronisieren:

```git pull --recurse-submodules```

Dies holt auch für das ggf. mit ausgecheckte pages-Verzeichnis die letzte verfügbare Version – für den Fall, dass jemand vergessen haben sollte, die Referenz im Wiki-Repo zu aktualisieren (siehe unten).

### Lokale Vorschau von Änderungen

1. Betreffende Datei ändern und speichern.
2. Im Projektverzeichnis aufrufen: `mkdocs serve`
3. Auf http-Link klicken, Browser sollten den lokalen Webserver ansteuern.

Bei Änderungen sollte der Browser automatisch neu geladen werden!

### Veröffentlichen von Änderungen

Alle Änderungen hübsch? Dann hochladen!

Dazu ist nur die Übertragung der Änderungen an den Quelldateien erforderlich. Die Seiten werden dann automatisiert von Codeberg/Forgejo Actions in HTML-Seiten umgewandelt (veröffentlicht). Dies dauert ggf. ein paar Minuten. Der Status bis zur Veröffentlichung ist am Symbol an der Commit-Nachricht oder auf der Seite [Actions](https://codeberg.org/OpenChurch/Wiki/actions) einsehbar.

```bash
# Quellenänderungen in den Commit aufnehmen
git add .

# Quellenänderungen in das lokale Repo committen
git commit add -m "Das und das habe ich ergänzt und das korrigiert"

# Änderungen in das Quellen-Repo übertragen
git push
```

Nach dem Push beginnt die Generierung der Webseite.

### Änderungen manuell veröffentlichen

Das hier ist der manuelle Prozess, für die die es selbst machen wollen und Änderungen sofort online haben wollen.

Zeilenweise nacheinander ausführen:

```bash
# Seiten generieren
mkdocs build

# In das Verzeichnis mit den generierten Seiten wechseln (Submodule, das auf das pages-Repo verweist)
cd pages

# Änderungen in den Commit aufnehmen
git add .

# Neu generierte Dateien in das lokale Repo committen
git commit add -m "Publish changes"

# Änderungen hochladen = auf openchurch.wiki veröffentlichen
git push

# In das übergeordnete Projektverzeichnis (Wiki) wechseln
cd ..

# Quellenänderungen + die aktuelle Referenz auf das Submodul (pages) in den Commit aufnehmen
git add .

# Quellenänderungen in das lokale Repo committen
git commit add -m "Das und das habe ich ergänzt und das korrigiert"

# Änderungen in das Quellen-Repo übertragen
git push
```
